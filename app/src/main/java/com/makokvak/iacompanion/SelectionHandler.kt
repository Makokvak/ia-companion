package com.makokvak.iacompanion

import com.makokvak.iacompanion.util.*

class SelectionHandler {

    var maxItemsSelected = Int.MAX_VALUE

    private val _selectedIndices = MutableNonNullLiveData(listOf<Int>())
    val selectedIndices: NonNullLiveData<List<Int>> = _selectedIndices

    fun isSelected(index: Int): Boolean {
        return selectedIndices.value.contains(index)
    }

    fun selectItem(index: Int) {
        if(!selectedIndices.value.contains(index))
            toggleItemSelection(index)
    }

    private fun canSelectAnotherItem()
        = maxItemsSelected > selectedIndices.value.size

    private fun isSingleItemSelection()
        = maxItemsSelected == 1

    fun toggleItemSelection(index: Int) {

        if (selectedIndices.value.contains(index)) {
            _selectedIndices.remove(index)
        }
        else {

            if(isSingleItemSelection() && selectedIndices.value.isNotEmpty())
                clearAllSelections()

            if(canSelectAnotherItem())
                _selectedIndices.add(index)
        }
    }

    fun <T> getSelectedItems(items: List<T>) =
        selectedIndices.value.map { items[it] }

    fun clearAllSelections() {
        _selectedIndices.clear()
    }
}
