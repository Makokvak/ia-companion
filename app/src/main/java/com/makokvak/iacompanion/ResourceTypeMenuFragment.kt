package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.util.LayoutResourceRecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.item_component.view.*
import org.koin.android.ext.android.inject

class ResourceTypeMenuFragment : ListBaseFragment<ResourceTypeMenuViewModel, ResourceType, View>(
    R.layout.fragment_generic_list,
    ResourceTypeMenuViewModel::class
) {

    override val itemFactory: RecyclerViewItemFactory<View>
        get() = LayoutResourceRecyclerViewItemFactory(R.layout.item_component)

    private val target: Navigation.ResourceTypeMenuTarget by inject()

    override val recyclerViewResourceId: Int
        get() = R.id.recyclerView

    override val data: NonNullLiveData<out List<ResourceType>>
        get() = viewModel.data

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
    }

    override fun onBindItemView(itemView: View, data: ResourceType, position: Int) {
        super.onBindItemView(itemView, data, position)

        itemView.textView.text = getString(data.title)
        itemView.imageView.setImageResource(data.imageResourceId)
        itemView.rootView.setOnClickListener {
            target.toResourceList.go(
                fragmentContainer = baseActivity,
                arg = Navigation.ResourceListParams(resourceType = data, component = target.retrieveArgument(fragment = this))
            )
        }
    }
}
