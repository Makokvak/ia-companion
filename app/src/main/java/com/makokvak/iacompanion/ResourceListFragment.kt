package com.makokvak.iacompanion

import android.content.res.Resources
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.extension.setHeightBoundByAspect
import com.makokvak.iacompanion.util.util.GridItemSpacingDecoration
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.fragment_generic_list.recyclerView
import kotlinx.android.synthetic.main.fragment_search_list.*
import kotlinx.android.synthetic.main.item_resource.view.*
import org.koin.android.ext.android.inject
import kotlin.reflect.KClass

typealias ShouldNavigateToDetail = Boolean

class OwnedItemsResourceListFragment : BaseResourceListFragment<OwnedItemsResourceListViewModel>(viewModelClass = OwnedItemsResourceListViewModel::class) {

    private val localTarget: Navigation.OwnedItemsResourceListTarget by inject()

    override fun onItemClicked(
        itemView: ResourceItemView,
        data: Resource,
        position: Int
    ): ShouldNavigateToDetail {

        localTarget.toDetail.go(fragmentContainer = baseActivity, data)
        return false
    }

}
class OwnedAlliesResourceListFragment : BaseResourceListFragment<OwnedAlliesResourceListViewModel>(viewModelClass = OwnedAlliesResourceListViewModel::class)
class OwnedVillainsResourceListFragment : BaseResourceListFragment<OwnedVillainsResourceListViewModel>(viewModelClass = OwnedVillainsResourceListViewModel::class)
class EarnedRewardsResourceListFragment : BaseResourceListFragment<EarnedRewardsResourceListViewModel>(viewModelClass = EarnedRewardsResourceListViewModel::class)
class ImperialEarnedRewardsResourceListFragment : BaseResourceListFragment<ImperialEarnedRewardsResourceListViewModel>(viewModelClass = ImperialEarnedRewardsResourceListViewModel::class)
class OngoingAgendaResourceListFragment : BaseResourceListFragment<OngoingAgendaResourceListViewModel>(viewModelClass = OngoingAgendaResourceListViewModel::class)
class BuyHeroSkillsResourceListFragment : BaseResourceListFragment<BuyHeroSkillsResourceListViewModel>(viewModelClass = BuyHeroSkillsResourceListViewModel::class) {

    private val localTarget: Navigation.BuyHeroSkillsResourceListTarget by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val argument = localTarget.retrieveArgument(fragment = this)
        viewModel.setHero(argument)
    }

    override fun onBindItemView(itemView: ResourceItemView, data: Resource, position: Int) {
        super.onBindItemView(itemView, data, position)

        val hasSkill = viewModel.hasSkill(data)
        itemView.setOwned(hasSkill)
    }

    override fun onItemClicked(
        itemView: ResourceItemView,
        data: Resource,
        position: Int
    ): ShouldNavigateToDetail {

        val hasSkill = viewModel.hasSkill(data)
        val isMissionActive = viewModel.isMissionActive()

        if(!hasSkill && !isMissionActive)
            localTarget.toBuyHeroSkillDetail.go(fragmentContainer = baseActivity, arg = data)
        else
            localTarget.toDetail.go(fragmentContainer = baseActivity, arg = data)

        return false
    }

    override var resourceType: ResourceType? = ResourceType.Skills
}

class SearchResourceListFragment : BaseResourceListFragment<SearchResourceListViewModel>(viewModelClass = SearchResourceListViewModel::class, layoutRes = R.layout.fragment_search_list) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editText_search.addTextChangedListener {
            viewModel.search(it.toString())
        }
    }

    override fun onResume() {
        super.onResume()

        editText_search.setText(viewModel.searchString.value)
    }
}

class ResourceListFragment : BaseResourceListFragment<ResourceListViewModel>(viewModelClass = ResourceListViewModel::class)

abstract class BaseResourceListFragment<ViewModel: ResourceListViewModel>(@LayoutRes val layoutRes: Int = R.layout.fragment_generic_list, val viewModelClass: KClass<ViewModel>) : ListBaseFragment<ViewModel, Resource, ResourceItemView>(
    layoutRes,
    viewModelClass
) {

    override val itemFactory = ResourceItemView.Factory()

    protected val target: Navigation.ResourceListTarget by inject()

    protected open var resourceType: ResourceType? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.addItemDecoration(GridItemSpacingDecoration(16))

        val component: Component
        if(resourceType == null) {
            val arg = target.retrieveArgument(fragment = this)
            component = arg.component
            resourceType = arg.resourceType
        }
        else component = Component.AllComponents

        val resourceType = resourceType ?: return
        viewModel.setResourceTypeAndComponent(resourceType, component)
    }

    override val layoutManager: RecyclerView.LayoutManager
        get() = GridLayoutManager(context, 2)

    override val data: NonNullLiveData<out List<Resource>>
        get() = viewModel.data

    override val recyclerViewResourceId: Int
        get() = R.id.recyclerView

    override fun onBindItemView(itemView: ResourceItemView, data: Resource, @Suppress("UNUSED_PARAMETER") position: Int) {
        itemView.imageView.setImageResource(data.displayableParams.drawable)

        itemView.textView_resource_name.text = getString(data.displayableParams.name)
        itemView.textView_resource_name.visibility = if(resourceType?.requiresNameShown == true) View.VISIBLE else View.GONE

        itemView.setOnClickListener {
            if(onItemClicked(itemView, data, position))
                target.toDetail.go(fragmentContainer = baseActivity, arg = data)
        }
    }

    override fun onCreateItemView(itemView: View) {
        super.onCreateItemView(itemView)

        val type = resourceType ?: return
        val calculatedWidth = (Resources.getSystem().displayMetrics.widthPixels / 2f) - dpToPx(24)
        itemView.setHeightBoundByAspect(calculatedWidth.toInt(), type.heightAspectRatioMultiplier)
    }

    protected open fun onItemClicked(itemView: ResourceItemView, data: Resource, position: Int): ShouldNavigateToDetail {
        return true
    }
}
