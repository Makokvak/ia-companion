package com.makokvak.iacompanion.util

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.util.util.LifecycleAwareRecyclerViewAdapter
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import kotlin.reflect.KClass

abstract class ListBaseFragment<TViewModel : BaseViewModel, TItem, TView : View>(
    @LayoutRes layoutResourceId: Int,
    viewModelClass: KClass<TViewModel>
) : BaseFragment<TViewModel>(layoutResourceId, viewModelClass) {

    abstract val recyclerViewResourceId: Int
    abstract val data: NonNullLiveData<out List<TItem>>

    open val layoutManager: RecyclerView.LayoutManager
        get() = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

    abstract val itemFactory: RecyclerViewItemFactory<TView>

    @Suppress("MemberVisibilityCanBePrivate")
    val adapter
        get() = _adapter

    private lateinit var _adapter: LifecycleAwareRecyclerViewAdapter<TItem, TView>
    private lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _adapter = LifecycleAwareRecyclerViewAdapter(
            viewLifecycleOwner,
            data,
            itemFactory,
            ::onBindItemView,
            ::onCreateItemView
        )

        recyclerView = view.findViewById(recyclerViewResourceId)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager
    }

    open fun onBindItemView(itemView: TView, data: TItem, position: Int) {
    }

    open fun onCreateItemView(itemView: View) {
    }
}
