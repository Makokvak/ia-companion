package com.makokvak.iacompanion.util

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import timber.log.Timber
import java.io.Serializable

internal fun <T, TArg : Serializable?> getClassInstance(clazz: Class<T>, arg: TArg? = null): T? {
    try {
        val fragment: T = clazz.getConstructor().newInstance()
        arg?.let {
            (fragment as Fragment).arguments = Bundle().apply {
                putSerializable(NavigationTarget.EXTRA_ARG_KEY, arg)
            }
        }
        return fragment
    } catch (exception: Exception) {
        Timber.e(exception)
    }
    return null
}

class FragmentNavigationAction<TNavigationTarget : FragmentTarget> internal constructor(
    private val destination: TNavigationTarget,
    private val navigationSetting: NavigationActionSetting
) : BaseFragmentNavigationAction<TNavigationTarget>(navigationSetting) {

    fun go(fragmentContainer: FragmentContainer?) {

        if (fragmentContainer == null) {
            Timber.e("No fragment container supplied, no action performed")
            return
        }

        val fragmentManager = fragmentContainer.containerFragmentManager
        val destinationName = destination.clazz.name
        val fragmentFromBackstack = fragmentManager.findFragmentByTag(destinationName)

        val shouldReuseBackstackFragment = fragmentFromBackstack != null && navigationSetting !is RootFragmentSetting && navigationSetting !is DoNotReuseBackstackFragment

        if (shouldReuseBackstackFragment) {
            fragmentManager.popBackStack(destinationName, 0)
            return
        }

        val fragmentInstance = getClassInstance(destination.clazz, null) ?: return
        replaceFragment(fragmentManager, fragmentContainer, fragmentInstance, destinationName)
    }
}

class ArgFragmentNavigationAction<TArg : Serializable?, TNavigationTarget : ArgFragmentTarget<TArg>> internal constructor(
    private val destination: TNavigationTarget,
    navigationSetting: NavigationActionSetting
) : BaseFragmentNavigationAction<TNavigationTarget>(navigationSetting) {

    fun go(fragmentContainer: FragmentContainer?, arg: TArg? = null) {

        if (fragmentContainer == null) {
            Timber.e("No fragment container supplied, no action performed")
            return
        }

        val fragmentManager = fragmentContainer.containerFragmentManager
        val destinationName = destination.clazz.name

        val fragmentInstance = getClassInstance(destination.clazz, arg) ?: return
        replaceFragment(fragmentManager, fragmentContainer, fragmentInstance, destinationName)
    }
}

open class BaseFragmentNavigationAction<TNavigationTarget : NavigationTarget>(
    private val navigationSetting: NavigationActionSetting
) : Serializable {

    protected fun replaceFragment(
        fragmentManager: FragmentManager,
        fragmentContainer: FragmentContainer,
        fragmentInstance: Fragment,
        destinationName: String
    ) {
        if (navigationSetting is RootFragmentSetting)
            clearBackStack(fragmentManager)

        fragmentManager.beginTransaction().apply {
            replace(fragmentContainer.fragmentContainerId, fragmentInstance, destinationName)
            if (navigationSetting.shouldAddToBackStack()) addToBackStack(destinationName)
            commit()
        }
    }

    private fun clearBackStack(fragmentManager: FragmentManager) {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}

class DialogFragmentNavigationAction<TNavigationTarget : DialogFragmentTarget>(private val destination: TNavigationTarget) {

    fun go(fragmentManager: FragmentManager) {
        val fragmentInstance = getClassInstance(destination.clazz, null) ?: throw AssertionError()
        fragmentInstance.show(fragmentManager, null)
    }
}

class ActivityNavigationAction<TNavigationTarget : ActivityTarget>(private val destination: TNavigationTarget) {

    fun go(context: Activity) {
        val intent = Intent(context, destination.clazz)
        context.startActivity(intent)
    }
}

class ArgActivityNavigationAction<TArg : Serializable?, TNavigationTarget : ArgActivityTarget<TArg>>(private val destination: TNavigationTarget) {

    fun go(context: Activity?, arg: TArg) {
        context ?: return
        val intent = Intent(context, destination.clazz)
        intent.putExtra(NavigationTarget.EXTRA_ARG_KEY, arg)
        context.startActivity(intent)
    }
}
