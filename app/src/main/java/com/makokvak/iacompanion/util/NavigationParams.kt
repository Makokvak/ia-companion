package com.makokvak.iacompanion.util

import java.io.Serializable

open class RootFragmentSetting internal constructor() : NavigationActionSetting()

class InitialRootFragmentSetting : RootFragmentSetting() {
    override fun shouldAddToBackStack(): Boolean = false
}

class AddToBackStackSetting internal constructor() : NavigationActionSetting() {
    var value = false
    override fun shouldAddToBackStack() = value
}

class DoNotReuseBackstackFragment internal constructor() : NavigationActionSetting()

open class NavigationActionSetting internal constructor() : Serializable {

    companion object {
        fun root() = RootFragmentSetting()
        fun initialRoot() = InitialRootFragmentSetting()
        fun doNotReuseBackstackFragment() = DoNotReuseBackstackFragment()
        fun addToBackStack(value: Boolean): AddToBackStackSetting {
            val setting = AddToBackStackSetting()
            setting.value = value
            return setting
        }
    }

    internal open fun shouldAddToBackStack() = true
}
