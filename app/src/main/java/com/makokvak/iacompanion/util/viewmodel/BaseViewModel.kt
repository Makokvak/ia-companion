package com.makokvak.iacompanion.util.viewmodel

import androidx.lifecycle.ViewModel
import com.makokvak.iacompanion.util.*

open class ViewModelState
object InitialState : ViewModelState()
object LoadingState : ViewModelState()
object ReadyState : ViewModelState()
object EmptyState : ViewModelState()
data class ErrorState(val error: Throwable) : ViewModelState()

open class BaseViewModel : ViewModel() {

    private val inLoadingState = MutableNonNullLiveData(false)
    private val inReadyState = MutableNonNullLiveData(false)
    private val inErrorState = MutableNonNullLiveData(false)
    private val inEmptyState = MutableNonNullLiveData(false)
    private val inInitialState = MutableNonNullLiveData(true)

    @Suppress("unused")
    val isInInitialState: NonNullLiveData<Boolean> = inInitialState
    @Suppress("unused")
    val isInLoadingState: NonNullLiveData<Boolean> = inLoadingState
    @Suppress("unused")
    val isInEmptyState: NonNullLiveData<Boolean> = inEmptyState
    @Suppress("unused")
    val isInReadyState: NonNullLiveData<Boolean> = inReadyState
    @Suppress("unused")
    val isInErrorState: NonNullLiveData<Boolean> = inErrorState

    @Suppress("MemberVisibilityCanBePrivate")
    protected val compositeLiveDataDisposable = CompositeLiveDataDisposable()

    @Suppress("MemberVisibilityCanBePrivate")
    private val mutableState: MutableNonNullLiveData<ViewModelState> =
        MutableNonNullLiveData(InitialState)

    private val mutableStateBackStack: MutableList<ViewModelState> = mutableListOf()
    private val mutablePreviousState: MutableNullableLiveData<ViewModelState> = MutableNullableLiveData()

    @Suppress("MemberVisibilityCanBePrivate")
    var state: NonNullLiveData<ViewModelState> = mutableState
    @Suppress("MemberVisibilityCanBePrivate")
    val previousState: NullableLiveData<ViewModelState> = mutablePreviousState
    @Suppress("MemberVisibilityCanBePrivate")
    val stateBackstack: List<ViewModelState> = mutableStateBackStack

    protected fun setState(state: ViewModelState) {
        mutableState.value = state
    }

    override fun onCleared() {
        compositeLiveDataDisposable.disposeAll()
        super.onCleared()
    }

    init {

        state.observeForeverWithDisposable {
            mutableStateBackStack.add(it)

            val localPreviousState = fetchPreviousState()

            if (localPreviousState != previousState.value)
                mutablePreviousState.value = localPreviousState

            when (it) {
                is InitialState -> {
                    inInitialState.value = true
                }
                is LoadingState -> {
                    inLoadingState.value = true
                }
                is EmptyState -> {
                    inEmptyState.value = true
                }
                is ReadyState -> {
                    inReadyState.value = true
                }
                is ErrorState -> {
                    inErrorState.value = true
                }
            }
        }.disposeWith(compositeLiveDataDisposable)

        previousState.observeForeverWithDisposable {
            when (it) {
                is InitialState -> {
                    inInitialState.value = false
                }
                is LoadingState -> {
                    inLoadingState.value = false
                }
                is EmptyState -> {
                    inEmptyState.value = false
                }
                is ReadyState -> {
                    inReadyState.value = false
                }
                is ErrorState -> {
                    inErrorState.value = false
                }
            }
        }.disposeWith(compositeLiveDataDisposable)
    }

    private fun fetchPreviousState(): ViewModelState? = mutableStateBackStack.getOrNull(mutableStateBackStack.size - 2)
}
