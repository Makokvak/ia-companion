package com.makokvak.iacompanion.util.dialog

import androidx.fragment.app.DialogFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.NullableLiveData
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class BaseDialogFragment<TViewModel : BaseViewModel>(
    viewModelClass: KClass<TViewModel>
) : DialogFragment() {

    fun <T> NullableLiveData<T>.observe(block: (T?) -> Unit) = this.observe(viewLifecycleOwner, block)
    fun <T : Any> NonNullLiveData<T>.observe(block: (T) -> Unit) = this.observe(viewLifecycleOwner, block)

    protected open val viewModel: TViewModel by lazy { getViewModel(viewModelClass) }
}
