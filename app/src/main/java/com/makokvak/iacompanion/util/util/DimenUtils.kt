package com.makokvak.iacompanion.util.util

import android.content.res.Resources
import android.util.TypedValue

/**
 * Converts dp to px
 *
 * @param dp dp
 * @return px
 */
fun dpToPx(dp: Int) = (dp * screenDensity).toInt()

/**
 * Converts px to dp
 *
 * @param px px
 * @return dp
 */
fun pxToDp(px: Int): Int = (px / screenDensity).toInt()

fun pxToDp(px: Double): Double = px / screenDensity

fun dpToPx(dp: Double): Double = dp * screenDensity

@Suppress("SameParameterValue")
fun spToPx(sp: Float) = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().displayMetrics)

val screenDensity: Float
    get() = Resources.getSystem().displayMetrics.density
