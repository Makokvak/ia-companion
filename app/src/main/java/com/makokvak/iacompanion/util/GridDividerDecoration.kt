package com.makokvak.iacompanion.util

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_detail.view.*

/**
 * ItemDecoration implementation that applies and inset margin
 * around each child of the RecyclerView. It also draws item dividers
 * that are expected from a vertical list implementation, such as
 * ListView.
 */
class GridDividerDecoration(context: Context) : RecyclerView.ItemDecoration() {

    companion object {
        private val INSETS = dpToPx(16)
        private val DIVIDER_SIZE: Int = dpToPx(1)
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = ContextCompat.getColor(context, R.color.divider)
    }

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        drawVertical(c, parent)
        drawHorizontal(c, parent)
    }

    /** Draw dividers at each expected grid interval  */
    private fun drawVertical(c: Canvas, parent: RecyclerView) {

        val childCount = parent.childCount

        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams

            val left = child.left - params.leftMargin - INSETS
            val right = child.right + params.rightMargin + INSETS
            val top = child.bottom + params.bottomMargin + INSETS
            val bottom = top + DIVIDER_SIZE

            c.drawRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), paint)
        }
    }

    /** Draw dividers to the right of each child view  */
    private fun drawHorizontal(c: Canvas, parent: RecyclerView) {

        val childCount = parent.childCount

        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            val params = child.layoutParams as RecyclerView.LayoutParams

            val left = child.right + params.rightMargin + INSETS
            val right = left + DIVIDER_SIZE
            val top = child.top - params.topMargin - INSETS + DIVIDER_SIZE
            val bottom = child.bottom + params.bottomMargin + INSETS

            c.drawRect(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat(), paint)
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        // We can supply forced insets for each item view here in the Rect
        outRect.set(
            INSETS,
            INSETS,
            INSETS,
            INSETS
        )
    }
}
