package com.makokvak.iacompanion.util.dialog

import android.os.Bundle
import android.app.Dialog
import androidx.appcompat.app.AlertDialog
import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import kotlin.reflect.KClass

abstract class SingleChoiceDialog<TViewModel : BaseViewModel>(viewModelClass: KClass<TViewModel>) :
    BaseDialogFragment<TViewModel>(viewModelClass) {

    abstract fun getDialogTitle(): String

    abstract fun getItems(): List<String>
    abstract fun getCurrentlySelectedItemIndex(): Int

    abstract fun onItemSelected(itemIndex: Int)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        var checkedIndex = getCurrentlySelectedItemIndex()
        val items = getItems().toTypedArray()

        return AlertDialog.Builder(requireContext())
            .setTitle(getDialogTitle())
            .setSingleChoiceItems(items, getCurrentlySelectedItemIndex()) { _, index -> checkedIndex = index }
            .setPositiveButton(getString(R.string.ok)) { _, _ ->
                if (items.isEmpty()) return@setPositiveButton
                onItemSelected(checkedIndex)
            }
            .create()
    }
}
