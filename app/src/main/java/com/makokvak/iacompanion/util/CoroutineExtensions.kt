package com.makokvak.iacompanion.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

fun <T> T.letInScope(scope: CoroutineScope, block: suspend CoroutineScope.(T) -> Unit): T {

    scope.launch {
        block(this@letInScope)
    }

    return this
}
