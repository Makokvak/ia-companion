package com.makokvak.iacompanion.util.util

import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.util.CompositeLiveDataDisposable
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.observeWithDisposable

/**
 * Generic wrapper of RecyclerView.Adapter intended for simpler and more concise use.
 *
 * @param <TItem> Type of items to be managed by this adapter
 * @param data list of data containing items managed by the adapter
 * @param onCreateItemView function that will be invoked whenever the RecyclerView creates an item view
 * @param onBindItemView function that will be invoked whenever the RecyclerView binds an item to a view
 */
class RecyclerViewAdapter<TItem, TView : View>(
    @Suppress("MemberVisibilityCanBePrivate") val data: MutableList<TItem>,
    private val recyclerViewItemFactory: RecyclerViewItemFactory<TView>,
    private val onBindItemView: ((itemView: View, data: TItem, position: Int) -> Unit)? = null,
    private val onCreateItemView: ((TView) -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val viewHolder = ViewHolder(recyclerViewItemFactory.makeView(parent.context, parent))
        @Suppress("UNCHECKED_CAST")
        onCreateItemView?.invoke(viewHolder.itemView as TView)

        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        require(holder is ViewHolder)

        onBindItemView?.invoke(holder.itemView, data[holder.adapterPosition], holder.adapterPosition)
    }
}

/**
 * Lifecycle aware generic wrapper of RecyclerView.Adapter intended for simpler and more concise use with LiveData and Android lifecycle.
 *
 * @param <TItem> Type of items to be managed by this adapter
 * @param lifecycleOwner lifecycle owner used to limit the scope of data observation
 * @param data live data containing items managed by the adapter, adapter will be automatically notified when you add items to it
 * @param onCreateItemView function that will be invoked whenever the RecyclerView creates an item view
 * @param onBindItemView function that will be invoked whenever the RecyclerView binds an item to a view
 *
 * DiffUtil.calculateDiff() is internally used to notify adapter about data changes. This requires TItem to correctly implement equals().
 * If not respected, results are unpredictable.
 */
open class LifecycleAwareRecyclerViewAdapter<TItem, TView : View>(
    private val lifecycleOwner: LifecycleOwner,
    @Suppress("MemberVisibilityCanBePrivate") data: NonNullLiveData<out List<TItem>>,
    private val recyclerViewItemFactory: RecyclerViewItemFactory<TView>,
    private val onBindItemView: ((itemView: TView, data: TItem, position: Int) -> Unit)? = null,
    private val onCreateItemView: ((View) -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currentListOfData = data.value.toList()
    private val compositeLiveDataDisposable = CompositeLiveDataDisposable()

    var data: NonNullLiveData<out List<TItem>> = data
        set(value) {
            compositeLiveDataDisposable.disposeAll()
            field = value
            field.observeWithDisposable(lifecycleOwner, listChangeObserver).disposeWith(compositeLiveDataDisposable)
        }

    private val listChangeObserver: (List<TItem>) -> Unit = { newList ->
        DiffUtil
            .calculateDiff(DiffCallback(currentListOfData, newList))
            .dispatchUpdatesTo(this)

        currentListOfData = newList.toList()
    }

    init {
        data.observe(lifecycleOwner, listChangeObserver)
    }

    override fun getItemId(position: Int): Long {

        val item = data.value.getOrNull(position)
            ?: throw IndexOutOfBoundsException("position is out of bounds, data size: ${data.value.size}, position: $position")

        return (item as? StableIdItem)?.getStableId()
            ?: throw IllegalStateException("RecyclerViewAdapter has stableIds enabled but its items do not implement StableIdItem")
    }

    override fun getItemCount(): Int = data.value.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val viewHolder = ViewHolder(recyclerViewItemFactory.makeView(parent.context, parent))
        onCreateItemView?.invoke(viewHolder.itemView)

        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        require(holder is ViewHolder)
        holder.itemView.tag = data.value[holder.adapterPosition]
        @Suppress("UNCHECKED_CAST")
        onBindItemView?.invoke(holder.itemView as TView, data.value[holder.adapterPosition], holder.adapterPosition)
    }
}

private class DiffCallback<T>(private val oldList: List<T>, private val newList: List<T>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition] === newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) = oldList[oldItemPosition] == newList[newItemPosition]
}

private class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
