package com.makokvak.iacompanion.util

import android.os.Handler
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import timber.log.Timber

data class LiveDataDisposable<T>(val liveData: LiveData<T>, val observer: Observer<in T>) {

    fun removeObserver() = liveData.removeObserver(observer)
    fun disposeWith(compositeDisposable: CompositeLiveDataDisposable) = compositeDisposable.addDisposable(
        this
    )
}

class CompositeLiveDataDisposable {

    private val disposables = mutableSetOf<LiveDataDisposable<*>>()
    private val handler = Handler()

    @Synchronized
    fun disposeAll() {
        handler.post {
            disposables.forEach {
                it.removeObserver()
            }

            disposables.clear()
        }
    }

    @Synchronized
    fun addDisposable(disposable: LiveDataDisposable<*>) {
        disposables.add(disposable)
    }

    operator fun plusAssign(disposable: LiveDataDisposable<*>) {
        addDisposable(disposable)
    }

    protected fun finalize() {
        Timber.d("finalized: $this")
        disposeAll()
    }
}

fun <T> NullableLiveData<T>.observeForeverWithDisposable(block: (T?) -> Unit): LiveDataDisposable<T> {
    val observer = this.observeForever(block)
    return LiveDataDisposable(this.javaLiveData, observer)
}

fun <T> NullableLiveData<T>.observeWithDisposable(
    lifecycleOwner: LifecycleOwner,
    block: (T?) -> Unit
): LiveDataDisposable<T> {
    val observer = this.observe(lifecycleOwner, block)
    return LiveDataDisposable(this.javaLiveData, observer)
}

fun <T : Any> NonNullLiveData<T>.observeForeverWithDisposable(block: (T) -> Unit): LiveDataDisposable<T> {
    val observer = this.observeForever(block)
    return LiveDataDisposable(this.javaLiveData, observer)
}

fun <T : Any> NonNullLiveData<T>.observeWithDisposable(
    lifecycleOwner: LifecycleOwner,
    block: (T) -> Unit
): LiveDataDisposable<T> {
    val observer = this.observe(lifecycleOwner, block)
    return LiveDataDisposable(this.javaLiveData, observer)
}
