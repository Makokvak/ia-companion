package com.makokvak.iacompanion.util

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateConsumer
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateObserver
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateObserverImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

abstract class BaseFragment<TViewModel : BaseViewModel>(
    @LayoutRes private val layoutResourceId: Int,
    viewModelClass: KClass<TViewModel>
) : Fragment(),
    CoroutineScope,
    ViewModelStateConsumer, ViewModelStateObserver by ViewModelStateObserverImpl() {

    private lateinit var job: Job

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    fun <T> NullableLiveData<T>.observe(block: (T?) -> Unit) =
        this.observe(viewLifecycleOwner, block)

    fun <T : Any> NonNullLiveData<T>.observe(block: (T) -> Unit) =
        this.observe(viewLifecycleOwner, block)

    val baseActivity
        get() = activity as? BaseActivity<*>

    protected val viewModel: TViewModel by lazy { getViewModel(viewModelClass) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(layoutResourceId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initializeViewModelStateObserver(viewLifecycleOwner, viewModel, this)
        initializeViews()
        initializeObservers()
        setUserActionListeners()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    open fun initializeViews() {
    }

    open fun initializeObservers() {
    }

    open fun setUserActionListeners() {
    }

    override fun onLoadingStateEnter() {
    }

    override fun onLoadingStateExit() {
    }

    override fun onInitialStateEnter() {
    }

    override fun onInitialStateExit() {
    }

    override fun onEmptyStateEnter() {
    }

    override fun onEmptyStateExit() {
    }

    override fun onReadyStateExit() {
    }

    override fun onReadyStateEnter() {
    }

    override fun onErrorStateEnter(error: Throwable) {
    }

    override fun onErrorStateExit() {
    }
}
