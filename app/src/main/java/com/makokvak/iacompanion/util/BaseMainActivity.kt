package com.makokvak.iacompanion.util

import androidx.annotation.LayoutRes
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import kotlin.reflect.KClass

abstract class BaseMainActivity<TViewModel : BaseViewModel>(@LayoutRes layoutRes: Int, viewModelClass: KClass<TViewModel>) :
    BaseActivity<TViewModel>(layoutRes, viewModelClass)
