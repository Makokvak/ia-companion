package com.makokvak.iacompanion.util

import android.app.Activity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import org.koin.core.KoinComponent
import java.io.Serializable

interface FragmentContainer {
    val fragmentContainerId: Int
    val containerFragmentManager: FragmentManager
}

abstract class NavigationTarget : KoinComponent {

    companion object {
        internal const val EXTRA_ARG_KEY = "nav_target_arg"
    }
}

abstract class FragmentTarget(internal val clazz: Class<out Fragment>) : NavigationTarget() {

    fun toAction(setting: NavigationActionSetting? = null): FragmentNavigationAction<out FragmentTarget> {
        return FragmentNavigationAction(this, setting ?: NavigationActionSetting())
    }

    operator fun unaryPlus() = this.toAction()
}

abstract class ArgFragmentTarget<TArg : Serializable?>(internal val clazz: Class<out Fragment>) : NavigationTarget() {

    fun toAction(setting: NavigationActionSetting? = null): ArgFragmentNavigationAction<TArg, ArgFragmentTarget<TArg>> {
        return ArgFragmentNavigationAction(this, setting ?: NavigationActionSetting())
    }

    operator fun unaryPlus() = this.toAction()

    @Suppress("UNCHECKED_CAST")
    fun retrieveArgument(fragment: Fragment): TArg {
        val arguments = fragment.arguments?.getSerializable(EXTRA_ARG_KEY) ?: throw IllegalStateException("Fragment was provided with no argument")
        return arguments as? TArg ?: throw AssertionError("Failed to cast argument to correct type")
    }
}

abstract class DialogFragmentTarget(internal val clazz: Class<out DialogFragment>) : NavigationTarget() {
    fun toAction() = DialogFragmentNavigationAction(this)
    operator fun unaryPlus() = this.toAction()
}

abstract class ActivityTarget(private val classProvisionContract: ClassProvisionContract) : NavigationTarget() {

    constructor(clazz: Class<out Activity>) : this(ClassProvisionContract.None) {
        _clazz = clazz
    }

    internal val clazz: Class<out Activity> get() = _clazz ?: throw IllegalStateException("Target class needed but not yet provided")

    private var _clazz: Class<out Activity>? = null

    open fun toAction() = ActivityNavigationAction(this)
    operator fun unaryPlus() = this.toAction()

    fun provideTargetClass(clazz: Class<out Activity>) {
        if (classProvisionContract != ClassProvisionContract.ExternalModule)
            throw IllegalStateException("External class provision is not allowed for this target")
        _clazz = clazz
    }
}

abstract class ArgActivityTarget<TArg : Serializable?>(private val classProvisionContract: ClassProvisionContract) : NavigationTarget() {

    constructor(clazz: Class<out Activity>) : this(ClassProvisionContract.None) {
        _clazz = clazz
    }

    internal val clazz: Class<out Activity> get() = _clazz ?: throw IllegalStateException("Target class needed but not yet provided")

    private var _clazz: Class<out Activity>? = null

    fun toAction() = ArgActivityNavigationAction(this)
    operator fun unaryPlus() = this.toAction()

    fun retrieveArgument(activity: Activity): TArg {
        val arg =
            activity.intent.getSerializableExtra(EXTRA_ARG_KEY) ?: throw IllegalStateException("Activity was provided with no argument")
        @Suppress("UNCHECKED_CAST")
        return arg as? TArg ?: throw AssertionError("Failed to cast argument to correct type")
    }

    fun provideTargetClass(clazz: Class<out Activity>) {
        if (classProvisionContract != ClassProvisionContract.ExternalModule)
            throw IllegalStateException("External class provision is not allowed for this target")
        _clazz = clazz
    }
}

enum class ClassProvisionContract {
    None, ExternalModule
}
