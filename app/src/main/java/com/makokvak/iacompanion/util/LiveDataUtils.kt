package com.makokvak.iacompanion.util

import androidx.annotation.MainThread
import androidx.lifecycle.*

interface CommonLiveData<T> {
    val javaLiveData: LiveData<T>

    val value: T?
        get() = javaLiveData.value
}

interface NullableLiveData<T> : CommonLiveData<T> {

    /**
     * @return Observer instance that has been created, useful if you later want to remove the observer
     */
    fun observe(owner: LifecycleOwner, block: (T?) -> Unit): Observer<T> =
        Observer<T> { block(it) }
            .also { observer -> javaLiveData.observe(owner, observer) }

    /**
     * @return Observer instance that has been created, useful if you later want to remove the observer
     */
    fun observeForever(block: (T?) -> Unit): Observer<T> =
        Observer<T> { block(it) }
            .also { observer -> javaLiveData.observeForever(observer) }
}

interface NonNullLiveData<T : Any> : CommonLiveData<T> {

    private class NullSafetyAssertionError : AssertionError("Value wrapped by safe live data was null")

    override val value: T
        get() = super.value ?: throw NullSafetyAssertionError()

    /**
     * @return Observer instance that has been created, useful if you later want to remove the observer
     */
    fun observe(owner: LifecycleOwner, block: (T) -> Unit): Observer<T> =
        Observer<T> {
            it ?: throw NullSafetyAssertionError()
            block(it)
        }.also { observer ->
            javaLiveData.observe(owner, observer)
        }

    /**
     * @return Observer instance that has been created, useful if you later want to remove the observer
     */
    fun observeForever(block: (T) -> Unit): Observer<T> =
        Observer<T> {
            it ?: throw NullSafetyAssertionError()
            block(it)
        }.also { observer ->
            javaLiveData.observeForever(observer)
        }
}

open class MutableNullableLiveData<T>(initialValue: T? = null) : NullableLiveData<T> {

    protected open val mutableLiveData = MutableLiveData<T>().apply {
        if (initialValue != null) value = initialValue
    }

    override val javaLiveData: LiveData<T> get() = mutableLiveData

    override var value: T?
        get() = super.value
        set(value) {
            mutableLiveData.value = value
        }

    fun postValue(value: T?) = mutableLiveData.postValue(value)
}

open class MutableNonNullLiveData<T : Any>(initialValue: T) : NonNullLiveData<T> {

    protected open val mutableLiveData = MutableLiveData<T>().apply {
        value = initialValue
    }

    override val javaLiveData: LiveData<T> get() = mutableLiveData

    override var value: T
        get() = super.value
        set(value) {
            mutableLiveData.value = value
        }

    fun postValue(value: T) = mutableLiveData.postValue(value)
}

class MediatorNonNullLiveData<T : Any>(initialValue: T) : MutableNonNullLiveData<T>(initialValue) {

    private val mediatorLiveData = MediatorLiveData<T>().apply {
        value = initialValue
    }

    override val mutableLiveData: MutableLiveData<T> = mediatorLiveData

    @MainThread
    fun <S : Any> addSource(source: NonNullLiveData<S>, onChanged: (S) -> Unit) =
        mediatorLiveData.addSource(source.javaLiveData, onChanged)

    @MainThread
    fun <S : Any> removeSource(toRemote: NonNullLiveData<S>) =
        mediatorLiveData.removeSource(toRemote.javaLiveData)
}

class MediatorNullableLiveData<T>(initialValue: T? = null) : MutableNullableLiveData<T>(initialValue) {

    private val mediatorLiveData = MediatorLiveData<T>().apply {
        if (initialValue != null) value = initialValue
    }

    override val mutableLiveData: MutableLiveData<T> = mediatorLiveData

    @MainThread
    fun <S> addSource(source: NullableLiveData<S>, onChanged: (S) -> Unit) =
        mediatorLiveData.addSource(source.javaLiveData, onChanged)

    @MainThread
    fun <S> removeSource(toRemote: NullableLiveData<S>) =
        mediatorLiveData.removeSource(toRemote.javaLiveData)
}

@MainThread
fun <T : Any, S : Any> NonNullLiveData<T>.map(mapFunction: (T) -> S): NonNullLiveData<S> =
    MediatorNonNullLiveData(mapFunction(this.value))
        .also { mediator ->
            mediator.addSource(this) { x -> mediator.value = mapFunction(x) }
        }

@MainThread
fun <T, S> NullableLiveData<T>.map(mapFunction: (T?) -> S): NullableLiveData<S> =
    MediatorNullableLiveData(mapFunction(this.value))
        .also { mediator ->
            mediator.addSource(this) { x -> mediator.value = mapFunction(x) }
        }

fun <T : Any> nonNullLiveDataOf(value: T): NonNullLiveData<T> =
    MutableNonNullLiveData(value)

fun <T> nullableLiveDataOf(value: T): NullableLiveData<T> =
    MutableNullableLiveData(value)
