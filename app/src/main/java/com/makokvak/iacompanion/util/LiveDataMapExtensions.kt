package com.makokvak.iacompanion.util

fun <K, V> MutableNonNullLiveData<Map<K, V>>.add(key: K, mapValue: V) {

    val listCopy = value.toMutableMap()
    listCopy[key] = mapValue
    value = listCopy
}

fun <K, V> MutableNonNullLiveData<Map<K, List<V>>>.insertValueToValueList(key: K, mapValue: V) {

    val listCopy = value.toMutableMap()

    if(!listCopy.containsKey(key))
        listCopy[key] = listOf(mapValue)
    else
        listCopy[key] = listCopy[key]!!.toMutableList().apply { add(mapValue) }

    value = listCopy
}
