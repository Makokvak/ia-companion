package com.makokvak.iacompanion.util.util

interface StableIdItem {
    fun getStableId(): Long
}
