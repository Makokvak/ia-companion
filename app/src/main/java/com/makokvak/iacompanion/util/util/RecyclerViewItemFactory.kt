package com.makokvak.iacompanion.util.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

interface RecyclerViewItemFactory<TView> {
    fun makeView(context: Context, parent: ViewGroup): TView
}

class LayoutResourceRecyclerViewItemFactory(@LayoutRes private val layoutRes: Int) : RecyclerViewItemFactory<View> {
    override fun makeView(context: Context, parent: ViewGroup): View =
        LayoutInflater.from(context).inflate(layoutRes, parent, false)
}
