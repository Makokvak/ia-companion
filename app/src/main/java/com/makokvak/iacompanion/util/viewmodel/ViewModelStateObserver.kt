package com.makokvak.iacompanion.util.viewmodel

import androidx.lifecycle.LifecycleOwner

interface ViewModelStateObserver {
    fun initializeViewModelStateObserver(
        lifecycleOwner: LifecycleOwner,
        viewModel: BaseViewModel,
        stateConsumer: ViewModelStateConsumer
    )
}
