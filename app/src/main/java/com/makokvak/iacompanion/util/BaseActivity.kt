package com.makokvak.iacompanion.util

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateConsumer
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateObserver
import com.makokvak.iacompanion.util.viewmodel.ViewModelStateObserverImpl
import org.koin.androidx.viewmodel.ext.android.getViewModel
import kotlin.reflect.KClass

abstract class BaseActivity<TViewModel : BaseViewModel>(
    @LayoutRes private val layoutResourceId: Int,
    viewModelClass: KClass<TViewModel>
) : AppCompatActivity(), FragmentContainer,
    ViewModelStateConsumer, ViewModelStateObserver by ViewModelStateObserverImpl() {

    override val containerFragmentManager: FragmentManager
        get() = supportFragmentManager

    override val fragmentContainerId: Int
        get() = throw UnsupportedOperationException("Attempted to navigate to a fragment but no fragment container ID was provided")

    fun <T> NullableLiveData<T>.observe(block: (T?) -> Unit) = this.observe(this@BaseActivity, block)
    fun <T : Any> NonNullLiveData<T>.observe(block: (T) -> Unit) = this.observe(this@BaseActivity, block)

    protected val viewModel: TViewModel by lazy { getViewModel(viewModelClass) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(layoutResourceId)
        initializeViewModelStateObserver(this, viewModel, this)
        initializeViews()
        initializeObservers()
        setUserActionListeners()
    }

    open fun initializeViews() {
    }

    open fun initializeObservers() {
    }

    open fun setUserActionListeners() {
    }

    override fun onLoadingStateEnter() {
    }

    override fun onLoadingStateExit() {
    }

    override fun onInitialStateEnter() {
    }

    override fun onInitialStateExit() {
    }

    override fun onEmptyStateEnter() {
    }

    override fun onEmptyStateExit() {
    }

    override fun onReadyStateExit() {
    }

    override fun onReadyStateEnter() {
    }

    override fun onErrorStateEnter(error: Throwable) {
    }

    override fun onErrorStateExit() {
    }
}
