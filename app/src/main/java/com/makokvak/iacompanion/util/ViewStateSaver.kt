package com.makokvak.iacompanion.util

import android.os.Bundle
import android.os.Parcelable

class ViewStateSaver<T : Parcelable>(initialState: T) {
    companion object {
        private const val BUNDLE_KEY = "state_saver_bundle_key"
        private const val BUNDLE_SUPER_STATE_KEY = "state_saver_super_state_key"
    }

    var state: T = initialState
        private set

    fun saveState(superState: Parcelable?) =
        Bundle().apply {
            superState?.let { putParcelable(BUNDLE_SUPER_STATE_KEY, it) }
            putParcelable(BUNDLE_KEY, state)
        }

    fun restoreState(parcelable: Parcelable, block: (T) -> Unit): Parcelable? {
        val bundle = parcelable as? Bundle
            ?: throw IllegalArgumentException("Incorrect type of provided parcelable, must be of type Bundle")

        @Suppress("RemoveExplicitTypeArguments")
        state = bundle.getParcelable<T>(BUNDLE_KEY) as T
        block.invoke(state)
        return bundle.getParcelable(BUNDLE_SUPER_STATE_KEY)
    }
}
