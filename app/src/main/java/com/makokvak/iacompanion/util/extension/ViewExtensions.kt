package com.makokvak.iacompanion.util.extension

import android.view.View
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.makokvak.iacompanion.util.util.dpToPx

fun View.setVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.setGone() {
    visibility = View.GONE
}

fun View.setVisible() {
    visibility = View.VISIBLE
}

fun View.setInvisible() {
    visibility = View.INVISIBLE
}

inline fun View.onClick(crossinline block: () -> Unit) {
    setOnClickListener {
        block()
    }
}

fun BottomNavigationView.selectMenuItem(actionId: Int) {
    menu.findItem(actionId)?.isChecked = true
}

fun View.setHeightBoundByAspect(width: Int, heightAspectMultiplier: Float) {
    this.layoutParams = this.layoutParams.apply { height = (width * heightAspectMultiplier).toInt() + dpToPx(28) }
}

fun View.setWidthBoundByAspect(height: Int, heightAspectMultiplier: Float) {
    this.layoutParams = this.layoutParams.apply { width = ((height - dpToPx(28)) / heightAspectMultiplier).toInt() }
}