package com.makokvak.iacompanion.util.viewmodel

import androidx.lifecycle.LifecycleOwner

class ViewModelStateObserverImpl : ViewModelStateObserver {

    override fun initializeViewModelStateObserver(
        lifecycleOwner: LifecycleOwner,
        viewModel: BaseViewModel,
        stateConsumer: ViewModelStateConsumer
    ) {

        viewModel.state.observe(lifecycleOwner) {
            when (it) {
                is InitialState -> {
                    stateConsumer.onInitialStateEnter()
                }
                is LoadingState -> {
                    stateConsumer.onLoadingStateEnter()
                }
                is EmptyState -> {
                    stateConsumer.onEmptyStateEnter()
                }
                is ReadyState -> {
                    stateConsumer.onReadyStateEnter()
                }
                is ErrorState -> {
                    stateConsumer.onErrorStateEnter(it.error)
                }
            }
        }

        viewModel.previousState.observe(lifecycleOwner) {
            when (it) {
                is InitialState -> {
                    stateConsumer.onInitialStateExit()
                }
                is LoadingState -> {
                    stateConsumer.onLoadingStateExit()
                }
                is EmptyState -> {
                    stateConsumer.onEmptyStateExit()
                }
                is ReadyState -> {
                    stateConsumer.onReadyStateExit()
                }
                is ErrorState -> {
                    stateConsumer.onErrorStateExit()
                }
            }
        }
    }
}
