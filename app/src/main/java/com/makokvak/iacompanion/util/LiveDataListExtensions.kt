package com.makokvak.iacompanion.util

/**
 * Adds an item to the end (if not specified otherwise) of the list wrapped by this live data and notifies it about changes.
 *
 * Not optimized for performance, creates a shallow copy of underlying list.
 * If you need better performance do not use this method and add the items manually to the list.
 *
 * @param item item to add
 * @param position position to add item to, default position is the end of the list
 */
fun <T> MutableNonNullLiveData<List<T>>.add(item: T, position: Int = this.value.size) {

    val listCopy = value.toMutableList()
    listCopy.add(position, item)
    value = listCopy
}

/**
 * Adds a collection of items to the end (if not specified otherwise) of the list wrapped by this live data and notifies it about changes.
 *
 * Not optimized for performance, creates a shallow copy of underlying list.
 * If you need better performance do not use this method and add the items manually to the list.
 *
 * @param itemsToAdd items to add
 * @param position position to add items to, default position is the end of the list
 */
fun <T> MutableNonNullLiveData<List<T>>.addRange(itemsToAdd: Collection<T>, position: Int = this.value.size) {

    val listCopy = value.toMutableList()
    listCopy.addAll(position, itemsToAdd)
    value = listCopy
}

/**
 * Removes an item from the list wrapped by this live data and notifies it about changes.
 *
 * Not optimized for performance, creates a shallow copy of underlying list.
 * If you need better performance do not use this method and add the items manually to the list.
 *
 * @param item item to remove from a list
 */
fun <T> MutableNonNullLiveData<List<T>>.remove(item: T) {

    val position = value.indexOf(item)
    check(position >= 0) { "Item $item was not found in the list" }

    removeAt(position)
}

/**
 * Removes an item at a specified position from the list wrapped by this live data and notifies it about changes.
 *
 * Not optimized for performance, creates a shallow copy of underlying list.
 * If you need better performance do not use this method and add the items manually to the list.
 *
 * @param position position of item to remove, must be equal to or greater than zero and less than the size of the list
 * @return
 */
fun <T> MutableNonNullLiveData<List<T>>.removeAt(position: Int): T {

    require(position < value.size && position >= 0) { "Position is out of bounds, position: $position, size: ${value.size}" }

    val listCopy = value.toMutableList()
    val removedItem = listCopy.removeAt(position)
    value = listCopy

    return removedItem
}

/**
 * Clears the list wrapped by this live data and notifies it about changes.
 */
fun <T> MutableNonNullLiveData<List<T>>.clear() {
    value = listOf()
}

/**
 * Sets new list wrapped by this live data and notifies it about changes.
 */
fun <T> MutableNonNullLiveData<List<T>>.setList(itemsToAdd: List<T>) {
    value = itemsToAdd
}
