package com.makokvak.iacompanion.util.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

class GridItemSpacingDecoration(val dp: Int = 0,
                                val edgeOffsetDp: Int? = null,
                                val leftOffsetDp: Int? = null,
                                val rightOffsetDp: Int? = null,
                                val isVertical: Boolean = true) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val adapter = parent.adapter ?: return

        val spanCount = ((parent.layoutManager as? GridLayoutManager)?.spanCount)
            ?: ((parent.layoutManager as? StaggeredGridLayoutManager)?.spanCount)
            ?: 1

        val spanIndex = (view.layoutParams as? GridLayoutManager.LayoutParams)?.spanIndex
            ?: (view.layoutParams as? StaggeredGridLayoutManager.LayoutParams)?.spanIndex
            ?: 0

        val position = parent.getChildLayoutPosition(view)

        val px = dpToPx(dp)
        val pxHalf = (px * 0.5f).toInt()
        val pxEdge = dpToPx(edgeOffsetDp ?: dp)

        //val pxTopOffset = if (topOffsetDp == null) pxEdge else WindowUtils.dpToPx(topOffsetDp)
        //val pxBottomOffset = if (bottomOffsetDp == null) pxEdge else WindowUtils.dpToPx(bottomOffsetDp)
        val pxLeftOffset = if (leftOffsetDp == null) pxEdge else dpToPx(leftOffsetDp)
        val pxRightOffset = if (rightOffsetDp == null) pxEdge else dpToPx(rightOffsetDp)

        //val isFirstRow = position < spanCount
        //val isLastRow = position >= (adapter.itemCount - spanCount)

        if(isVertical) {

            val isFirstRow = position < spanCount
            val isLastRow = position >= (adapter.itemCount - spanCount)
            val isFirstColumn = spanIndex == 0
            val isLastColumn = spanIndex == (spanCount - 1)

            outRect.left = if (isFirstColumn) pxLeftOffset else pxHalf
            outRect.right = if (isLastColumn) pxRightOffset else pxHalf

            //Log.d(javaClass.simpleName, "position: $position, isFirstColumn: $isFirstColumn, isLastColumn: $isLastColumn")

            outRect.top = px
            //outRect.bottom = if(isLastRow) px else 0

        }
        else {

            val isFirstColumn = position < spanCount
            val isLastColumn = position >= (adapter.itemCount - spanCount)
            val isFirstRow = spanIndex == 0
            val isLastRow = spanIndex == (spanCount - 1)

            outRect.left = if (isFirstColumn) pxLeftOffset else pxHalf
            outRect.right = if (isLastColumn) pxRightOffset else pxHalf

            //Log.d(javaClass.simpleName, "position: $position, isFirstColumn: $isFirstColumn, isLastColumn: $isLastColumn")

            outRect.top = if(isFirstRow) px else px - dpToPx(6)
            outRect.bottom = 0
        }

        //Log.d(javaClass.simpleName, "layoutPos: $layoutPos, adapterPos: $adapterPos, isFirstRow: $isFirstRow, isLastRow: $isLastRow, indexOfChild: $indexOfChild")

    }
}
