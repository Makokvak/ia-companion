package com.makokvak.iacompanion.util

import androidx.annotation.MainThread
import androidx.annotation.Nullable
import androidx.lifecycle.*
import java.util.concurrent.atomic.AtomicBoolean

internal class PendingObserver<T>(val observer: Observer<in T>) {
    val pending = AtomicBoolean(false)
}

/**
 * A lifecycle-aware observable that sends only new updates after subscription, used for events like
 * navigation and Snackbar messages.
 *
 *
 * This avoids a common problem with events: on configuration change (like rotation) an update
 * can be emitted if the observer is active. This LiveData only calls the observable if there's an
 * explicit call to setValue() or call().
 *
 *
 * Note that only one observer is going to be notified of changes.
 */
class MutableSingleEventLiveData<T> : MutableLiveData<T>() {

    private val registeredObserversMap = mutableMapOf<Observer<in T>, PendingObserver<in T>>()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        super.observe(owner, mapAndRegisterObserver(observer))
    }

    override fun observeForever(observer: Observer<in T>) {
        super.observeForever(mapAndRegisterObserver(observer))
    }

    override fun removeObservers(owner: LifecycleOwner) {
        throw NotImplementedError()
    }

    override fun removeObserver(observer: Observer<in T>) {
        val observerToRemove = registeredObserversMap.remove(observer)?.observer
        @Suppress("UNCHECKED_CAST") val param = (observerToRemove ?: observer) as Observer<in T>
        super.removeObserver(param)
    }

    @MainThread
    override fun setValue(@Nullable t: T?) {
        registeredObserversMap.values.forEach {
            it.pending.set(true)
        }
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun call() {
        value = null
    }

    private fun mapAndRegisterObserver(observer: Observer<in T>): Observer<T> {

        val observerToRegister = Observer<T> { t ->
            val pending = registeredObserversMap[observer]?.pending
                ?: throw AssertionError("Observer not added to map")

            if (pending.compareAndSet(true, false))
                observer.onChanged(t)
        }

        registeredObserversMap[observer] = PendingObserver(observerToRegister)
        return observerToRegister
    }
}
