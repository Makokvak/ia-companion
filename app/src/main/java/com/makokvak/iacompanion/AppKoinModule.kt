package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.*
import com.makokvak.iacompanion.model.packs.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

object AppKoinModule {

    private val instance = module {

        factory { SelectionHandler() }
        factory { AgendaDealer(get()) }
        factory { ShopDealer(get(), get()) }
        factory { MissionDealer(get(), get()) }
        single { SessionManager(get()) }

        single { CoreGameResources() }
        single { JabbasRealmResources() }
        single { BespinGambitResources() }
        single { TyrantsOfLothalResources() }
        single { HeartOfTheEmpireResources() }
        single { AgentBlaiseVillainPack() }
        single { AhsokaAllyPack() }
        single { BosskVillainPack() }
        single { ChewbaccaAllyPack() }
        single { GrandInquisitorVillainPack() }
        single { GreedoVillainPack() }
        single { HanSoloAllyPack() }
        single { HeraAllyPack() }
        single { JabbaVillainPack() }
        single { JawaVillainPack() }
        single { LandoAllyPack() }
        single { LukeAllyPack() }
        single { MaulVillainPack() }
        single { ObiWanAllyPack() }
        single { PalpatineVillainPack() }
        single { RoyalGuardChampionVillainPack() }
        single { ThrawnVillainPack() }
        single { HondoVillainPack() }
        single { SabineAllyPack() }
        single { ComponentResourceMap(get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get(), get()) }
        single { SessionSerializer(androidContext()) }

        viewModel { MainActivityViewModel() }
        viewModel { DetailViewModel(androidContext(), get()) }
        viewModel { MainMenuViewModel() }
        viewModel { ComponentListViewModel(get(), get(), get()) }
        viewModel { ResourceListViewModel(get(), get(), get(), get()) }
        viewModel { ResourceTypeMenuViewModel() }
        viewModel { CampaignStageViewModel(get()) }
        viewModel { MissionStageViewModel(get()) }
        viewModel { OwnedItemsResourceListViewModel(get(), get(), get(), get()) }
        viewModel { OwnedVillainsResourceListViewModel(get(), get(), get(), get()) }
        viewModel { OwnedAlliesResourceListViewModel(get(), get(), get(), get()) }
        viewModel { BuyHeroSkillsResourceListViewModel(get(), get(), get(), get()) }
        viewModel { EarnedRewardsResourceListViewModel(get(), get(), get(), get()) }
        viewModel { OngoingAgendaResourceListViewModel(get(), get(), get(), get()) }
        viewModel { SessionListViewModel(get(), get()) }
        viewModel { ChooseOutcomeViewModel(get(), get(), get(), get()) }
        viewModel { SearchResourceListViewModel(androidContext(), get(), get(), get(), get()) }
        viewModel { ImperialEarnedRewardsResourceListViewModel(get(), get(), get(), get()) }
    }

    fun start() {
        loadKoinModules(instance)
        loadKoinModules(Navigation.koinModule)
    }
}
