package com.makokvak.iacompanion

import android.content.Context
import androidx.lifecycle.LiveData
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.ComponentResourceMap
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.*
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import java.util.regex.Pattern

class OwnedItemsResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.ownedItems.value

        compositeLiveDataDisposable += sessionManager.session.ownedItems.observeForeverWithDisposable {
            _data.value = sessionManager.session.ownedItems.value
        }
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class OngoingAgendaResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.ongoingAgenda.value
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class BuyHeroSkillsResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    fun setHero(hero: Resource) {
        if(hero is Resource.ResourceWithReferences.Hero) {
            _data.value = hero.skillDeck
        }
        else {
            _data.value = (hero as Resource.ImperialClassDeck).cards
        }
    }

    fun hasSkill(skill: Resource): Boolean {
        return if(skill is Resource.HeroSkill) {
            sessionManager.session.heroSkills.value.values.flatten().contains(skill)
        } else {
            sessionManager.session.imperialSkills.value.contains(skill)
        }
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }

    fun isMissionActive() =
        sessionManager.session.activeMission.value != null
}

class EarnedRewardsResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.earnedRewards.value
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class ImperialEarnedRewardsResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.earnedImperialRewards.value
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class OwnedVillainsResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.ownedVillains.value
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class OwnedAlliesResourceListViewModel(
    componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    init {
        _data.value = sessionManager.session.ownedAllies.value
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

class SearchResourceListViewModel(
    private val context: Context,
    private val componentResourceMap: ComponentResourceMap,
    sessionManager: SessionManager,
    dealer: MissionDealer,
    selectionHandler: SelectionHandler
) : ResourceListViewModel(componentResourceMap, sessionManager, selectionHandler, dealer) {

    val searchString = MutableNonNullLiveData("")
    private val dataForSearch = componentResourceMap.getAllResourcesAndNestedResources()

    fun search(string: String) {

        if(searchString.value == string || string.isBlank()) return

        searchString.value = string

        if(string.length < 3) return

        val pattern = Pattern.compile(
            string,
            Pattern.CASE_INSENSITIVE or
                    Pattern.LITERAL or
                    Pattern.UNICODE_CASE
        )

        _data.value =
            dataForSearch
                .filter {
                    val name = context.getString(it.displayableParams.name)
                    val matcher = pattern.matcher(name)
                    return@filter matcher.find()
                }
    }

    // Prevent parent to automatically load resources
    override fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) { }
}

open class ResourceListViewModel(
    private val componentResourceMap: ComponentResourceMap,
    protected val sessionManager: SessionManager,
    private val selectionHandler: SelectionHandler,
    private val dealer: MissionDealer
) : BaseViewModel() {

    fun onCampaignSelected() {
        sessionManager.session.activeMission.value = sessionManager.session.campaign.value!!.firstMission
    }

    fun filterData(by: (Resource) -> Boolean) {
        _data.value = _data.value.filter(by)
    }

    fun buildSideMissionDeck() {
        dealer.makeSideMissionDeck(selectedData.value.map { it as Resource.Mission.Side })
    }

    @Suppress("PropertyName")
    protected val _data = MutableNonNullLiveData(
        componentResourceMap.getAllResources()
    )
    val data: NonNullLiveData<List<Resource>> = _data

    val selectedData = selectionHandler.selectedIndices.map {
        it.map { index -> data.value[index] }
    }

    fun getCurrentMission() =
        sessionManager.session.activeMission.value

    fun setMaxSelection(count: Int) {
        selectionHandler.maxItemsSelected = count
    }

    open fun setResourceTypeAndComponent(resourceType: ResourceType, component: Component) {

        _data.value = when (resourceType) {
            ResourceType.AllTypes -> componentResourceMap.getByComponent(component)
            ResourceType.Items -> componentResourceMap.getByResourceTypeAndComponent<Resource.ResourceWithReferences.Item>(
                component
            )
            ResourceType.Supplies -> componentResourceMap.getByResourceTypeAndComponent<Resource.ResourceWithReferences.Supply>(
                component
            )
            ResourceType.SideMissions -> componentResourceMap.getByResourceTypeAndComponent<Resource.Mission.Side>(
                component
            )
            ResourceType.StoryMissions -> componentResourceMap.getByResourceTypeAndComponent<Resource.Mission.Story>(
                component
            )
            ResourceType.AgendaSets -> componentResourceMap.getByResourceTypeAndComponent<Resource.AgendaSet>(
                component
            )
            ResourceType.ImperialClassDecks -> componentResourceMap.getByResourceTypeAndComponent<Resource.ImperialClassDeck>(
                component
            )
            ResourceType.Deployables -> componentResourceMap.getByResourceTypeAndComponent<Resource.ResourceWithReferences.Deployable>(
                component
            )
            ResourceType.Allies -> componentResourceMap.getByResourceTypeAndComponent<Resource.Ally>(
                component
            )
            ResourceType.Heroes -> componentResourceMap.getByResourceTypeAndComponent<Resource.ResourceWithReferences.Hero>(
                component
            )
            ResourceType.Campaign -> componentResourceMap.getByResourceTypeAndComponent<Resource.Campaign>(
                component
            )
            ResourceType.Agendas -> componentResourceMap.getByResourceTypeAndComponent<Resource.Agenda>(
                component
            )
            ResourceType.Rewards -> componentResourceMap.getByResourceTypeAndComponent<Resource.ResourceWithReferences.Reward>(
                component
            )
            ResourceType.Skills -> componentResourceMap.getByResourceTypeAndComponent<Resource.HeroSkill>(
                component
            )
        }
    }

    fun toggleItemSelection(index: Int) {
        selectionHandler.toggleItemSelection(index)
    }

    fun selectAll() {
        for(index in data.value.indices)
            selectionHandler.selectItem(index)
    }

    fun resetSelection() {
        selectionHandler.clearAllSelections()
    }

    fun confirmSelection(resourceType: ResourceType) {
        when(resourceType) {
            ResourceType.AgendaSets -> {
                sessionManager.session.chosenAgendaDecks.value =
                    selectedData.value.map { it as Resource.AgendaSet }
            }
            ResourceType.ImperialClassDecks -> {
                sessionManager.session.chosenImperialClassDeck.value =
                    selectedData.value.map { it as Resource.ImperialClassDeck }.first()
            }
            ResourceType.Heroes -> {
                sessionManager.session.chosenHeroes.value =
                    selectedData.value.map { it as Resource.ResourceWithReferences.Hero }
            }
            ResourceType.Campaign -> {
                sessionManager.session.campaign.value =
                    selectedData.value.map { it as Resource.Campaign }.first()
            }
            ResourceType.SideMissions -> {
            }
            else -> throw IllegalArgumentException()
        }
    }
}
