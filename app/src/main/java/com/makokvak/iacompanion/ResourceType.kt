package com.makokvak.iacompanion

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

enum class ResourceType(@StringRes val title: Int, @DrawableRes val imageResourceId: Int, val requiresNameShown: Boolean = false, val heightAspectRatioMultiplier: Float = 1.5723f) {

    AllTypes(R.string.all_types, R.mipmap.core_game, true),
    Items(R.string.items, R.mipmap.core_game, true),
    Supplies(R.string.supplies, R.mipmap.core_game, true),
    SideMissions(R.string.side_missions, R.mipmap.core_game, true),
    StoryMissions(R.string.story_missions, R.mipmap.core_game, true),
    AgendaSets(R.string.agenda_sets, R.mipmap.core_game, true),
    Agendas(R.string.agendas, R.mipmap.core_game, true),
    ImperialClassDecks(R.string.imperial_class_decks, R.mipmap.core_game, true),
    Deployables(R.string.imperial_deployables, R.mipmap.core_game, true),
    Allies(R.string.allies, R.mipmap.core_game, true),
    Heroes(R.string.heroes, R.mipmap.core_game, true, 0.802f),
    Rewards(R.string.rewards, R.mipmap.core_game, true),
    Skills(R.string.skills, R.mipmap.core_game, true),
    Campaign(R.string.campaigns, R.mipmap.core_game, true, 1f)
}
