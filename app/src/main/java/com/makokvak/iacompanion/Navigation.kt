package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.*
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.dsl.module
import java.io.Serializable

object Navigation : KoinComponent {

    val koinModule = module {
        single { MainActivityTarget() }
        single { MainMenuTarget() }
        single { ComponentListTarget() }
        single { ResourceListTarget() }
        single { ResourceTypeMenuTarget() }
        single { DetailTarget() }
        single { SelectComponentListTarget() }
        single { SelectAgendaSetTarget() }
        single { SelectImperialClassDeckTarget() }
        single { SelectHeroesTarget() }
        single { CampaignStageTarget() }
        single { SelectCampaignTarget() }
        single { MissionStageTarget() }
        single { TriggerableMissionDetailTarget() }
        single { ShopItemDetailTarget() }
        single { OwnedItemDetailTarget() }
        single { BuyAgendaDetailTarget() }
        single { BuyHeroSkillDetailTarget() }
        single { GameStageTarget() }
        single { OwnedItemsResourceListTarget() }
        single { OwnedVillainsResourceListTarget() }
        single { OwnedAlliesResourceListTarget() }
        single { EarnedRewardsResourceListTarget() }
        single { OngoingAgendaResourceListTarget() }
        single { BuyHeroSkillsResourceListTarget() }
        single { ChooseOutcomeTarget() }
        single { SessionsTarget() }
        single { SelectSideMissionsTarget() }
        single { SearchResourceListTarget() }
        single { ImperialEarnedRewardsResourceListTarget() }
    }

    class ChooseOutcomeTarget : FragmentTarget(ChooseOutcomeFragment::class.java) {
        val toCampaignStage get() = +get<CampaignStageTarget>()
    }

    class MainActivityTarget : ActivityTarget(MainActivity::class.java) {
        val toMainMenu get() = get<MainMenuTarget>().toAction(NavigationActionSetting.initialRoot())
    }

    class MainMenuTarget : FragmentTarget(MainMenuFragment::class.java) {
        val toComponentList get() = get<ComponentListTarget>().toAction(AddToBackStackSetting().apply { value = true })
        val toSessions get() = get<SessionsTarget>().toAction(AddToBackStackSetting().apply { value = true })
        val toSearch get() = get<SearchResourceListTarget>().toAction(AddToBackStackSetting().apply { value = true })
    }

    class SessionsTarget : FragmentTarget(SessionListFragment::class.java) {
        val toNewSession = get<SelectComponentListTarget>().toAction(NavigationActionSetting.root())
        val toSessionCampaign = get<CampaignStageTarget>().toAction(NavigationActionSetting.root())
        val toSessionMission = get<MissionStageTarget>().toAction(NavigationActionSetting.root())
    }

    class ComponentListTarget : FragmentTarget(ComponentListFragment::class.java) {
        val toResourceList = +get<ResourceTypeMenuTarget>()
    }

    class SearchResourceListTarget : ArgFragmentTarget<ResourceListParams>(SearchResourceListFragment::class.java) {
    }

    class ResourceTypeMenuTarget : ArgFragmentTarget<Component>(ResourceTypeMenuFragment::class.java) {
        val toResourceList = +get<ResourceListTarget>()
    }

    class ResourceListTarget : ArgFragmentTarget<ResourceListParams>(ResourceListFragment::class.java) {
        val toDetail = +get<DetailTarget>()
    }

    class DetailTarget : ArgFragmentTarget<Resource>(DetailFragment::class.java) {
        val toDetail get() = get<DetailTarget>().toAction(NavigationActionSetting.doNotReuseBackstackFragment())
        val toMission get() = +get<MissionStageTarget>()
    }

    class TriggerableMissionDetailTarget : ArgFragmentTarget<Resource>(TriggerableMissionDetailFragment::class.java) {
        val toMission get() = +get<MissionStageTarget>()
    }

    class ShopItemDetailTarget : ArgFragmentTarget<Resource>(ShopItemDetailFragment::class.java)
    class OwnedItemDetailTarget : ArgFragmentTarget<Resource>(OwnedItemDetailFragment::class.java)
    class BuyAgendaDetailTarget : ArgFragmentTarget<Resource>(BuyAgendaDetailFragment::class.java)
    class BuyHeroSkillDetailTarget : ArgFragmentTarget<Resource>(BuyHeroSkillDetailFragment::class.java)

    class SelectAgendaSetTarget : FragmentTarget(SelectAgendaSetFragment::class.java) {
        val toNext get() = +get<SelectImperialClassDeckTarget>()
    }

    class SelectCampaignTarget : FragmentTarget(SelectCampaignFragment::class.java) {
        val toNext get() = +get<SelectAgendaSetTarget>()
    }

    class SelectComponentListTarget : FragmentTarget(SelectComponentListFragment::class.java) {
        val toNext = +get<SelectCampaignTarget>()
    }

    class SelectImperialClassDeckTarget : FragmentTarget(SelectImperialClassDeckFragment::class.java) {
        val toNext get() = +get<SelectSideMissionsTarget>()
    }

    class SelectHeroesTarget : FragmentTarget(SelectHeroesFragment::class.java) {
        val toCampaignStage get() = +get<CampaignStageTarget>()
        val toMissionStage get() = +get<MissionStageTarget>()
    }

    class SelectSideMissionsTarget : FragmentTarget(SelectSideMissionsFragment::class.java) {
        val toNext get() = +get<SelectHeroesTarget>()
    }

    class CampaignStageTarget : FragmentTarget(CampaignStageFragment::class.java) {
        val toShopItemDetail = +get<ShopItemDetailTarget>()
        val toBuyAgendaDetail = +get<BuyAgendaDetailTarget>()
        val toBuyHeroSkillsResourceList = +get<BuyHeroSkillsResourceListTarget>()
        val toMissionDetail = +get<TriggerableMissionDetailTarget>()
    }

    class MissionStageTarget : ArgFragmentTarget<Resource.Mission>(MissionStageFragment::class.java) {
        val toDetail = +get<DetailTarget>()
        val toChooseOutcome = +get<ChooseOutcomeTarget>()
    }

    class OwnedItemsResourceListTarget : ArgFragmentTarget<ResourceListParams>(OwnedItemsResourceListFragment::class.java) {
        val toDetail = +get<OwnedItemDetailTarget>()
    }
    class OwnedAlliesResourceListTarget : ArgFragmentTarget<ResourceListParams>(OwnedAlliesResourceListFragment::class.java)
    class OwnedVillainsResourceListTarget : ArgFragmentTarget<ResourceListParams>(OwnedVillainsResourceListFragment::class.java)
    class EarnedRewardsResourceListTarget : ArgFragmentTarget<ResourceListParams>(EarnedRewardsResourceListFragment::class.java)
    class ImperialEarnedRewardsResourceListTarget : ArgFragmentTarget<ResourceListParams>(ImperialEarnedRewardsResourceListFragment::class.java)
    class OngoingAgendaResourceListTarget : ArgFragmentTarget<ResourceListParams>(OngoingAgendaResourceListFragment::class.java)

    class BuyHeroSkillsResourceListTarget : ArgFragmentTarget<Resource>(BuyHeroSkillsResourceListFragment::class.java) {
        val toBuyHeroSkillDetail = +get<BuyHeroSkillDetailTarget>()
        val toDetail = +get<DetailTarget>()
    }

    class GameStageTarget : FragmentTarget(GameStageFragment::class.java) {
        val toOwnedItems = +get<OwnedItemsResourceListTarget>()
        val toOwnedVillains = +get<OwnedVillainsResourceListTarget>()
        val toOwnedAllies = +get<OwnedAlliesResourceListTarget>()
        val toEarnedRewards = +get<EarnedRewardsResourceListTarget>()
        val toImoperialEarnedRewards = +get<ImperialEarnedRewardsResourceListTarget>()
        val toDetail get() = +get<DetailTarget>()
        val toOngoingAgendas = +get<OngoingAgendaResourceListTarget>()
        val toBuyHeroSkillsResourceList = +get<BuyHeroSkillsResourceListTarget>()
    }

    data class ResourceListParams(val resourceType: ResourceType, val component: Component) : Serializable
}
