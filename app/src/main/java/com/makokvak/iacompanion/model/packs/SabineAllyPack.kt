package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.DisplayableParams
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.MissionRewards
import com.makokvak.iacompanion.model.Resource

class SabineAllyPack {

    val list = listOf<Resource>(
        Resource.Mission.Side(
            DisplayableParams(R.string.intimidation_factor, R.mipmap.intimidation_factor),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(description = R.string.spectre_reward_ezra, reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.ezra_bridger, R.mipmap.ezra_bridger_campaign)))
                )),
                MissionResult.RebelVictory(description = R.string.spectre_reward_kanan, reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.kanan_jarrus, R.mipmap.kanan_jarrus_campaign)))
                )),
                MissionResult.RebelVictory(description = R.string.spectre_reward_zeb, reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.zeb_orrelios, R.mipmap.zeb_orrelios)))
                )),
                MissionResult.RebelVictory(description = R.string.spectre_reward_sabine, reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.sabine_wren, R.mipmap.sabine_wren_campaign)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}
