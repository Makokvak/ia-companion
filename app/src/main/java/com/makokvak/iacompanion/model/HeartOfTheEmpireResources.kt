package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R

class HeartOfTheEmpireResources {

    val drokkatta = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.drokkatta, R.mipmap.drokkatta_hero),
        damageDealer = 3,
        durable = 1,
        support = 1,
        range = 2,
        crowdControl = 1,
        mobility = 1,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.mgl9boomer, R.mipmap.mgl9boomer), 0),
            Resource.HeroSkill(DisplayableParams(R.string.chargingup, R.mipmap.chargingup), 1),
            Resource.HeroSkill(DisplayableParams(R.string.leavenoonebehind, R.mipmap.leavenoonebehind), 1),
            Resource.HeroSkill(DisplayableParams(R.string.bankshot, R.mipmap.bankshot), 2),
            Resource.HeroSkill(DisplayableParams(R.string.shrapnelrounds, R.mipmap.shrapnelrounds), 2),
            Resource.HeroSkill(DisplayableParams(R.string.repeatercannon, R.mipmap.repeatercannon), 3),
            Resource.HeroSkill(DisplayableParams(R.string.structuralexploitation, R.mipmap.structuralexploitation), 3),
            Resource.HeroSkill(DisplayableParams(R.string.thermalexplosives, R.mipmap.thermalexplosives), 4),
            Resource.HeroSkill(DisplayableParams(R.string.wookieewrath, R.mipmap.wookieewrath), 4)
        )
    )

    val koTunFeralo = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.ko_tun_feralo, R.mipmap.ko_tun_feralo_hero),
        damageDealer = 1,
        durable = 1,
        support = 3,
        range = 2,
        crowdControl = 0,
        mobility = 1,
        difficulty = 2,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.servicerifle, R.mipmap.servicerifle), 0),
            Resource.HeroSkill(DisplayableParams(R.string.auxiliarytraining, R.mipmap.auxiliarytraining), 1),
            Resource.HeroSkill(DisplayableParams(R.string.inchbyinch, R.mipmap.inchbyinch), 1),
            Resource.HeroSkill(DisplayableParams(R.string.combatlogistics, R.mipmap.combatlogistics), 2),
            Resource.HeroSkill(DisplayableParams(R.string.digin, R.mipmap.digin), 2),
            Resource.HeroSkill(DisplayableParams(R.string.firesupportspecialist, R.mipmap.firesupportspecialist), 3),
            Resource.HeroSkill(DisplayableParams(R.string.opportunist, R.mipmap.opportunist), 3),
            Resource.HeroSkill(DisplayableParams(R.string.selfsufficient, R.mipmap.selfsufficient), 4),
            Resource.HeroSkill(DisplayableParams(R.string.squadcohesion, R.mipmap.squadcohesion), 4)
        )
    )

    val jarrodKelvin = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.jarrod_kelvin, R.mipmap.jarrod_kelvin_hero),
        damageDealer = 2,
        durable = 2,
        support = 0,
        range = 0,
        crowdControl = 0,
        mobility = 2,
        difficulty = 3,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.vibroclaws, R.mipmap.vibroclaws), 0),
            Resource.HeroSkill(DisplayableParams(R.string.balancedapproach, R.mipmap.balancedapproach), 1),
            Resource.HeroSkill(DisplayableParams(R.string.forwardmomentum, R.mipmap.forwardmomentum), 1),
            Resource.HeroSkill(DisplayableParams(R.string.scoutsloadout, R.mipmap.scoutsloadout), 2),
            Resource.HeroSkill(DisplayableParams(R.string.slicersupgrades, R.mipmap.slicersupgrades), 2),
            Resource.HeroSkill(DisplayableParams(R.string.explosivereflexes, R.mipmap.explosivereflexes), 3),
            Resource.HeroSkill(DisplayableParams(R.string.mutualprogression, R.mipmap.mutualprogression), 3),
            Resource.HeroSkill(DisplayableParams(R.string.leapingslash, R.mipmap.leapingslash), 4),
            Resource.HeroSkill(DisplayableParams(R.string.mechanicalmaster, R.mipmap.mechanicalmaster), 4)
        ),
        references = listOf(
            Resource.Misc(DisplayableParams(R.string.j4x7, R.mipmap.j4_x_7))
        )
    )

     val list = listOf<Resource>(

         drokkatta,
         koTunFeralo,
         jarrodKelvin,

         Resource.Ally(DisplayableParams(R.string.ahsoka_tano, R.mipmap.ahsoka_tano)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.at_dp, R.mipmap.at_dp)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.emperorPalapatine, R.mipmap.emperor_palpatine)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.riot_trooper, R.mipmap.riot_trooper_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.riot_trooper_elite, R.mipmap.riot_trooper_elite_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.sentry_droid, R.mipmap.sentry_droid)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.sentry_droid_elite, R.mipmap.sentry_droid_elite)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.clawdite_shapeshifter, R.mipmap.clawdite), listOf(
             Resource.Misc(DisplayableParams(R.string.clawdite_scout, R.mipmap.scoutclawdite)),
             Resource.Misc(DisplayableParams(R.string.clawdite_senator, R.mipmap.senatorclawdite)),
             Resource.Misc(DisplayableParams(R.string.clawdite_streetrat, R.mipmap.streetratclawdite)),
         )),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.clawdite_shapeshifter_elite, R.mipmap.clawditeelite), listOf(
             Resource.Misc(DisplayableParams(R.string.clawdite_scout, R.mipmap.scoutclawdite)),
             Resource.Misc(DisplayableParams(R.string.clawdite_senator, R.mipmap.senatorclawdite)),
             Resource.Misc(DisplayableParams(R.string.clawdite_streetrat, R.mipmap.streetratclawdite)),
         )),

         Resource.Mission.Side(
             DisplayableParams(R.string.civil_unrest, R.mipmap.civil_unrest),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.populist_support, R.mipmap.populist_support)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.extraction, R.mipmap.extraction),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.alliance_efficiency, R.mipmap.alliance_efficiency)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = koTunFeralo
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.test_of_metal, R.mipmap.test_of_metal),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.x_8_upgrade, R.mipmap.x_8_upgrade)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = jarrodKelvin
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.unfinished_business, R.mipmap.unfinished_business),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.wookiee_roar, R.mipmap.wookiee_roar)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = drokkatta
         ),

         Resource.ImperialClassDeck(
             DisplayableParams(R.string.power_of_the_dark_side, R.mipmap.power_of_the_dark_side),
             R.string.power_of_the_dark_side_descrtiption,
             listOf(
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.manifest_agression, R.mipmap.manifest_aggression), xpCost = 0),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.dark_resurgence, R.mipmap.dark_resurgence), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.embrace_fear, R.mipmap.embrace_fear), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.supernatural_vigor, R.mipmap.supernatural_vigor), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.embrace_anger, R.mipmap.embrac_eanger), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.embrace_hate, R.mipmap.embrace_hate), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.unnatural_abilities, R.mipmap.unnatural_abilities), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.embrace_suffering, R.mipmap.embrace_suffering), xpCost = 4),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.the_power_of_passion, R.mipmap.the_power_of_passion), xpCost = 4)
             )
         ),
         Resource.ImperialClassDeck(
             DisplayableParams(R.string.reactive_defenses, R.mipmap.reactive_defenses),
             R.string.reactive_defenses_description,
             listOf(
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.active_surveillance, R.mipmap.active_surveillance), xpCost = 0),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.shielded, R.mipmap.shielded), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.blaster_emplacements, R.mipmap.blaster_emplacements), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.remote_activator, R.mipmap.remote_activator), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.mechanical_protocol, R.mipmap.mechanical_protocol), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.electromagnetic_disruptors, R.mipmap.electromagnetic_disruptors), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.infrared_scanners, R.mipmap.infrared_scanners), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.overclock, R.mipmap.overclock), xpCost = 4),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.targeting_sensors, R.mipmap.targeting_sensors), xpCost = 4)
             )
         ),
         Resource.AgendaSet(
             DisplayableParams(R.string.field_commander, R.mipmap.assaultcoordination),
             Resource.Agenda(DisplayableParams(R.string.assaultcoordination, R.mipmap.assaultcoordination), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.reinforcementsenroute, R.mipmap.reinforcementsenroute), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.unyielding_dedication, R.mipmap.unyieldingdedication), 2, isSecret = true)
         ),
         Resource.AgendaSet(
             DisplayableParams(R.string.security_protocols, R.mipmap.heavyriotgear),
             Resource.Agenda(DisplayableParams(R.string.heavyriotgear, R.mipmap.heavyriotgear), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.shieldsonline, R.mipmap.shieldsonline), 2, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.surveillancecams, R.mipmap.surveillancecams), 1, isSecret = true)
         ),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.grappler_arm, R.mipmap.grappler_arm), countInDeck = 1),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.shield_pack, R.mipmap.shield_pack), countInDeck = 1),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.supply_chest, R.mipmap.supply_chest), countInDeck = 1),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.charge_pistol, R.mipmap.charge_pistol), tier = ItemTier.First, cost = 350),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.charged_ammo_pack, R.mipmap.charged_ammo_pack), tier = ItemTier.First, cost = 100),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.responsive_armor, R.mipmap.responsive_armor), tier = ItemTier.First, cost = 250),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.focusing_beam, R.mipmap.focusing_beam), tier = ItemTier.Second, cost = 250),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.hunters_rifle, R.mipmap.hunters_rifle), tier = ItemTier.Second, cost = 600),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.polearm, R.mipmap.polearm), tier = ItemTier.Second, cost = 650),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.a_12_sniper_rifle, R.mipmap.a_12_sniper_rifle), tier = ItemTier.Third, cost = 1150),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.power_charger, R.mipmap.power_charger), tier = ItemTier.Third, cost = 400),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.supply_pack, R.mipmap.supply_pack), tier = ItemTier.Third, cost = 200)
     )
}
