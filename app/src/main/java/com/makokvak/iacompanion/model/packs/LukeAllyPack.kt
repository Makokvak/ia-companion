package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.DisplayableParams
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.MissionRewards
import com.makokvak.iacompanion.model.Resource

class LukeAllyPack {

    val list = listOf<Resource>(
        Resource.Mission.Side(
            DisplayableParams(R.string.a_light_in_the_darkness, R.mipmap.a_light_in_the_darkness),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.luke_skywalker_jedi_knight, R.mipmap.luke_skywalker_jedi_knight)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}
