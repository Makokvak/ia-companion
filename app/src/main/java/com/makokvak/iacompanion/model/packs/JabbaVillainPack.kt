package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class JabbaVillainPack {

    val list = listOf(

        Resource.AgendaSet(
            DisplayableParams(R.string.jabbas_empire, R.mipmap.one_fat_slug),
            Resource.Agenda(DisplayableParams(R.string.one_fat_slug, R.mipmap.one_fat_slug), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.one_fat_slug, R.mipmap.one_fat_slug),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jabba_the_hut, R.mipmap.jabba_the_hutt_campaign))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.head_hunting, R.mipmap.headhunting), 2, true),
            Resource.Agenda(DisplayableParams(R.string.on_jabbas_orders, R.mipmap.on_jabbas_orders), 1, isSecret = true)
        ),
    )
}
