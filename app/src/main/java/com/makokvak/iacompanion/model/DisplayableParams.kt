package com.makokvak.iacompanion.model;

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

@kotlinx.serialization.Serializable
data class DisplayableParams(
    @StringRes val name: Int,
    @DrawableRes val drawable: Int
): java.io.Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DisplayableParams

        if (name != other.name) return false
        if (drawable != other.drawable) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name
        result = 31 * result + drawable
        return result
    }
}
