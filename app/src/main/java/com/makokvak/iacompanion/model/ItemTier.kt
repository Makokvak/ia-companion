package com.makokvak.iacompanion.model

import androidx.annotation.StringRes
import com.makokvak.iacompanion.R
import kotlinx.serialization.Serializable

@Serializable
enum class ItemTier(@StringRes val title: Int) : java.io.Serializable {
    First(R.string.tier_1),
    Second(R.string.tier_2),
    Third(R.string.tier_3)
}
