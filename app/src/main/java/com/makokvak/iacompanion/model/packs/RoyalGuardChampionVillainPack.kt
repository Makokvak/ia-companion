package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class RoyalGuardChampionVillainPack {

    val list = listOf<Resource>(
        Resource.AgendaSet(
            DisplayableParams(R.string.crimson_empire, R.mipmap.infection),
            Resource.Agenda(DisplayableParams(R.string.infection, R.mipmap.infection), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.infection, R.mipmap.infection),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.royal_guard_champion, R.mipmap.royal_guard_champion))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.best_of_the_best, R.mipmap.best_of_the_best), 1, true),
            Resource.Agenda(DisplayableParams(R.string.pulling_the_strings, R.mipmap.pulling_the_strings), 1)
        ),
    )
}
