package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*
import kotlin.math.cos

class GreedoVillainPack {

    val list = listOf(

        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.greedo, R.mipmap.greedo)),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.dt_12_heavy_blaster_pistol, R.mipmap.dt12_heavy_blaster_pistol), tier = ItemTier.Second, cost = 600),

        Resource.AgendaSet(
            DisplayableParams(R.string.contract_gunmen, R.mipmap.top_target),
            Resource.Agenda(DisplayableParams(R.string.top_target, R.mipmap.top_target), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.top_target, R.mipmap.top_target),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.greedo, R.mipmap.greedo))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.hunt_for_a_fee, R.mipmap.hunter_for_a_fee), 1, true),
            Resource.Agenda(DisplayableParams(R.string.stealth_squad, R.mipmap.stealth_squad), 1, isSecret = true)
        ),
    )
}
