package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class AgentBlaiseVillainPack {

    val list = listOf<Resource>(
        Resource.AgendaSet(
            DisplayableParams(R.string.imperial_influence, R.mipmap.secuirty_breach),
            Resource.Agenda(DisplayableParams(R.string.security_breach, R.mipmap.secuirty_breach), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.security_breach, R.mipmap.secuirty_breach),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.agent_blaise, R.mipmap.agent_blaise_campaign))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.classified_techniques, R.mipmap.classified_tehniques), 1, true),
            Resource.Agenda(DisplayableParams(R.string.under_surveillance, R.mipmap.under_surveillance), 2, isSecret = true)
        ),
    )
}
