package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R

class JabbasRealmResources {

    val onarKoma = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.onar_koma, R.mipmap.onar_koma_hero),
        damageDealer = 1,
        durable = 3,
        support = 2,
        range = 0,
        crowdControl = 0,
        mobility = 1,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.bodyguard_rifle, R.mipmap.bodyguardrifle), 0),
            Resource.HeroSkill(DisplayableParams(R.string.get_down, R.mipmap.getdown), 1),
            Resource.HeroSkill(DisplayableParams(R.string.keep_up, R.mipmap.keepup), 1),
            Resource.HeroSkill(DisplayableParams(R.string.stay_behind_me, R.mipmap.staybehindme), 2),
            Resource.HeroSkill(DisplayableParams(R.string.mutual_destruction, R.mipmap.mutualdestruction), 2),
            Resource.HeroSkill(DisplayableParams(R.string.brute_strength, R.mipmap.brutestrength), 3),
            Resource.HeroSkill(DisplayableParams(R.string.hold_still, R.mipmap.holdstill), 3),
            Resource.HeroSkill(DisplayableParams(R.string.dont_make_me_hurt_you, R.mipmap.dontmakemehurtyou), 4),
            Resource.HeroSkill(DisplayableParams(R.string.black_sun_armor, R.mipmap.blacksunarmor), 4)
        )
    )
    val shylaVarad = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.shyla_varad, R.mipmap.shyla_varad_hero),
        damageDealer = 3,
        durable = 1,
        support = 1,
        range = 0,
        crowdControl = 1,
        mobility = 1,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.duelists_blade, R.mipmap.duelistsblade), 0),
            Resource.HeroSkill(DisplayableParams(R.string.responsiveness, R.mipmap.responsiveness), 1),
            Resource.HeroSkill(DisplayableParams(R.string.all_out_attack, R.mipmap.alloutattack), 1),
            Resource.HeroSkill(DisplayableParams(R.string.smoke_bombs, R.mipmap.smokebombs), 2),
            Resource.HeroSkill(DisplayableParams(R.string.proximity_strike, R.mipmap.proximitystrike), 2),
            Resource.HeroSkill(DisplayableParams(R.string.swords_dance, R.mipmap.swordsdance), 3),
            Resource.HeroSkill(DisplayableParams(R.string.remote_detonator, R.mipmap.remotedetonator), 3),
            Resource.HeroSkill(DisplayableParams(R.string.full_sweep, R.mipmap.fullsweep), 4),
            Resource.HeroSkill(DisplayableParams(R.string.deadly_grace, R.mipmap.deadlygrace), 4)
        )
    )
    val vintoHreeda = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.vinto_hreeda, R.mipmap.vinto_hreeda_hero),
        damageDealer = 3,
        durable = 0,
        support = 0,
        range = 2,
        crowdControl = 0,
        mobility = 1,
        difficulty = 3,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.hairtrigger_pistol, R.mipmap.hairtrigger), 0),
            Resource.HeroSkill(DisplayableParams(R.string.pinpoint_shot, R.mipmap.pinpointshot), 1),
            Resource.HeroSkill(DisplayableParams(R.string.shot_on_the_run, R.mipmap.shotontherun), 1),
            Resource.HeroSkill(DisplayableParams(R.string.sharpshooter, R.mipmap.sharpshooter), 2),
            Resource.HeroSkill(DisplayableParams(R.string.battlefield_experience, R.mipmap.battlefieldexperience), 2),
            Resource.HeroSkill(DisplayableParams(R.string.dead_on, R.mipmap.deadon), 3),
            Resource.HeroSkill(DisplayableParams(R.string.offhand_blaster, R.mipmap.offhandblaster), 3),
            Resource.HeroSkill(DisplayableParams(R.string.rapid_fire, R.mipmap.rapidfire), 4),
            Resource.HeroSkill(DisplayableParams(R.string.merciless, R.mipmap.mercilessvinto), 4)
        )
    )

    val stormingThePalace = Resource.Mission.Story(
        DisplayableParams(R.string.storming_the_palace, R.mipmap.storming_the_palace),
        staticReward = MissionRewards(),
        possibleOutcomes = listOf()
    )

    val mutiny = Resource.Mission.Story(
        DisplayableParams(R.string.mutiny, R.mipmap.mutiny),
        staticReward = MissionRewards(),
        possibleOutcomes = listOf()
    )

    val executeThePlan = Resource.Mission.Story(
        DisplayableParams(R.string.execute_the_plan, R.mipmap.execute_the_plan),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_one_hero, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_more_heroes, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.almost_home_imperial_victory_terro_suffered, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.almost_home_imperial_victory_terro_not_suffered, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(stormingThePalace, mutiny)
    )

    val almostHome = Resource.Mission.Story(
        DisplayableParams(R.string.almost_home, R.mipmap.almost_home),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_one_hero, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_more_heroes, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.almost_home_imperial_victory_terro_suffered, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.almost_home_imperial_victory_terro_not_suffered, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(mutiny, stormingThePalace)
    )

    val dangerousAllies = Resource.Mission.Story(
        DisplayableParams(R.string.dangerous_allies, R.mipmap.dangerous_allies),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.dangerous_allies_rebel_victory_imperial_figures_on_map, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.dangerous_allies_rebel_victory_no_imperial_figure_on_map, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.dangerous_allies_imperial_victory_terro_dead, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.dangerous_allies_imperial_victory_terro_alive, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(executeThePlan)
    )

    val fromAllSides = Resource.Mission.Story(
        DisplayableParams(R.string.from_all_sides, R.mipmap.from_all_sides),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.from_all_sides_rebel_victory_terro_not_defeated, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.from_all_sides_rebel_victory_terro_defeated, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.from_all_sides_imperial_victory_luke_not_defeated, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.from_all_sides_imperial_victory_luke_defeated, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(almostHome)
    )

    val momentOfFate = Resource.Mission.Story(
        DisplayableParams(R.string.moment_of_fate, R.mipmap.moment_of_fate),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.rebel_victory, reward = MissionRewards(
                expForRebels = 1
            )),
            MissionResult.ImperialVictory(description = R.string.imperial_victory, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
            MissionResult.AdditionalOutcome(R.string.moment_of_fate_heroes_shut_down_all_defenses, reward = MissionRewards(
                rewardsForRebels = setOf(
                    Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.alliance_ranger, R.mipmap.alliance_ranger)),
                    Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.rebel_saboteur, R.mipmap.rebel_saboteur_2)),
                    Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.rebel_trooper, R.mipmap.rebel_trooper)),
                )
            )),
            MissionResult.AdditionalOutcome(R.string.moment_of_fate_heroes_stole_codes, reward = MissionRewards(
                creditsPerHero = 25
                // todo implement tier deck search
            ))
        ),
        nextMissions = setOf(dangerousAllies, fromAllSides)
    )

    val turfWar = Resource.Mission.Story(
        DisplayableParams(R.string.turf_war, R.mipmap.turf_war),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_one_hero, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.turf_war_rebel_victory_more_heroes, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.turf_war_imperial_victory_supports_destroyed, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.turf_war_imperial_victory_supports_not_destroyed, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(momentOfFate)
    )

    val trophyHunting = Resource.Mission.Story(
        DisplayableParams(R.string.trophy_hunting, R.mipmap.trophy_hunting),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.trophy_hunting_rebel_victory_one_hero, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.trophy_hunting_rebel_victory_more_heroes, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.trophy_hunting_imperial_victory_kallenn_defeated, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.trophy_hunting_imperial_victory_kallenn_not_defeated, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(momentOfFate)
    )

    val overcharged = Resource.Mission.Story(
        DisplayableParams(R.string.overcharged, R.mipmap.overcharged),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.overcharged_rebel_victory_only_one_hero, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.overcharged_rebel_victory_more_heroes, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.overcharged_imperial_victory_explosives, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.overcharged_imperial_victory_no_explosives, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(trophyHunting, turfWar)
    )

    val hostileNegotiations = Resource.Mission.Story(
        DisplayableParams(R.string.hostile_negotiations, R.mipmap.hostile_negotiations),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(description = R.string.hostile_negotiations_rebel_victory_barrels, reward = MissionRewards(
                creditsPerHero = 100,
                drawMoreAgendaCards = 1
            )),
            MissionResult.RebelVictory(description = R.string.hostile_negotiations_rebel_victory_no_barrels, reward = MissionRewards(
                creditsPerHero = 100
            )),
            MissionResult.ImperialVictory(description = R.string.hostile_negotiations_imperial_victory_barrels, reward = MissionRewards(
                creditsPerHero = 25,
                expForImperialPlayer = 1
            )),
            MissionResult.ImperialVictory(description = R.string.hostile_negotiations_imperial_victory_no_barrels, reward = MissionRewards(
                expForImperialPlayer = 1
            )),
        ),
        nextMissions = setOf(trophyHunting, turfWar)
    )

    val trespass = Resource.Mission.Story(
        DisplayableParams(R.string.trespass, R.mipmap.jabbas_realm),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.RebelVictory(reward = MissionRewards(
                expForRebels = 1
            )),
            MissionResult.ImperialVictory(
                reward = MissionRewards(expForImperialPlayer = 1)
            )
        ),
        nextMissions = setOf(overcharged, hostileNegotiations)
    )

    val list = listOf(
        onarKoma,
        shylaVarad,
        vintoHreeda,
        // Campaign
        Resource.Campaign(DisplayableParams(R.string.jabbas_realm_campaign, R.mipmap.jabbas_realm), firstMission = trespass, details = listOf(
            CampaignMissionDetails(threatLevel = 2, tiers = setOf(ItemTier.First), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 3, tiers = setOf(ItemTier.First), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 3, tiers = setOf(ItemTier.First, ItemTier.Second), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second, ItemTier.Third), isSlotForSideMission = false, fixedMission = momentOfFate),
            CampaignMissionDetails(threatLevel = 5, tiers = setOf(ItemTier.Third), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 5, tiers = setOf(ItemTier.Third), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 6, tiers = setOf(ItemTier.Third), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 6, tiers = setOf(), isSlotForSideMission = false),
        )),
        Resource.Mission.Side(
            DisplayableParams(R.string.perilous_hunt, R.mipmap.perilous_hunt),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    creditsPerHero = 100
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(
                    rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.bred_for_war, R.mipmap.bred_for_war))
                ))
            ),
            isRandom = true
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.born_from_death, R.mipmap.born_from_death),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.manadalorian_heritage, R.mipmap.mandalorian_heritage)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = shylaVarad
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.a_heros_welcome, R.mipmap.a_heros_welcome),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.desperado, R.mipmap.desperado)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = vintoHreeda
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.extortion, R.mipmap.extortion),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.haymaker, R.mipmap.haymaker)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = onarKoma
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.guild_hunters, R.mipmap.shoot_first),
            Resource.Agenda(DisplayableParams(R.string.a_dark_power, R.mipmap.shoot_first), 2, isSecret = true),
            Resource.Agenda(DisplayableParams(R.string.as_you_wish, R.mipmap.call_your_shot), 2, isSecret = true),
            Resource.Agenda(DisplayableParams(R.string.dark_obsession, R.mipmap.abandon_contract), 2, isSecret = true)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.persistence, R.mipmap.no_rest_for_the_weary),
            Resource.Agenda(DisplayableParams(R.string.blackmail, R.mipmap.snap_fire), 2, isSecret = true),
            Resource.Agenda(DisplayableParams(R.string.learn_their_weaknesses, R.mipmap.no_rest_for_the_weary), 1, isSecret = false),
            Resource.Agenda(DisplayableParams(R.string.weak_links, R.mipmap.jetpacks_agenda_card), 1, isSecret = true)
        ),
        Resource.ImperialClassDeck(
            DisplayableParams(R.string.hutt_mercenaries, R.mipmap.hutt_mercenaries),
            R.string.hutt_mercenaries_description,
            listOf(
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.wanted_dead, R.mipmap.wanted_dead), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.scouted, R.mipmap.scouted), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.vendetta, R.mipmap.vendetta), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.cheap_shot, R.mipmap.cheap_shot), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.savage_motivation, R.mipmap.savage_motivation), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.nowhere_to_hide, R.mipmap.nowhere_to_hide), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.nowhere_to_run, R.mipmap.nowhere_to_run), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.guild_hunters, R.mipmap.guild_hunters), xpCost = 4),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.most_wanted, R.mipmap.most_wanted), xpCost = 4)
            )
        ),
        Resource.ImperialClassDeck(
            DisplayableParams(R.string.nemeses, R.mipmap.nemeses),
            R.string.nemeses_description,
            listOf(
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.insprational, R.mipmap.inspirational), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.powerful_foes, R.mipmap.powerful_foes), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.fearsome_presence, R.mipmap.fearsome_presence), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.prepare_the_ambush, R.mipmap.prepare_the_ambush), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.im_on_the_leader, R.mipmap.im_on_the_leader), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.ringleader, R.mipmap.ringleader), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.leave_them_to_me, R.mipmap.leave_them_to_me), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.punishing_force, R.mipmap.punishing_force), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.indomitable, R.mipmap.indomitable), xpCost = 4),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.devastating_legion, R.mipmap.devastating_legion), xpCost = 4)
            )
        ),

        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.flash_bomb, R.mipmap.flash_bomb), countInDeck = 1),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.painkillers, R.mipmap.painkillers), countInDeck = 1),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.pit_droid, R.mipmap.pitdroidsupply), countInDeck = 1, references = listOf(
            Resource.Misc(DisplayableParams(R.string.pit_droid, R.mipmap.pit_droid_companion))
        )),

        Resource.Ally(DisplayableParams(R.string.luke_skywalker_jedi_knight, R.mipmap.luke_skywalker_jedi_knight)),
        Resource.Ally(DisplayableParams(R.string.alliance_ranger, R.mipmap.alliance_ranger)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jet_trooper, R.mipmap.jet_trooper)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jet_trooper_elite, R.mipmap.jet_trooper_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.captain_terro, R.mipmap.captain_terro)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.gamorrean_guard, R.mipmap.gamorrean_guard)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.gamorrean_guard_elite, R.mipmap.gamorrean_guard_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.weequay_pirate, R.mipmap.weequay_pirate)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.weequay_pirate_elite, R.mipmap.weequay_pirate_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.rancor, R.mipmap.rancor_campaign)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jabba_the_hut, R.mipmap.jabba_the_hutt_campaign)),

        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.bacta_pump, R.mipmap.bacta_pump), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.emergency_injector, R.mipmap.emergency_injector), tier = ItemTier.First, cost = 150),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.hand_cannon, R.mipmap.hand_cannon), tier = ItemTier.First, cost = 400),

        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.bolt_upgrade, R.mipmap.bolt_upgrade), tier = ItemTier.Second, cost = 250),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.double_vibrosword, R.mipmap.double_vibrosword), tier = ItemTier.Second, cost = 650),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.energized_hilt, R.mipmap.energized_hilt), tier = ItemTier.Second, cost = 250),

        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.admirals_uniform, R.mipmap.admirals_uniform), tier = ItemTier.Third, cost = 350),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.modified_energy_cannon, R.mipmap.modified_energy_cannon), tier = ItemTier.Third, cost = 1000),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.ryyk_blades, R.mipmap.ryyk_blades), tier = ItemTier.Third, cost = 950)
    )
}
