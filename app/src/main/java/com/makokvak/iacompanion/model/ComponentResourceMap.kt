package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.model.packs.*

class ComponentResourceMap(
    coreGameResources: CoreGameResources,
    bespinGambitResources: BespinGambitResources,
    heartOfTheEmpireResources: HeartOfTheEmpireResources,
    tyrantsOfLothalResources: TyrantsOfLothalResources,
    jabbasRealmResources: JabbasRealmResources,
    chewbaccaAllyPack: ChewbaccaAllyPack,
    hanSoloAllyPack: HanSoloAllyPack,
    royalGuardChampionVillainPack: RoyalGuardChampionVillainPack,
    agentBlaiseVillainPack: AgentBlaiseVillainPack,
    ahsokaAllyPack: AhsokaAllyPack,
    bosskVillainPack: BosskVillainPack,
    grandInquisitorVillainPack: GrandInquisitorVillainPack,
    greedoVillainPack: GreedoVillainPack,
    heraAllyPack: HeraAllyPack,
    jabbaVillainPack: JabbaVillainPack,
    jawaVillainPack: JawaVillainPack,
    landoAllyPack: LandoAllyPack,
    lukeAllyPack: LukeAllyPack,
    maulVillainPack: MaulVillainPack,
    sabineAllyPack: SabineAllyPack,
    hondoVillainPack: HondoVillainPack,
    obiWanAllyPack: ObiWanAllyPack,
    palpatineVillainPack: PalpatineVillainPack,
    thrawnVillainPack: ThrawnVillainPack
) {

    val map = hashMapOf(
        Component.CoreGame to coreGameResources.list,
        Component.Expansion.JabbasRealm to jabbasRealmResources.list,
        Component.Expansion.BespinGambit to bespinGambitResources.list,
        Component.Expansion.HeartOfTheEmpire to heartOfTheEmpireResources.list,
        Component.Expansion.TyrantsOfLothal to tyrantsOfLothalResources.list,
        Component.Pack.Ally.Chewbacca to chewbaccaAllyPack.list,
        Component.Pack.Ally.HanSolo to hanSoloAllyPack.list,
        Component.Pack.Villain.RoyalGuardChampion to royalGuardChampionVillainPack.list,
        Component.Pack.Villain.AgentBlaise to agentBlaiseVillainPack.list,
        Component.Pack.Ally.Ahsoka to ahsokaAllyPack.list,
        Component.Pack.Villain.Bossk to bosskVillainPack.list,
        Component.Pack.Villain.Greedo to greedoVillainPack.list,
        Component.Pack.Villain.GrandInquisitor to grandInquisitorVillainPack.list,
        Component.Pack.Ally.Hera to heraAllyPack.list,
        Component.Pack.Villain.Jabba to jabbaVillainPack.list,
        Component.Pack.Villain.Jawa to jawaVillainPack.list,
        Component.Pack.Ally.Lando to landoAllyPack.list,
        Component.Pack.Ally.Luke to lukeAllyPack.list,
        //Component.Pack.Villain.Maul to maulVillainPack.list,
        Component.Pack.Ally.ObiWan to obiWanAllyPack.list,
        Component.Pack.Ally.SabineZeb to sabineAllyPack.list,
        Component.Pack.Villain.EmperorPalpatine to palpatineVillainPack.list,
        Component.Pack.Villain.Thrawn to thrawnVillainPack.list,
        Component.Pack.Villain.Hondo to hondoVillainPack.list
    )

    val components by lazy { map.keys.toList() }

    fun getAllResources() =
        map.values.flatten().toList()

    fun getAllResourcesAndNestedResources(): List<Resource> =
        getAllResources()
            .plus(getByResourceTypeAndComponent<Resource.ResourceWithReferences.Hero>(Component.AllComponents).map { it.skillDeck }.flatten())
            .plus(getByResourceTypeAndComponent<Resource.AgendaSet>(Component.AllComponents).map { listOf(it.agenda0, it.agenda1, it.agenda2) }.flatten())
            .plus(getAllPossibleRewards())

    private fun getAllPossibleRewards(): List<Resource> {
        val missions = getByResourceTypeAndComponent<Resource.Mission>(Component.AllComponents)
        val result = mutableListOf<Resource>()
        missions.forEach { mission ->
            if(mission.staticReward.earnedAllies.isNotEmpty())
                result.addAll(mission.staticReward.earnedAllies)
            if(mission.staticReward.earnedVillain != null)
                result.add(mission.staticReward.earnedVillain!!)
            if(mission.staticReward.rewardForImperialPlayer != null)
                result.add(mission.staticReward.rewardForImperialPlayer!!)
            if(mission.staticReward.rewardsForRebels.isNotEmpty())
                result.addAll(mission.staticReward.rewardsForRebels)

            mission.possibleOutcomes.forEach { outcome ->
                val reward = outcome.reward

                if(reward.earnedAllies.isNotEmpty())
                    result.addAll(reward.earnedAllies)
                if(reward.earnedVillain != null)
                    result.add(reward.earnedVillain)
                if(reward.rewardForImperialPlayer != null)
                    result.add(reward.rewardForImperialPlayer)
                if(reward.rewardsForRebels.isNotEmpty())
                    result.addAll(reward.rewardsForRebels)
            }
        }

        return result
    }


    inline fun <reified ResourceType : Resource> getByResourceTypeAndComponents(components: List<Component>) =
        if(!components.contains(Component.AllComponents))
            components.map { getByComponent(it) }.flatten().filterIsInstance<ResourceType>().toList()
        else
            map.values.flatten().filterIsInstance<ResourceType>().toList()

    inline fun <reified ResourceType : Resource> getByResourceTypeAndComponent(component: Component) =
        getByResourceTypeAndComponents<ResourceType>(listOf(component))

    fun getByComponent(component: Component) =
        if(component == Component.AllComponents)
            getAllResources()
        else
            map[component] ?: throw IllegalArgumentException("key $component not found in the map")

}
