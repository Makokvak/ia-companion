package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class ObiWanAllyPack {

    val list = listOf(
        Resource.Ally(DisplayableParams(R.string.obi_wan_kenobi, R.mipmap.obi_wan)),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.ancient_lightsaber, R.mipmap.ancient_lightsaber), tier = ItemTier.Third, cost = 1000),

        Resource.Mission.Side(
            DisplayableParams(R.string.deadly_transmission, R.mipmap.deadly_transmission),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.obi_wan_kenobi, R.mipmap.obi_wan)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}
