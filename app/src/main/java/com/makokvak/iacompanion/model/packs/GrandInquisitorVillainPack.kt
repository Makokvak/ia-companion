package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class GrandInquisitorVillainPack {

    val list = listOf(

        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.grand_inquisitor, R.mipmap.grand_inquisitor)),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.electrostaff, R.mipmap.electrostaff), tier = ItemTier.Third, cost = 1250),

        Resource.AgendaSet(
            DisplayableParams(R.string.inquisition, R.mipmap.cornered),
            Resource.Agenda(DisplayableParams(R.string.cornered, R.mipmap.cornered), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.cornered, R.mipmap.cornered),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards()),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.grand_inquisitor, R.mipmap.grand_inquisitor))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.targeted_by_the_empire, R.mipmap.targeted_by_the_empire), 2, true),
            Resource.Agenda(DisplayableParams(R.string.when_they_least_expect_it, R.mipmap.when_they_least_expect_it), 1, isSecret = true)
        ),
    )
}
