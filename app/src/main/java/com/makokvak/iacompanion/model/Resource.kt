package com.makokvak.iacompanion.model

import androidx.annotation.StringRes
import com.makokvak.iacompanion.R
import kotlinx.serialization.Serializable

@Serializable
sealed class Resource : java.io.Serializable {
    abstract val displayableParams: DisplayableParams

    @Serializable
    sealed class ResourceWithReferences : Resource() {
        abstract val references: List<Resource>

        @Serializable
        class Hero(
            override val displayableParams: DisplayableParams,
            val damageDealer: Int,
            val difficulty: Int,
            val durable: Int,
            val support: Int,
            val range: Int,
            val mobility: Int,
            val crowdControl: Int,
            val skillDeck: List<HeroSkill>,
            override val references: List<Resource> = listOf()) : ResourceWithReferences() {
            override fun equals(other: Any?): Boolean {
                if (this === other) return true
                if (javaClass != other?.javaClass) return false

                other as Hero

                if (displayableParams != other.displayableParams) return false

                return true
            }

            override fun hashCode(): Int {
                return displayableParams.hashCode()
            }
        }

        @Serializable
        class Deployable(override val displayableParams: DisplayableParams, override val references: List<Resource> = listOf()) : ResourceWithReferences()
        @Serializable
        class Reward(override val displayableParams: DisplayableParams, override val references: List<Resource> = listOf()) : ResourceWithReferences()
        @Serializable
        class Item(override val displayableParams: DisplayableParams, val tier: ItemTier, val cost: Int, override  val references: List<Resource> = listOf()) : ResourceWithReferences()
        @Serializable
        class Supply(override val displayableParams: DisplayableParams, val countInDeck: Int = 1, override  val references: List<Resource> = listOf()) : ResourceWithReferences()
    }

    @Serializable
    class Ally(override val displayableParams: DisplayableParams) : Resource()
    @Serializable
    class HeroSkill(override val displayableParams: DisplayableParams, val expCost: Int) : Resource() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as HeroSkill

            if (displayableParams != other.displayableParams) return false

            return true
        }

        override fun hashCode(): Int {
            return displayableParams.hashCode()
        }
    }


    @Serializable
    class Misc(override val displayableParams: DisplayableParams) : Resource()
    @Serializable
    class Agenda(override val displayableParams: DisplayableParams, val cost: Int, val isSecret: Boolean = false, val forcedMission: Mission.Forced? = null, val sideMission: Mission.Side? = null) : Resource()
    @Serializable
    class AgendaSet(override val displayableParams: DisplayableParams, val agenda0: Agenda, val agenda1: Agenda, val agenda2: Agenda) : Resource()
    @Serializable
    sealed class Mission : Resource() {
        abstract val staticReward: MissionRewards
        abstract val possibleOutcomes: List<MissionResult>

        @Serializable
        class Story(override val displayableParams: DisplayableParams, override val staticReward: MissionRewards, override val possibleOutcomes: List<MissionResult>, val nextMissions: Set<Mission> = setOf()) : Mission()
        @Serializable
        class Side(override val displayableParams: DisplayableParams, override val staticReward: MissionRewards, override val possibleOutcomes: List<MissionResult>, val forCharacter: ResourceWithReferences.Hero? = null, val isRandom: Boolean = false, val isFromAgenda: Boolean = false) : Mission()
        @Serializable
        class Final(override val displayableParams: DisplayableParams, override val staticReward: MissionRewards, override val possibleOutcomes: List<MissionResult>) : Mission()
        @Serializable
        class Forced(override val displayableParams: DisplayableParams, override val staticReward: MissionRewards, override val possibleOutcomes: List<MissionResult>) : Mission()
    }
    @Serializable
    class Campaign(override val displayableParams: DisplayableParams, val firstMission: Mission.Story, val details: List<CampaignMissionDetails>) : Resource()
    @Serializable
    class ImperialClassDeckCard(override val displayableParams: DisplayableParams, val xpCost: Int) : Resource() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ImperialClassDeckCard

            if (displayableParams != other.displayableParams) return false

            return true
        }

        override fun hashCode(): Int {
            return displayableParams.hashCode()
        }
    }
    @Serializable
    class ImperialClassDeck(override val displayableParams: DisplayableParams, @StringRes val description: Int, val cards: List<ImperialClassDeckCard>) : Resource() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as ImperialClassDeck

            if (displayableParams != other.displayableParams) return false

            return true
        }

        override fun hashCode(): Int {
            return displayableParams.hashCode()
        }
    }
}

@Serializable
data class MissionRewards(
    val expForEachPlayer: Int = 0,
    val influence: Int = 0,
    val expForRebels: Int = 0,
    val creditsPerHero: Int = 0,
    val expForImperialPlayer: Int = 0,
    val rewardsForRebels: Set<Resource.ResourceWithReferences.Reward> = setOf(),
    val rewardForImperialPlayer: Resource.ResourceWithReferences.Reward? = null,
    val earnedAllies: List<Resource.Ally> = listOf(),
    val earnedVillain: Resource.ResourceWithReferences.Deployable? = null,
    val instantForcedMission: Resource.Mission? = null,
    val drawMoreAgendaCards: Int = 0
) : java.io.Serializable

@Serializable
sealed class MissionResult : java.io.Serializable {

    abstract val name: Int
    abstract val reward: MissionRewards
    abstract val description: Int?

    @Serializable
    sealed class StoryMissionResult : MissionResult() {
        abstract val nextMission: Resource.Mission
        @Serializable
        class StoryRebelVictory(override val description: Int? = null, override val reward: MissionRewards, override val nextMission: Resource.Mission) : StoryMissionResult() {
            override val name: Int = R.string.rebel_victory
        }
        @Serializable
        class StoryImperialVictory(override val description: Int? = null, override val reward: MissionRewards, override val nextMission: Resource.Mission) : StoryMissionResult() {
            override val name: Int = R.string.imperial_victory
        }
    }
    @Serializable
    class ImperialVictory(override val description: Int? = null, override val reward: MissionRewards) : MissionResult() {
        override val name: Int = R.string.imperial_victory
    }
    @Serializable
    class RebelVictory(override val description: Int? = null, override val reward: MissionRewards) : MissionResult() {
        override val name: Int = R.string.rebel_victory
    }
    @Serializable
    class AdditionalOutcome(override val name: Int, override val description: Int? = null, override val reward: MissionRewards) : MissionResult()
    @Serializable
    class ManualOutcome(override val name: Int, override val description: Int? = null, val promptManualCreditOutcomeForRebels: Boolean = false, val promptManualInfluenceOutcome: Boolean = false) : MissionResult() {
        override val reward: MissionRewards = MissionRewards()
    }
}

@Serializable
data class CampaignMissionDetails (
    val threatLevel: Int,
    val tiers: Set<ItemTier>,
    val isSlotForSideMission: Boolean,
    val fixedMission: Resource.Mission? = null
) : java.io.Serializable
