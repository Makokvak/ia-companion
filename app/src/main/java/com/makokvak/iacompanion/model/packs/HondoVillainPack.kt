package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class HondoVillainPack {

    val list = listOf(
        Resource.AgendaSet(
            DisplayableParams(R.string.ohnaka_gang, R.mipmap.the_pirates_ploy),
            Resource.Agenda(DisplayableParams(R.string.the_pirates_ploy, R.mipmap.the_pirates_ploy), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.the_pirates_ploy, R.mipmap.the_pirates_ploy),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.hondo_ohnaka, R.mipmap.hondo_ohnaka_campaign))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.missing_treasure, R.mipmap.missing_treasure), 1),
            Resource.Agenda(DisplayableParams(R.string.final_offer, R.mipmap.final_offer), 1, isSecret = true)
        ),
    )
}
