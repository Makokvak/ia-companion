package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.DisplayableParams
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.MissionRewards
import com.makokvak.iacompanion.model.Resource

class HeraAllyPack {

    companion object {
        val hera = Resource.Ally(DisplayableParams(R.string.hera_syndulla, R.mipmap.hera_syndulla))
        val chopper = Resource.Ally(DisplayableParams(R.string.c1_10p, R.mipmap.c1_10p))
    }

    val list = listOf<Resource>(
        Resource.Mission.Side(
            DisplayableParams(R.string.phantom_extraction, R.mipmap.phantom_extraction),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(hera, chopper)
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}
