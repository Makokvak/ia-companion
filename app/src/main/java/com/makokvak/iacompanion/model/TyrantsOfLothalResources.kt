package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R

class TyrantsOfLothalResources {

    val ct1701 = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.ct_1701, R.mipmap.ct_1701_hero),
        damageDealer = 2,
        durable = 1,
        support = 2,
        range = 3,
        crowdControl = 1,
        mobility = 1,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.dc15sblaster, R.mipmap.dc15sblaster), 0),
            Resource.HeroSkill(DisplayableParams(R.string.coveringfire, R.mipmap.coveringfirect1701), 1),
            Resource.HeroSkill(DisplayableParams(R.string.reload, R.mipmap.reload), 1),
            Resource.HeroSkill(DisplayableParams(R.string.pinthemdown, R.mipmap.pinthemdown), 2),
            Resource.HeroSkill(DisplayableParams(R.string.strafingrun, R.mipmap.strafingrun), 2),
            Resource.HeroSkill(DisplayableParams(R.string.squadtactics, R.mipmap.squadtactics), 3),
            Resource.HeroSkill(DisplayableParams(R.string.weaknessidentified, R.mipmap.weaknessidentified), 3),
            Resource.HeroSkill(DisplayableParams(R.string.concentratefire, R.mipmap.concentratefire), 4),
            Resource.HeroSkill(DisplayableParams(R.string.wildfire, R.mipmap.wildfire), 4)
        )
    )

    val tressHacnua = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.tress_hacnua, R.mipmap.tress_hacnua_hero),
        damageDealer = 2,
        durable = 2,
        support = 0,
        range = 0,
        crowdControl = 1,
        mobility = 3,
        difficulty = 3,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.reinforcedcyberarm, R.mipmap.reinforcedcyberarm), 0),
            Resource.HeroSkill(DisplayableParams(R.string.aryxvariation, R.mipmap.aryxvariation), 1),
            Resource.HeroSkill(DisplayableParams(R.string.mynockstrike, R.mipmap.mynockstrike), 1),
            Resource.HeroSkill(DisplayableParams(R.string.dianogasweep, R.mipmap.dianogasweep), 2),
            Resource.HeroSkill(DisplayableParams(R.string.gundarkthrow, R.mipmap.gundarkthrow), 2),
            Resource.HeroSkill(DisplayableParams(R.string.acklaycounter, R.mipmap.acklaycounter), 3),
            Resource.HeroSkill(DisplayableParams(R.string.dragonsnakevariation, R.mipmap.dragonsnakevariation), 3),
            Resource.HeroSkill(DisplayableParams(R.string.fluidity, R.mipmap.fluidity), 4),
            Resource.HeroSkill(DisplayableParams(R.string.kraytdragonfury, R.mipmap.kraytdragonfury), 4)
        )
    )

     val list = listOf(
         ct1701,
         tressHacnua,

         Resource.Ally(DisplayableParams(R.string.ezra_bridger, R.mipmap.ezra_bridger_campaign)),
         Resource.Ally(DisplayableParams(R.string.kanan_jarrus, R.mipmap.kanan_jarrus_campaign)),
         Resource.Ally(DisplayableParams(R.string.sabine_wren, R.mipmap.sabine_wren_campaign)),
         Resource.Ally(DisplayableParams(R.string.zeb_orrelios, R.mipmap.zeb_orrelios)),

         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.death_trooper, R.mipmap.death_trooper_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.death_trooper_elite, R.mipmap.death_trooper_elite_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.thrawn, R.mipmap.thrawn_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.loth_cat, R.mipmap.loth_cat)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.loth_cat_elite, R.mipmap.loth_cat_elite)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.hondo_ohnaka, R.mipmap.hondo_ohnaka_campaign)),

         Resource.Mission.Side(
             DisplayableParams(R.string.duel_on_devaron, R.mipmap.duel_on_devaron),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.wholeness, R.mipmap.wholeness)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = tressHacnua
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.sands_of_seelos, R.mipmap.sands_of_seelos),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(description = R.string.sands_of_seelos_not_ct_hero, reward = MissionRewards(
                     creditsPerHero = 150
                 )),
                 MissionResult.RebelVictory(description = R.string.sands_of_seelos_ct_hero, reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.bullseye, R.mipmap.bullseye)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = ct1701
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.race_on_ryloth, R.mipmap.race_on_ryloth),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(description = R.string.spectre_reward_ezra, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.ezra_bridger, R.mipmap.ezra_bridger_campaign)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_kanan, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.kanan_jarrus, R.mipmap.kanan_jarrus_campaign)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_zeb, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.zeb_orrelios, R.mipmap.zeb_orrelios)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_sabine, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.sabine_wren, R.mipmap.sabine_wren_campaign)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = false
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.siege_on_geonosis, R.mipmap.siege_on_geonosis),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(description = R.string.spectre_reward_ezra, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.ezra_bridger, R.mipmap.ezra_bridger_campaign)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_kanan, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.kanan_jarrus, R.mipmap.kanan_jarrus_campaign)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_zeb, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.zeb_orrelios, R.mipmap.zeb_orrelios)))
                 )),
                 MissionResult.RebelVictory(description = R.string.spectre_reward_sabine, reward = MissionRewards(
                     earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.sabine_wren, R.mipmap.sabine_wren_campaign)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = false
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.call_to_action, R.mipmap.call_to_action),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.nova_cell_leader, R.mipmap.nova_cell_leader)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.the_final_order, R.mipmap.the_final_order),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     creditsPerHero = 150
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),

         Resource.AgendaSet(
             DisplayableParams(R.string.war_of_attrition, R.mipmap.acceptable_margins),
             Resource.Agenda(DisplayableParams(R.string.acceptable_margins, R.mipmap.acceptable_margins), 2, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.costly_victory, R.mipmap.costly_victory), 1, isSecret = false),
             Resource.Agenda(DisplayableParams(R.string.selfless_devotion, R.mipmap.selfless_devotion), 1, isSecret = true)
         ),
         Resource.AgendaSet(
             DisplayableParams(R.string.weapons_division, R.mipmap.prototypes),
             Resource.Agenda(DisplayableParams(R.string.gas_canisters, R.mipmap.gas_canisters), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.ongoing_research, R.mipmap.ongoing_research), 1, isSecret = false),
             Resource.Agenda(DisplayableParams(R.string.prototypes, R.mipmap.prototypes), 2, isSecret = true)
         ),

         Resource.ImperialClassDeck(
             DisplayableParams(R.string.overwhelming_oppression, R.mipmap.overwhelming_oppression),
             R.string.overwhelming_oppression_description,
             listOf(
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.commence_landing, R.mipmap.commence_landing), xpCost = 0),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.personal_flagship, R.mipmap.personal_flagship), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.redouble_our_efforts, R.mipmap.redouble_our_efforts), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.implacable, R.mipmap.implacable), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.tie_landers, R.mipmap.tie_landers), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.tactical_mastery, R.mipmap.tactical_mastery), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.tie_fighter_patrol, R.mipmap.tie_fighter_patrol), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.indomitable_force, R.mipmap.indomitable_force), xpCost = 4),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.limitless_arsenal, R.mipmap.limitless_arsenal), xpCost = 4)
             )
         ),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.locked_chest, R.mipmap.locked_chest), countInDeck = 1),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.missing_key, R.mipmap.missing_key), countInDeck = 1),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.combat_vambrace, R.mipmap.combat_vambrace), tier = ItemTier.First, cost = 200),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.punch_dagger, R.mipmap.punch_dagger), tier = ItemTier.First, cost = 400),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.e_11d, R.mipmap.e_11), tier = ItemTier.Second, cost = 700),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.mandalorian_helmet, R.mipmap.mandalorian_helmet), tier = ItemTier.Second, cost = 250),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.bo_rifle, R.mipmap.bo_rifle), tier = ItemTier.Third, cost = 750),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.plastoid_armor, R.mipmap.plastoid_armor), tier = ItemTier.Third, cost = 350)
     )
}
