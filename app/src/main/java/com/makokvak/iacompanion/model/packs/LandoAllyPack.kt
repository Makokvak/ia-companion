package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.DisplayableParams
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.MissionRewards
import com.makokvak.iacompanion.model.Resource

class LandoAllyPack {

    val list = listOf<Resource>(
        Resource.Mission.Side(
            DisplayableParams(R.string.paying_debts, R.mipmap.paying_debts),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.lady_luck, R.mipmap.lady_luck)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}
