package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class MaulVillainPack {

    val list = listOf(
        Resource.AgendaSet(
            DisplayableParams(R.string.a_former_sith, R.mipmap.dubious_disposition),
            Resource.Agenda(DisplayableParams(R.string.dubious_disposition, R.mipmap.dubious_disposition), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.dubious_disposition, R.mipmap.dubious_disposition),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards()),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.maul, R.mipmap.maul))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.imminent_reprisal, R.mipmap.imminent_reprisal), 1, isSecret = true),
            Resource.Agenda(DisplayableParams(R.string.critical_strikes, R.mipmap.critical_strikes), 1, isSecret = true)
        ),
    )
}
