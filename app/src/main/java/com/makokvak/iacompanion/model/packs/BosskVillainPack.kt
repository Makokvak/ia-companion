package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class BosskVillainPack {

    val list = listOf<Resource>(
        Resource.AgendaSet(
            DisplayableParams(R.string.base_instincts, R.mipmap.gunrunner),
            Resource.Agenda(DisplayableParams(R.string.gunrunner, R.mipmap.gunrunner), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.gunrunner, R.mipmap.gunrunner),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.bossk, R.mipmap.bossk))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.furious_retaliation, R.mipmap.furious_retaliation), 1, true),
            Resource.Agenda(DisplayableParams(R.string.on_the_hunt, R.mipmap.on_the_hunt), 1, isSecret = false)
        ),
    )
}
