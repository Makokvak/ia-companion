package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.DisplayableParams
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.MissionRewards
import com.makokvak.iacompanion.model.Resource

class AhsokaAllyPack {

    val list = listOf<Resource>(
        Resource.Mission.Side(
            DisplayableParams(R.string.home_invasion, R.mipmap.home_invasion),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.ahsoka_tano, R.mipmap.ahsoka_tano)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        )
    )
}