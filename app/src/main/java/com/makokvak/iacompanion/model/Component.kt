package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.util.util.StableIdItem
import kotlinx.serialization.Polymorphic
import java.io.Serializable

@kotlinx.serialization.Serializable
sealed class Component : Serializable, StableIdItem {

    abstract val displayableParams: DisplayableParams

    override fun getStableId(): Long {
        return hashCode().toLong()
    }

    @kotlinx.serialization.Serializable
    object AllComponents : Component() {
        override val displayableParams: DisplayableParams = DisplayableParams(R.string.all_components, R.mipmap.all_components)
    }
    @kotlinx.serialization.Serializable
    object CoreGame : Component() {
        override val displayableParams: DisplayableParams = DisplayableParams(R.string.coreGame, R.mipmap.core_game)
    }
    @kotlinx.serialization.Serializable
    sealed class Expansion(override val displayableParams: DisplayableParams) : Component() {
        @kotlinx.serialization.Serializable
        object JabbasRealm : Expansion(DisplayableParams(R.string.jabbasRealm, R.mipmap.jabbas_realm))
        @kotlinx.serialization.Serializable
        object BespinGambit : Expansion(DisplayableParams(R.string.bespin_gambit, R.mipmap.bespin_gambit))
        @kotlinx.serialization.Serializable
        object HeartOfTheEmpire : Expansion(DisplayableParams(R.string.heart_of_the_empire, R.mipmap.heart_of_the_empire))
        @kotlinx.serialization.Serializable
        object TyrantsOfLothal : Expansion(DisplayableParams(R.string.tyrants_of_lothal, R.mipmap.tyrants_of_lothal))
    }
    @kotlinx.serialization.Serializable
    sealed class Pack : Component() {

        @kotlinx.serialization.Serializable
        sealed class Villain : Pack() {
            @kotlinx.serialization.Serializable
            object EmperorPalpatine : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.emperorPalapatine, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object RoyalGuardChampion : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.royal_guard_champion, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object AgentBlaise : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.agent_blaise, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Bossk : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.bossk, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object GrandInquisitor : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.grand_inquisitor, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Greedo : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.greedo, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Jabba : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.jabba_the_hut, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Jawa : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.jawa, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Maul : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.maul, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Thrawn : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.thrawn, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Hondo : Villain() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.hondo_ohnaka, R.drawable.ic_launcher_background)
            }
        }
        @kotlinx.serialization.Serializable
        sealed class Ally : Pack() {
            @kotlinx.serialization.Serializable
            object Chewbacca : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.chewbacca, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object HanSolo : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.han_solo, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Ahsoka : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.ahsoka_tano, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Hera : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.hera_syndulla, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Lando : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.lando_calrissian, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object Luke : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.luke_skywalker_jedi_knight, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object ObiWan : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.obi_wan_kenobi, R.drawable.ic_launcher_background)
            }
            @kotlinx.serialization.Serializable
            object SabineZeb : Ally() {
                override val displayableParams: DisplayableParams = DisplayableParams(R.string.sabine_zeb, R.drawable.ic_launcher_background)
            }
        }
    }
}
