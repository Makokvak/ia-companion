package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class JawaVillainPack {

    val list = listOf<Resource>(

        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jawa_scavenger, R.mipmap.jawa_scavenger)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.jawa_scavenger_elite, R.mipmap.jawa_scavenger_elite)),


        Resource.AgendaSet(
            DisplayableParams(R.string.desert_scavengers, R.mipmap.salvage_operatives),
            Resource.Agenda(DisplayableParams(R.string.salvage_operatives, R.mipmap.salvage_operatives), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.salvage_operatives, R.mipmap.salvage_operatives),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.mechanical_salvage, R.mipmap.mechanical_salvage))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.ion_blasters, R.mipmap.ion_blasters), 1, true),
            Resource.Agenda(DisplayableParams(R.string.jawa_raid, R.mipmap.jawa_raid), 1, isSecret = true)
        ),
    )
}
