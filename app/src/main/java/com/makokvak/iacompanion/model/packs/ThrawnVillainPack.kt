package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class ThrawnVillainPack {

    val list = listOf(
        Resource.AgendaSet(
            DisplayableParams(R.string.the_art_of_war, R.mipmap.the_admirals_grip),
            Resource.Agenda(DisplayableParams(R.string.the_admirals_grip, R.mipmap.the_admirals_grip), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.the_admirals_grip, R.mipmap.the_admirals_grip),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards(
                        creditsPerHero = 100
                    )),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.thrawn, R.mipmap.thrawn_campaign))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.three_steps_ahead, R.mipmap.three_steps_ahead), 2, false),
            Resource.Agenda(DisplayableParams(R.string.piece_by_piece, R.mipmap.piece_by_piece), 1, isSecret = true)
        ),
    )
}
