package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R

class CoreGameResources {
    // Heroes
    val dialaPassil = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.diala_passil, R.mipmap.diala_passil_hero),
        damageDealer = 2,
        durable = 2,
        support = 2,
        range = 1,
        crowdControl = 1,
        mobility = 2,
        difficulty = 2,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.plasteel_staff, R.mipmap.plasteelstaff), 0),
            Resource.HeroSkill(DisplayableParams(R.string.force_adept, R.mipmap.forceadept), 1),
            Resource.HeroSkill(DisplayableParams(R.string.force_throw, R.mipmap.forcethrow), 1),
            Resource.HeroSkill(DisplayableParams(R.string.battle_meditation, R.mipmap.battlemeditation), 2),
            Resource.HeroSkill(DisplayableParams(R.string.defensive_stance, R.mipmap.defensivestance), 2),
            Resource.HeroSkill(DisplayableParams(R.string.art_of_movement, R.mipmap.artofmovement), 3),
            Resource.HeroSkill(DisplayableParams(R.string.snap_kick, R.mipmap.snapkick), 3),
            Resource.HeroSkill(DisplayableParams(R.string.dancing_weapon, R.mipmap.dancingweapon), 4),
            Resource.HeroSkill(DisplayableParams(R.string.way_of_the_sarlacc, R.mipmap.wayofthesarlacc), 4)
        )
    )
    val fennSignis = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.fenn_signis, R.mipmap.fenn_signis_hero),
        damageDealer = 3,
        durable = 1,
        support = 0,
        range = 2,
        mobility = 2,
        crowdControl = 1,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.infantry_rifle, R.mipmap.infantry_rifle), 0),
            Resource.HeroSkill(DisplayableParams(R.string.take_cover, R.mipmap.take_cover), 1),
            Resource.HeroSkill(DisplayableParams(R.string.tactical_movement, R.mipmap.tacticalmovement), 1),
            Resource.HeroSkill(DisplayableParams(R.string.weapon_expert, R.mipmap.weapon_expert), 2),
            Resource.HeroSkill(DisplayableParams(R.string.adrenaline_rush, R.mipmap.adrenalinerush), 2),
            Resource.HeroSkill(DisplayableParams(R.string.trench_fighter, R.mipmap.trenchfighter), 3),
            Resource.HeroSkill(DisplayableParams(R.string.suppresive_fire, R.mipmap.suppressive_fire), 3),
            Resource.HeroSkill(DisplayableParams(R.string.rebel_elite, R.mipmap.rebelelite), 4),
            Resource.HeroSkill(DisplayableParams(R.string.superior_positioning, R.mipmap.superior_positioning), 4)
        )
    )
    val gaarkhan = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.gaarkhan, R.mipmap.gaarkhan_hero),
        damageDealer = 3,
        durable = 2,
        support = 1,
        range = 0,
        mobility = 3,
        crowdControl = 1,
        difficulty = 0,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.vibro_ax, R.mipmap.vibroax), 0),
            Resource.HeroSkill(DisplayableParams(R.string.wookiee_fortitude, R.mipmap.wookieefortitude), 1),
            Resource.HeroSkill(DisplayableParams(R.string.wookiee_loyalty, R.mipmap.wookieeloyalty), 1),
            Resource.HeroSkill(DisplayableParams(R.string.ferocity, R.mipmap.ferocity), 2),
            Resource.HeroSkill(DisplayableParams(R.string.staggering_blow, R.mipmap.staggeringblow), 2),
            Resource.HeroSkill(DisplayableParams(R.string.vicious_strike, R.mipmap.viciousstrike), 3),
            Resource.HeroSkill(DisplayableParams(R.string.rampage, R.mipmap.rampage), 3),
            Resource.HeroSkill(DisplayableParams(R.string.unstoppable, R.mipmap.unstoppable), 4),
            Resource.HeroSkill(DisplayableParams(R.string.brutal_cleave, R.mipmap.brutalcleave), 4)
        )
    )
    val gideonArgus = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.gideon_argus, R.mipmap.gideon_argus_hero),
        damageDealer = 0,
        durable = 1,
        support = 3,
        range = 2,
        crowdControl = 2,
        mobility = 2,
        difficulty = 1,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.holdout_blaster, R.mipmap.holdoutblaster), 0),
            Resource.HeroSkill(DisplayableParams(R.string.called_shot, R.mipmap.called_shot), 1),
            Resource.HeroSkill(DisplayableParams(R.string.military_efficiency, R.mipmap.militaryefficiency), 1),
            Resource.HeroSkill(DisplayableParams(R.string.air_of_command, R.mipmap.airofcommand), 2),
            Resource.HeroSkill(DisplayableParams(R.string.mobile_tactician, R.mipmap.mobiletactician), 2),
            Resource.HeroSkill(DisplayableParams(R.string.for_the_cause, R.mipmap.for_the_cause), 3),
            Resource.HeroSkill(DisplayableParams(R.string.rallying_shout, R.mipmap.rallyingshout), 3),
            Resource.HeroSkill(DisplayableParams(R.string.hammer_and_anvil, R.mipmap.hammerandanvil), 4),
            Resource.HeroSkill(DisplayableParams(R.string.master_stroke, R.mipmap.masterstroke), 4)
        )
    )
    val jynOdan = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.jyn_odan, R.mipmap.jyn_odan_hero),
        damageDealer = 3,
        durable = 1,
        support = 1,
        crowdControl = 0,
        mobility = 3,
        range = 1,
        difficulty = 2,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.vintage_blaster, R.mipmap.vintageblaster), 0),
            Resource.HeroSkill(DisplayableParams(R.string.quick_as_a_whip, R.mipmap.quickasawhip), 1),
            Resource.HeroSkill(DisplayableParams(R.string.smugglers_luck, R.mipmap.smugglersluck), 1),
            Resource.HeroSkill(DisplayableParams(R.string.cheap_shot, R.mipmap.cheapshot), 2),
            Resource.HeroSkill(DisplayableParams(R.string.roll_with_it, R.mipmap.rollwithit), 2),
            Resource.HeroSkill(DisplayableParams(R.string.get_cocky, R.mipmap.getcocky), 3),
            Resource.HeroSkill(DisplayableParams(R.string.gunslinger, R.mipmap.gunslinger), 3),
            Resource.HeroSkill(DisplayableParams(R.string.sidewinder, R.mipmap.sidewinder), 4),
            Resource.HeroSkill(DisplayableParams(R.string.trick_shot, R.mipmap.trickshot), 4)
        )
    )
    val makEshkarey = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.mak_eshkarey, R.mipmap.mak_eshkarey_hero),
        damageDealer = 3,
        durable = 1,
        support = 1,
        range = 3,
        mobility = 1,
        crowdControl = 0,
        difficulty = 2,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.longblaster, R.mipmap.longblaster), 0),
            Resource.HeroSkill(DisplayableParams(R.string.disengage, R.mipmap.disengage), 1),
            Resource.HeroSkill(DisplayableParams(R.string.supply_network, R.mipmap.supplynetwork), 1),
            Resource.HeroSkill(DisplayableParams(R.string.jeswandi_training, R.mipmap.jeswanditraining), 2),
            Resource.HeroSkill(DisplayableParams(R.string.target_acquired, R.mipmap.targetacquired), 2),
            Resource.HeroSkill(DisplayableParams(R.string.execute, R.mipmap.execute), 3),
            Resource.HeroSkill(DisplayableParams(R.string.expertise, R.mipmap.expertise), 3),
            Resource.HeroSkill(DisplayableParams(R.string.decoy, R.mipmap.decoy), 4),
            Resource.HeroSkill(DisplayableParams(R.string.no_escape, R.mipmap.noescape), 4)
        )
    )
    // Forced missions
    private val darkObsession = Resource.Mission.Side(
        DisplayableParams(R.string.dark_obsession, R.mipmap.darkobsession),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.darth_vader, R.mipmap.darth_vader_lord_of_the_sith)))
            ),
            MissionResult.RebelVictory(reward = MissionRewards(
                creditsPerHero = 100
            ))
        ),
        isFromAgenda = true
    )
    private val captured = Resource.Mission.Forced(
        DisplayableParams(R.string.captured, R.mipmap.core_game),
        staticReward = MissionRewards(),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(influence = 2)
            ),
            MissionResult.RebelVictory(reward = MissionRewards())
        )
    )
    private val meansOfProduction = Resource.Mission.Side(
        DisplayableParams(R.string.means_of_production, R.mipmap.meansofproduction),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.imperial_industry, R.mipmap.imperial_industry_reward_card)))
            ),
            MissionResult.RebelVictory(reward = MissionRewards(
                creditsPerHero = 100
            ))
        ),
        isFromAgenda = true
    )
    private val breakingPoint = Resource.Mission.Side(
        DisplayableParams(R.string.breaking_point, R.mipmap.breakingpoint),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.supply_deficit, R.mipmap.supply_deficit)))
            ),
            MissionResult.RebelVictory(reward = MissionRewards(
                creditsPerHero = 100
            ))
        ),
        isFromAgenda = true
    )
    private val impounded = Resource.Mission.Forced(
        DisplayableParams(R.string.impounded, R.mipmap.impounded),
        staticReward = MissionRewards(),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.special_operations, R.mipmap.special_operations)))
            ),
            MissionResult.RebelVictory(reward = MissionRewards())
        )
    )
    private val wanted = Resource.Mission.Forced(
        DisplayableParams(R.string.wanted, R.mipmap.wanted),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.ImperialVictory(
                reward = MissionRewards(rewardForImperialPlayer = Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.old_wounds, R.mipmap.old_wounds)))
            ),
            MissionResult.RebelVictory(reward = MissionRewards())
        )
    )
    // Story missions
    private val lastStand = Resource.Mission.Final(
        DisplayableParams(R.string.last_stand, R.mipmap.last_stand),
        MissionRewards(),
        listOf())
    private val desperateHour = Resource.Mission.Final(
        DisplayableParams(R.string.desperate_hour, R.mipmap.desperate_hour),
        MissionRewards(),
        listOf())
    private val theSource = Resource.Mission.Story(
        DisplayableParams(R.string.the_source, R.mipmap.the_source),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_terminal_secured,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            ),
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_officer_freed,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = desperateHour,
                reward = MissionRewards(
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = lastStand,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val chainOfCommand = Resource.Mission.Story(
        DisplayableParams(R.string.chain_of_command, R.mipmap.chain_of_command),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_weiss_defeated_before_entering_at_st,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            ),
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_weiss_not_defeated_before_entering_at_st,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = desperateHour,
                reward = MissionRewards(
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = lastStand,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val drawnIn = Resource.Mission.Story(
        DisplayableParams(R.string.drawn_in, R.mipmap.drawn_in),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_raise_the_alarms_did_not_trigger,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            ),
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_raise_the_alarms_did_trigger,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = theSource,
                reward = MissionRewards(
                    expForImperialPlayer = 1,
                    instantForcedMission = captured
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = chainOfCommand,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val incoming = Resource.Mission.Story(
        DisplayableParams(R.string.incoming, R.mipmap.incoming),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_passage_opened,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            ),
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_passage_not_opened,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = theSource,
                reward = MissionRewards(
                    expForImperialPlayer = 1,
                    instantForcedMission = captured
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = chainOfCommand,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val imperialHospitality = Resource.Mission.Story(
        DisplayableParams(R.string.imperial_hospitality, R.mipmap.imperial_hospitality),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_terminal_destroyed,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            ),
            MissionResult.AdditionalOutcome(
                name = R.string.additional_outcome_terminal_not_destroyed,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = drawnIn,
                reward = MissionRewards(
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = incoming,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val flySolo = Resource.Mission.Story(
        DisplayableParams(R.string.fly_solo, R.mipmap.fly_solo),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = drawnIn,
                reward = MissionRewards(
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = incoming,
                reward = MissionRewards(
                    expForRebels = 1
                )
            )
        )
    )
    private val underSiege = Resource.Mission.Story(
        DisplayableParams(R.string.under_siege, R.mipmap.under_siege),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.StoryMissionResult.StoryImperialVictory(
                description = R.string.under_siege_outcome_points_secured,
                nextMission = flySolo,
                reward = MissionRewards(
                    influence = 1,
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryImperialVictory(
                description = R.string.other_imperial_victory,
                nextMission = imperialHospitality,
                reward = MissionRewards(
                    influence = 1,
                    expForEachPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = imperialHospitality,
                reward = MissionRewards(
                    creditsPerHero = 100,
                    expForRebels = 1
                )
            )
        )
    )
    private val aNewThreat = Resource.Mission.Story(
        DisplayableParams(R.string.a_new_threat, R.mipmap.a_new_threat),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = flySolo,
                reward = MissionRewards(
                    expForImperialPlayer = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = imperialHospitality,
                reward = MissionRewards(
                    expForRebels = 100
                )
            )
        )
    )
    private val aftermath = Resource.Mission.Story(
        DisplayableParams(R.string.aftermath, R.mipmap.core_game),
        staticReward = MissionRewards(
            expForEachPlayer = 1,
            creditsPerHero = 100,
            influence = 1
        ),
        possibleOutcomes = listOf(
            MissionResult.StoryMissionResult.StoryImperialVictory(
                nextMission = underSiege,
                reward = MissionRewards(
                    influence = 1
                )
            ),
            MissionResult.StoryMissionResult.StoryRebelVictory(
                nextMission = aNewThreat,
                reward = MissionRewards(
                    creditsPerHero = 100
                )
            )
        )
    )

    val list = listOf(
        // Campaign
        Resource.Campaign(DisplayableParams(R.string.core_game_campaign, R.mipmap.core_game), firstMission =  aftermath, details = listOf(
            CampaignMissionDetails(threatLevel = 2, tiers = setOf(ItemTier.First), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 2, tiers = setOf(ItemTier.First), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 3, tiers = setOf(ItemTier.First), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 3, tiers = setOf(ItemTier.First, ItemTier.Second), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 4, tiers = setOf(ItemTier.Second, ItemTier.Third), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 5, tiers = setOf(ItemTier.Third), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 5, tiers = setOf(ItemTier.Third), isSlotForSideMission = true),
            CampaignMissionDetails(threatLevel = 6, tiers = setOf(ItemTier.Third), isSlotForSideMission = false),
            CampaignMissionDetails(threatLevel = 6, tiers = setOf(), isSlotForSideMission = false)
        )),
        // Heroes
        dialaPassil,
        gaarkhan,
        gideonArgus,
        jynOdan,
        makEshkarey,
        fennSignis,
        // Allies
        Resource.Ally(DisplayableParams(R.string.luke_skywalker, R.mipmap.luke_skywalker)),
        Resource.Ally(DisplayableParams(R.string.rebel_trooper, R.mipmap.rebel_trooper)),
        Resource.Ally(DisplayableParams(R.string.rebel_saboteur, R.mipmap.rebel_saboteur_2)),
        Resource.Ally(DisplayableParams(R.string.rebel_high_command, R.mipmap.rebel_high_command)),
        Resource.Ally(DisplayableParams(R.string.chewbacca, R.mipmap.chewbacca)),
        Resource.Ally(DisplayableParams(R.string.han_solo, R.mipmap.han_solo)),
        // Deployables
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.stormtrooper, R.mipmap.stormtrooper)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.stormtrooper_elite, R.mipmap.stormtrooper_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.imperial_officer, R.mipmap.imperial_officer)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.imperial_officer_elite, R.mipmap.imperial_officer_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.e_web_engineer, R.mipmap.e_web_engineer)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.e_web_engineer_elite, R.mipmap.e_web_engineer_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.royal_guard, R.mipmap.royal_guard)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.royal_guard_elite, R.mipmap.royal_guard_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.probe_droid, R.mipmap.probe_droid)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.probe_droid_elite, R.mipmap.probe_droid_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.at_st, R.mipmap.at_st)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.darth_vader, R.mipmap.darth_vader_lord_of_the_sith)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.general_weiss, R.mipmap.general_weiss)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.royal_guard_champion, R.mipmap.royal_guard_champion)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.trandoshan_hunter, R.mipmap.trandoshan_hunter)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.trandoshan_hunter_elite, R.mipmap.trandoshan_hunter_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.nexu, R.mipmap.nexu)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.nexu_elite, R.mipmap.nexu_elite)),
        Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.ig_88, R.mipmap.ig_88)),
        // Story missions
        aNewThreat,
        underSiege,
        imperialHospitality,
        incoming,
        drawnIn,
        chainOfCommand,
        theSource,
        lastStand,
        desperateHour,
        // Side missions
        Resource.Mission.Side(
            DisplayableParams(R.string.brushfire, R.mipmap.brushfire),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.veteran_prowess, R.mipmap.veteran_prowess)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = fennSignis
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.temptation, R.mipmap.temptation),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.shu_yens_lightsaber, R.mipmap.shu_yens_lightsaber)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = dialaPassil
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.high_moon, R.mipmap.high_moon),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.peacemaker, R.mipmap.peacemaker)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = jynOdan
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.indebted, R.mipmap.indebted),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.life_debt, R.mipmap.life_debt)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = gaarkhan
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.loose_canon, R.mipmap.loose_cannon),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.shadow_suit, R.mipmap.shadow_suit)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = makEshkarey
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.friends_of_old, R.mipmap.friends_of_old),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.fearless_leader, R.mipmap.fearless_leader)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            forCharacter = gideonArgus
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.sorry_about_the_mess, R.mipmap.sorry_about_the_mess),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.han_solo, R.mipmap.han_solo)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.homecoming, R.mipmap.homecoming),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.luke_skywalker, R.mipmap.luke_skywalker)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.the_spice_job, R.mipmap.the_spice_job),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.chewbacca, R.mipmap.chewbacca)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.target_of_opportunity, R.mipmap.target_of_opportunity),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    earnedAllies = listOf(Resource.Ally(DisplayableParams(R.string.rebel_saboteur, R.mipmap.rebel_saboteur_2)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            )
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.vipers_den, R.mipmap.vipers_den),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.rebel_recon, R.mipmap.rebel_recon)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            isRandom = true
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.a_simple_task, R.mipmap.a_simple_task),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.adrenal_implant, R.mipmap.adrenal_implant)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            isRandom = true
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.generous_donations, R.mipmap.generous_donations),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.ManualOutcome(R.string.set_manual_outcome, promptManualCreditOutcomeForRebels = true, promptManualInfluenceOutcome = true)
            ),
            isRandom = true
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.luxury_cruise, R.mipmap.luxury_cruise),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.allied_operations, R.mipmap.allied_operations)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            isRandom = true
        ),
        Resource.Mission.Side(
            DisplayableParams(R.string.sympathy_for_the_rebellion, R.mipmap.sympathy_for_the_rebellion),
            staticReward = MissionRewards(
                expForEachPlayer = 1,
                creditsPerHero = 100,
                influence = 1
            ),
            possibleOutcomes = listOf(
                MissionResult.RebelVictory(reward = MissionRewards(
                    rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.the_ways_of_the_force, R.mipmap.the_ways_of_the_force)))
                )),
                MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
            ),
            isRandom = true
        ),
        // Agendas
        Resource.AgendaSet(
            DisplayableParams(R.string.lord_vaders_command, R.mipmap.darkobsession),
            Resource.Agenda(DisplayableParams(R.string.a_dark_power, R.mipmap.adarkpower), 2, true),
            Resource.Agenda(DisplayableParams(R.string.as_you_wish, R.mipmap.asyouwish), 1, true),
            Resource.Agenda(DisplayableParams(R.string.dark_obsession, R.mipmap.darkobsession), 3, sideMission = darkObsession)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.imperial_industry, R.mipmap.meansofproduction),
            Resource.Agenda(DisplayableParams(R.string.interrogation_protocol, R.mipmap.interrogationprotocol), 1, true),
            Resource.Agenda(DisplayableParams(R.string.means_of_production, R.mipmap.meansofproduction), 3, sideMission = meansOfProduction),
            Resource.Agenda(DisplayableParams(R.string.restorative_supplies, R.mipmap.restorativesupplies), 2)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.retaliation, R.mipmap.breakingpoint),
            Resource.Agenda(DisplayableParams(R.string.breaking_point, R.mipmap.breakingpoint), 3, sideMission = breakingPoint),
            Resource.Agenda(DisplayableParams(R.string.tactical_explosives, R.mipmap.tacticalexplosives), 1, true),
            Resource.Agenda(DisplayableParams(R.string.weakness_revealed, R.mipmap.weaknessrevealed), 2)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.agents_of_the_empire, R.mipmap.impounded),
            Resource.Agenda(DisplayableParams(R.string.impounded, R.mipmap.impounded), 4, forcedMission = impounded),
            Resource.Agenda(DisplayableParams(R.string.imperial_informants, R.mipmap.imperialinformants), 1, true),
            Resource.Agenda(DisplayableParams(R.string.tracking_beacon, R.mipmap.trackingbeacon), 1)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.for_the_right_price, R.mipmap.wanted),
            Resource.Agenda(DisplayableParams(R.string.high_value_target, R.mipmap.highvaluetarget), 2),
            Resource.Agenda(DisplayableParams(R.string.hired_help, R.mipmap.hiredhelp), 1, true),
            Resource.Agenda(DisplayableParams(R.string.wanted, R.mipmap.wanted), 4, forcedMission = wanted)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.imperial_discipline, R.mipmap.fireatwill),
            Resource.Agenda(DisplayableParams(R.string.fire_at_will, R.mipmap.fireatwill), 2),
            Resource.Agenda(DisplayableParams(R.string.impending_doom, R.mipmap.impendingdoom), 1, true),
            Resource.Agenda(DisplayableParams(R.string.tactical_maneuvering, R.mipmap.tacticalmaneuvering), 1, true)
        ),
        Resource.AgendaSet(
            DisplayableParams(R.string.imperial_security_bureau, R.mipmap.imperialoperative),
            Resource.Agenda(DisplayableParams(R.string.imperial_operative, R.mipmap.imperialoperative), 1, true),
            Resource.Agenda(DisplayableParams(R.string.internal_affairs, R.mipmap.internalaffairs), 2),
            Resource.Agenda(DisplayableParams(R.string.isb_enforcers, R.mipmap.isbenforcers), 2, true)
        ),
        // Imperial class decks
        Resource.ImperialClassDeck(
            DisplayableParams(R.string.military_might, R.mipmap.militarymight),
            R.string.military_might_description,
            listOf(
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.show_of_force, R.mipmap.showofforce), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.combat_medic, R.mipmap.combatmedic), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.riot_grenades, R.mipmap.riotgrenades), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.assault_armor, R.mipmap.assaultarmor), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.endless_ranks, R.mipmap.endlessranks), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.shock_troopers, R.mipmap.shocktroopers), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.sustained_fire, R.mipmap.sustainedfire), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.combat_veterans, R.mipmap.combatveterans), xpCost = 4),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.shock_and_awe, R.mipmap.shockandawe), xpCost = 4)
            )
        ),
        Resource.ImperialClassDeck(
            DisplayableParams(R.string.technological_superiority, R.mipmap.technologicalsuperiority),
            R.string.technological_superiority_description,
            listOf(
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.experimental_arms, R.mipmap.experimentalarms), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.jetpacks, R.mipmap.jetpacks), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.technical_support, R.mipmap.technicalsupport), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.failsafe, R.mipmap.failsafe), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.hidden_detonators, R.mipmap.hiddendetonators), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.cloaking_device, R.mipmap.cloakingdevice), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.arc_blasters, R.mipmap.arcblasters), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.superior_augments, R.mipmap.superioraugments), xpCost = 4),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.adaptive_weapons, R.mipmap.adaptiveweapons), xpCost = 4)
            )
        ),
        Resource.ImperialClassDeck(
            DisplayableParams(R.string.subversive_tactics, R.mipmap.subversive_tactics),
            R.string.subversive_tactics_description,
            listOf(
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.prey_upon_doubt, R.mipmap.prey_upon_doubt), xpCost = 0),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.savage_weaponry, R.mipmap.savage_weaponry), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.surgicalstrike, R.mipmap.surgicalstrike), xpCost = 1),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.heavypressure, R.mipmap.heavypressure), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.exploit_weakness, R.mipmap.exploit_weakness), xpCost = 2),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.executioner, R.mipmap.executioner), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.wearytarget, R.mipmap.wearytarget), xpCost = 3),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.oppression, R.mipmap.oppression), xpCost = 4),
                Resource.ImperialClassDeckCard(DisplayableParams(R.string.no_quarter, R.mipmap.no_quarter), xpCost = 4)
            )
        ),
        // Supply
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.shock_grenade, R.mipmap.shock_grenade), countInDeck = 2),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.adrenal_stim, R.mipmap.adrenal_stim), countInDeck = 2),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.emergency_medpack, R.mipmap.emergency_medpac), countInDeck = 2),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.bacta_infusion, R.mipmap.bacta_infusion), countInDeck = 2),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.valuable_goods, R.mipmap.valuable_goods)),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.c22_frag_grenade, R.mipmap.c22_frag_grenade)),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.troop_data, R.mipmap.troop_data)),
        Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.c1_comlink, R.mipmap.c1_comlink)),
        // Items
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.dh_17, R.mipmap.dh_17), tier = ItemTier.First, cost = 200),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.e_11, R.mipmap.e_11), tier = ItemTier.First, cost = 400),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.dl_44, R.mipmap.dl_44), tier = ItemTier.First, cost = 500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.vibroblade, R.mipmap.vibroblade), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.armored_gauntlets, R.mipmap.armored_gauntlets), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.balanced_hilt, R.mipmap.balanced_hilt), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.extended_haft, R.mipmap.extended_haft), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.tactical_display, R.mipmap.tactical_display), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.marksman_barrel, R.mipmap.marksman_barrel), tier = ItemTier.First, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.combat_coat, R.mipmap.combat_coat), tier = ItemTier.First, cost = 500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.survival_gear, R.mipmap.survival_gear), tier = ItemTier.First, cost = 250),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.portable_medkit, R.mipmap.portable_medkit), tier = ItemTier.First, cost = 450),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.a280, R.mipmap.a280), tier = ItemTier.Second, cost = 600),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.t_21, R.mipmap.t_21), tier = ItemTier.Second, cost = 900),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.x434_deathhammer, R.mipmap.x434_deathhammer), tier = ItemTier.Second, cost = 600),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.vibro_knucklers, R.mipmap.vibro_knucklers), tier = ItemTier.Second, cost = 400),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.bd_1_vibro_ax, R.mipmap.bd_1_vibro_ax), tier = ItemTier.Second, cost = 600),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.high_impact_guard, R.mipmap.high_impact_guard), tier = ItemTier.Second, cost = 500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.spread_barrel, R.mipmap.spread_barrel), tier = ItemTier.Second, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.overcharger, R.mipmap.overcharger), tier = ItemTier.Second, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.laminate_armor, R.mipmap.laminate_armor), tier = ItemTier.Second, cost = 700),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.extra_ammunition, R.mipmap.extra_ammunition), tier = ItemTier.Second, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.slicing_tools, R.mipmap.slicing_tools), tier = ItemTier.Second, cost = 250),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.pulse_cannon, R.mipmap.pulse_cannon), tier = ItemTier.Third, cost = 1200),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.dxr_6, R.mipmap.dxr_6), tier = ItemTier.Third, cost = 1000),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.sporting_blaster, R.mipmap.sporting_blaster), tier = ItemTier.Third, cost = 900),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.force_pike, R.mipmap.force_pike), tier = ItemTier.Third, cost = 1000),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.personal_shields, R.mipmap.personal_shields), tier = ItemTier.Third, cost = 550),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.reinforced_helmet, R.mipmap.reinforced_helmet), tier = ItemTier.Third, cost = 300),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.combat_visor, R.mipmap.combat_visor), tier = ItemTier.Third, cost = 3500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.combat_knife, R.mipmap.combat_knife), tier = ItemTier.Third, cost = 500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.shock_emitter, R.mipmap.shock_emitter), tier = ItemTier.Third, cost = 500),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.disruption_cell, R.mipmap.disruption_cell), tier = ItemTier.Third, cost = 600),
        Resource.ResourceWithReferences.Item(DisplayableParams(R.string.telescoping_sights, R.mipmap.telescoping_sights), tier = ItemTier.Third, cost = 400)
    )
}
