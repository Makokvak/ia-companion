package com.makokvak.iacompanion.model.packs

import com.makokvak.iacompanion.R
import com.makokvak.iacompanion.model.*

class PalpatineVillainPack {

    val list = listOf(
        Resource.AgendaSet(
            DisplayableParams(R.string.the_emperors_plots, R.mipmap.insidious),
            Resource.Agenda(DisplayableParams(R.string.insidious, R.mipmap.insidious), 3, sideMission = Resource.Mission.Side(
                DisplayableParams(R.string.insidious, R.mipmap.insidious),
                staticReward = MissionRewards(
                    expForEachPlayer = 1,
                    creditsPerHero = 100,
                    influence = 1
                ),
                possibleOutcomes = listOf(
                    MissionResult.RebelVictory(reward = MissionRewards()),
                    MissionResult.ImperialVictory(reward = MissionRewards(
                        earnedVillain = Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.emperorPalapatine, R.mipmap.emperor_palpatine))
                    ))
                ),
                isFromAgenda = true
            )),
            Resource.Agenda(DisplayableParams(R.string.isolation, R.mipmap.isolation), 1, true),
            Resource.Agenda(DisplayableParams(R.string.for_the_emperor, R.mipmap.for_the_emperor), 1, isSecret = true)
        ),
    )
}
