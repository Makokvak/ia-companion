package com.makokvak.iacompanion.model

import com.makokvak.iacompanion.R

class BespinGambitResources {

    val davithElso = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.davith_elso, R.mipmap.devithherosheet),
        damageDealer = 3,
        durable = 2,
        support = 1,
        range = 0,
        crowdControl = 0,
        mobility = 3,
        difficulty = 2,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.heirloom_dagger, R.mipmap.heirloomdagger), 0),
            Resource.HeroSkill(DisplayableParams(R.string.elusive_agent, R.mipmap.dlusiveagent), 1),
            Resource.HeroSkill(DisplayableParams(R.string.covert_operative, R.mipmap.covertoperative), 1),
            Resource.HeroSkill(DisplayableParams(R.string.blindside, R.mipmap.blindside), 2),
            Resource.HeroSkill(DisplayableParams(R.string.falling_leaf, R.mipmap.fallingleaf), 2),
            Resource.HeroSkill(DisplayableParams(R.string.shrouded_lightsaber, R.mipmap.shrouded), 3),
            Resource.HeroSkill(DisplayableParams(R.string.force_illusion, R.mipmap.forceillusion), 3),
            Resource.HeroSkill(DisplayableParams(R.string.embody_the_force, R.mipmap.embodytheforce), 4),
            Resource.HeroSkill(DisplayableParams(R.string.fell_swoop, R.mipmap.fellswoop), 4)
        )
    )
    val murneRin = Resource.ResourceWithReferences.Hero(DisplayableParams(R.string.murne_rin, R.mipmap.murne_rin_hero),
        damageDealer = 0,
        durable = 0,
        support = 3,
        range = 2,
        crowdControl = 3,
        mobility = 1,
        difficulty = 3,
        skillDeck = listOf(
            Resource.HeroSkill(DisplayableParams(R.string.diplomats_blaster, R.mipmap.diplomat), 0),
            Resource.HeroSkill(DisplayableParams(R.string.company_of_heroes, R.mipmap.company), 1),
            Resource.HeroSkill(DisplayableParams(R.string.sonic_bellow, R.mipmap.sonic), 1),
            Resource.HeroSkill(DisplayableParams(R.string.professional_aide, R.mipmap.professional_aide), 2),
            Resource.HeroSkill(DisplayableParams(R.string.rebel_propaganda, R.mipmap.rebel_propaganda), 2),
            Resource.HeroSkill(DisplayableParams(R.string.double_agent, R.mipmap.double_agent), 3),
            Resource.HeroSkill(DisplayableParams(R.string.solidarity, R.mipmap.solid), 3),
            Resource.HeroSkill(DisplayableParams(R.string.lead_from_the_front, R.mipmap.lead_from_the_front), 4),
            Resource.HeroSkill(DisplayableParams(R.string.waylay, R.mipmap.waylay), 4)
        )
    )

     val list = listOf(
         murneRin,
         davithElso,
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.shadowsilk_cloak, R.mipmap.shadowsilk_cloak), tier = ItemTier.First, cost = 350),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.vibrosword, R.mipmap.vibrosword), tier = ItemTier.First, cost = 350),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.r5_astromech, R.mipmap.r5astromech_1), tier = ItemTier.Second, cost = 250, references = listOf(
             Resource.Misc(DisplayableParams(R.string.r5_astromech, R.mipmap.r5_astromech_companion))
         )),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.weighted_head, R.mipmap.weighted_head), tier = ItemTier.Second, cost = 400),

         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.disruptor_pistol, R.mipmap.disruptor_pistol), tier = ItemTier.Third, cost = 700),
         Resource.ResourceWithReferences.Item(DisplayableParams(R.string.sniper_scope, R.mipmap.sniper_scope), tier = ItemTier.Third, cost = 250),

         Resource.Ally(DisplayableParams(R.string.lando_calrissian, R.mipmap.lando_calrissian)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.agent_blaise, R.mipmap.agent_blaise_campaign)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.wing_guard, R.mipmap.wing_guard)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.wing_guard_elite, R.mipmap.wing_guard_elite)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.bossk, R.mipmap.bossk)),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.ugnaught_tinkerer, R.mipmap.swi24_ugnaught_tinkerer), listOf(
             Resource.Misc(DisplayableParams(R.string.junk_droid, R.mipmap.junk_droid))
         )),
         Resource.ResourceWithReferences.Deployable(DisplayableParams(R.string.ugnaught_tinkerer_elite, R.mipmap.swi24_ugnaught_tinkerer_elite), listOf(
             Resource.Misc(DisplayableParams(R.string.junk_droid, R.mipmap.junk_droid))
         )),
         Resource.Mission.Side(
             DisplayableParams(R.string.cloud_citys_secret, R.mipmap.cloud_citys_secret),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.under_the_Radar, R.mipmap.under_the_radar)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.freedom_fighters, R.mipmap.freedom_fighters),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.lobots_favor, R.mipmap.lobots_favor)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.hostile_takeover, R.mipmap.hostile_takeover),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.relief_effort, R.mipmap.relief_effort)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             isRandom = true
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.panic_in_the_streets, R.mipmap.panic_in_the_streets),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.cam_droid, R.mipmap.camdroidreward),
                        listOf(Resource.Misc(DisplayableParams(R.string.cam_droid, R.mipmap.cam_droid_companion)))))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = murneRin
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.reclamation, R.mipmap.reclamation),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.lando_calrissian, R.mipmap.lando_calrissian)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             )
         ),
         Resource.Mission.Side(
             DisplayableParams(R.string.reclassified, R.mipmap.reclassified),
             staticReward = MissionRewards(
                 expForEachPlayer = 1,
                 creditsPerHero = 100,
                 influence = 1
             ),
             possibleOutcomes = listOf(
                 MissionResult.RebelVictory(reward = MissionRewards(
                     rewardsForRebels = setOf(Resource.ResourceWithReferences.Reward(DisplayableParams(R.string.radiant_holocron, R.mipmap.radiant_holocron)))
                 )),
                 MissionResult.ImperialVictory(reward = MissionRewards(influence = 2))
             ),
             forCharacter = davithElso
         ),
         Resource.AgendaSet(
             DisplayableParams(R.string.evasive_maneuvers, R.mipmap.contingency_plan),
             Resource.Agenda(DisplayableParams(R.string.a_dark_power, R.mipmap.contingency_plan), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.as_you_wish, R.mipmap.feigned_retreat), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.dark_obsession, R.mipmap.hasty_ambush), 2, isSecret = true)
         ),
         Resource.AgendaSet(
             DisplayableParams(R.string.enhanced_interrogation, R.mipmap.blackmail),
             Resource.Agenda(DisplayableParams(R.string.blackmail, R.mipmap.blackmail), 1),
             Resource.Agenda(DisplayableParams(R.string.learn_their_weaknesses, R.mipmap.learn_their_weaknesses), 1, isSecret = true),
             Resource.Agenda(DisplayableParams(R.string.weak_links, R.mipmap.weak_links), 2, isSecret = true)
         ),
         Resource.ImperialClassDeck(
             DisplayableParams(R.string.imperial_black_ops, R.mipmap.imperial_black_ops),
             R.string.imperial_black_ops_description,
             listOf(
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.show_of_force, R.mipmap.in_the_shadows_imperial_class_card), xpCost = 0),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.combat_medic, R.mipmap.shadow_corps), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.riot_grenades, R.mipmap.stealth), xpCost = 1),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.assault_armor, R.mipmap.shadow_armor), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.endless_ranks, R.mipmap.surprise_attack), xpCost = 2),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.shock_troopers, R.mipmap.execution_squad), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.sustained_fire, R.mipmap.strategic_redeployment), xpCost = 3),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.combat_veterans, R.mipmap.true_shadow), xpCost = 4),
                 Resource.ImperialClassDeckCard(DisplayableParams(R.string.shock_and_awe, R.mipmap.versatility), xpCost = 4)
             )
         ),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.flash_emitter, R.mipmap.flash_emitter), countInDeck = 1),
         Resource.ResourceWithReferences.Supply(DisplayableParams(R.string.turbocharger, R.mipmap.turbocharger), countInDeck = 1),
     )
}
