package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.ComponentResourceMap
import com.makokvak.iacompanion.model.ItemTier
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.add
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class ShopDealer(private val sessionManager: SessionManager, private val map: ComponentResourceMap) : BaseViewModel() {

    private fun getAvailableItems(tier: ItemTier) =
        map.getByResourceTypeAndComponents<Resource.ResourceWithReferences.Item>(sessionManager.session.components.value)
            .minus(sessionManager.session.ownedItems.value)
            .filter { it.tier == tier }

    private fun getTiersAvailable(): Set<ItemTier> {
        val missionDetails = sessionManager.session.campaign.value!!.details[sessionManager.session.missionsCompletedCount.value]
        return missionDetails.tiers
    }

    fun openShop(): List<Resource.ResourceWithReferences.Item> {

        val tiers = getTiersAvailable()
        val itemsInEachTier = tiers.map { getAvailableItems(it) }

        val result = mutableListOf<Resource.ResourceWithReferences.Item>()

        itemsInEachTier.forEach {
            val size = it.size
            if(size < 1) return@forEach

            val items = it.shuffled().take((size + 1) / 2)
            result.addAll(items)
        }

        return result
    }
}
