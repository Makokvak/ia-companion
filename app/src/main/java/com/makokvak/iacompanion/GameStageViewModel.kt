package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.map
import com.makokvak.iacompanion.util.nonNullLiveDataOf
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

abstract class GameStageViewModel(private val sessionManager: SessionManager) : BaseViewModel() {

    val heroes = sessionManager.session.chosenHeroes
    val heroesAndImperialPlayer = heroes.map { list -> list.map { it as Resource }.toMutableList().apply { add(sessionManager.session.chosenImperialClassDeck.value as Resource) } }
    val agenda = sessionManager.session.chosenAgendaDecks
    val influence = sessionManager.session.influence
    val credits = sessionManager.session.credits

    val missionsCompletedCount = sessionManager.session.missionsCompletedCount
    val totalMissionsCount = sessionManager.session.campaign.map {
        val campaign = it ?: throw AssertionError()
        campaign.details.size
    }

    val mission = sessionManager.session.activeMission
    val threat = sessionManager.session.threat

    val heroSkills = sessionManager.session.heroSkills

    fun getHeroExperience(hero: Resource) =
        sessionManager.getHeroExperience(hero)
}
