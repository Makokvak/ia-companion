package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.MutableNonNullLiveData
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.addRange
import com.makokvak.iacompanion.util.remove
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class ChooseOutcomeViewModel(
    private val sessionManager: SessionManager,
    private val missionDealer: MissionDealer,
    private val shopDealer: ShopDealer,
    private val agendaDealer: AgendaDealer
) : BaseViewModel() {

    private val _data = MutableNonNullLiveData(sessionManager.session.activeMission.value!!.possibleOutcomes)
    val data: NonNullLiveData<List<MissionResult>> = _data

    val checkedItems = mutableListOf<MissionResult>()

    fun confirmSelection(credits: Int, influence: Int): Boolean {

        val mission = sessionManager.session.activeMission.value ?: throw AssertionError()

        sessionManager.finishActiveMission(
            checkedItems,
            if(shouldPromptCredits()) credits else 0,
            if(shouldPromptInfluence()) influence else 0
        )

        if(mission is Resource.Mission.Story) {
            val storyOutcome = checkedItems.filterIsInstance<MissionResult.StoryMissionResult>()
            if(storyOutcome.isNotEmpty())
                missionDealer.dealStoryMissions(mission, storyOutcome.first())

            if(mission.nextMissions.isNotEmpty()) {
                val missionsToDelete = sessionManager.session.missions.value.filterIsInstance<Resource.Mission.Story>()
                missionsToDelete.forEach {
                    sessionManager.session.missions.remove(it)
                }

                sessionManager.session.missions.addRange(mission.nextMissions)
            }
        }

        val extraAgendaCardsToDraw = checkedItems.sumBy { it.reward.drawMoreAgendaCards }
        sessionManager.session.extraAgendaCardsToDraw += extraAgendaCardsToDraw

        sessionManager.session.currentShopItems.value = shopDealer.openShop()
        sessionManager.session.currentAgendaShopItems.value = agendaDealer.dealAgendas()

        missionDealer.dealSideMissions()
        return true
    }

    fun setChecked(result: MissionResult, checked: Boolean) {
        if(!checked)
            checkedItems.remove(result)
        else
            checkedItems.add(result)
    }

    // always due to supply boxes
    fun shouldPromptCredits() =
        true

    fun shouldPromptInfluence() =
        sessionManager.session.activeMission.value?.possibleOutcomes
            ?.filterIsInstance<MissionResult.ManualOutcome>()
            ?.firstOrNull()
            ?.promptManualInfluenceOutcome == true
}
