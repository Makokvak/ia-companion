package com.makokvak.iacompanion

import com.makokvak.iacompanion.util.nonNullLiveDataOf
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class ResourceTypeMenuViewModel : BaseViewModel() {

    val data = nonNullLiveDataOf(ResourceType.values().toList())
}
