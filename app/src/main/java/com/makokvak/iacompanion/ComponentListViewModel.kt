package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.ComponentResourceMap
import com.makokvak.iacompanion.util.*
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class ComponentListViewModel(
    componentResourceMap: ComponentResourceMap,
    private val sessionManager: SessionManager,
    private val selectionHandler: SelectionHandler
) : BaseViewModel() {

    val _data = MutableNonNullLiveData(componentResourceMap.components)
    val data: NonNullLiveData<List<Component>> = _data

    fun setMaxSelection(count: Int) {
        selectionHandler.maxItemsSelected = count
    }

    fun addAllComponentsItem() {
        if(!data.value.contains(Component.AllComponents))
            _data.add(Component.AllComponents)
    }

    val selectedData = selectionHandler.selectedIndices.map {
        it.map { index -> data.value[index] }
    }

    fun toggleItemSelection(index: Int) {
        selectionHandler.toggleItemSelection(index)
    }

    fun isSelected(index: Int) =
        selectionHandler.isSelected(index)

    fun selectAll() {
        for(index in data.value.indices)
            selectionHandler.selectItem(index)
    }

    fun resetSelection() {
        selectionHandler.clearAllSelections()
    }

    fun confirmSelection() {
        sessionManager.session.components.value = selectedData.value
    }
}
