package com.makokvak.iacompanion

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.makokvak.iacompanion.util.util.dpToPx

class HeightSetItemDecoration(val dp: Int = 8) : RecyclerView.ItemDecoration() {



    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val spanCount = ((parent.layoutManager as? GridLayoutManager)?.spanCount)
            ?: ((parent.layoutManager as? StaggeredGridLayoutManager)?.spanCount) ?: return

        val position = parent.getChildAdapterPosition(view)
        val itemCount = parent.adapter?.itemCount ?: 0
        outRect.bottom = if (position >= (itemCount - spanCount)) dpToPx(dp) else 0
    }
}
