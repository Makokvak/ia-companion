package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.BaseFragment
import com.makokvak.iacompanion.util.extension.setWidthBoundByAspect
import com.makokvak.iacompanion.util.util.GridItemSpacingDecoration
import com.makokvak.iacompanion.util.util.LayoutResourceRecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.LifecycleAwareRecyclerViewAdapter
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_campaign_stage.*
import kotlinx.android.synthetic.main.item_detail_info.view.*
import kotlinx.android.synthetic.main.item_resource.view.*
import org.koin.android.ext.android.inject
import kotlin.reflect.KClass

open class GameStageFragment<ViewModel : GameStageViewModel>(@LayoutRes layoutRes: Int, viewModelClass: KClass<ViewModel>) : BaseFragment<ViewModel>(
    layoutRes,
    viewModelClass
) {

    private val target: Navigation.GameStageTarget by inject()

    private val heroesAdapter by lazy {
        LifecycleAwareRecyclerViewAdapter(
            viewLifecycleOwner,
            viewModel.heroesAndImperialPlayer,
            ResourceItemView.Factory(),
            { itemView, data, _ ->
                itemView.imageView.setImageResource(data.displayableParams.drawable)
                itemView.textView_resource_name.visibility = View.VISIBLE
                itemView.textView_resource_name.text = getString(R.string.experience_template, viewModel.getHeroExperience(data))
                itemView.setOnClickListener {
                    onHeroClicked(data)
                }
            },
            { view ->
                view.layoutParams = view.layoutParams.apply { height = (recyclerView_heroes.height - dpToPx(32)) }
                view.setWidthBoundByAspect(view.layoutParams.height, ResourceType.Heroes.heightAspectRatioMultiplier)
            }
        )
    }

    open fun onHeroClicked(hero: Resource) {
        target.toBuyHeroSkillsResourceList.go(fragmentContainer = baseActivity, arg = hero)
    }

    protected val horizontalLayoutManager: RecyclerView.LayoutManager
        get() = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout_missions.textView_key.text = getString(R.string.missions_completed)
        viewModel.missionsCompletedCount.observe(viewLifecycleOwner) {
            layout_missions.textView_value.text = getString(R.string.missions_template, it, viewModel.totalMissionsCount.value)
        }

        layout_threat.textView_key.text = getString(R.string.threat)
        viewModel.threat.observe(viewLifecycleOwner) {
            layout_threat.textView_value.text = it.toString()
        }

        layout_influence.textView_key.text = getString(R.string.influence)
        viewModel.influence.observe(viewLifecycleOwner) {
            layout_influence.textView_value.text = it.toString()
        }

        layout_credits.textView_key.text = getString(R.string.credits)
        viewModel.credits.observe(viewLifecycleOwner) {
            layout_credits.textView_value.text = it.toString()
        }

        viewModel.heroSkills.observe(viewLifecycleOwner) {
            // Refresh after player bought a skill
            viewModel.heroes.value = viewModel.heroes.value
        }

        recyclerView_heroes.adapter = heroesAdapter
        recyclerView_heroes.layoutManager = horizontalLayoutManager
        recyclerView_heroes.addItemDecoration(GridItemSpacingDecoration(dp = 16, isVertical = false))

        button_ownedItems.setOnClickListener {
            target.toOwnedItems.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Items, Component.AllComponents))
        }

        button_earnedRewards.setOnClickListener {
            target.toEarnedRewards.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Rewards, Component.AllComponents))
        }

        button_earnedImperialRewards.setOnClickListener {
            target.toImoperialEarnedRewards.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Rewards, Component.AllComponents))
        }

        button_ongoingAgenda.setOnClickListener {
            target.toOngoingAgendas.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Agendas, Component.AllComponents))
        }

        button_ownedVillains.setOnClickListener {
            target.toOwnedVillains.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Deployables, Component.AllComponents))
        }

        button_ownedAllies.setOnClickListener {
            target.toOwnedAllies.go(fragmentContainer = baseActivity, arg = Navigation.ResourceListParams(ResourceType.Allies, Component.AllComponents))
        }
    }
}
