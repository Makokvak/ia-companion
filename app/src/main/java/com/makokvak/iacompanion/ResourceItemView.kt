package com.makokvak.iacompanion

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.dpToPx

class ResourceItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : CardView(context, attrs, defStyle) {

    class Factory : RecyclerViewItemFactory<ResourceItemView> {
        override fun makeView(context: Context, parent: ViewGroup): ResourceItemView {
            return ResourceItemView(context)
        }
    }

    val imageView by lazy { findViewById<ImageView>(R.id.imageView) }
    val textView by lazy { findViewById<TextView>(R.id.textView_resource_name) }

    private val standardBackgroundColor by lazy {
        ContextCompat.getColor(
            context,
            R.color.backgroundLight
        )
    }
    private val highlightColor by lazy { ContextCompat.getColor(context, R.color.secondaryDarkColor) }
    private val ownedColor by lazy { ContextCompat.getColor(context, R.color.confirmColor) }

    init {
        inflate(getContext(), R.layout.item_resource, this)
        layoutParams =
            CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, dpToPx(300))
        radius = dpToPx(12).toFloat()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            elevation = dpToPx(6).toFloat()
        }
        setHighlighted(false)
        //android:background="?attr/selectableItemBackground"
    }

    fun setHighlighted(boolean: Boolean) {
        setCardBackgroundColor(if (boolean) highlightColor else standardBackgroundColor)
    }

    fun setOwned(boolean: Boolean) {
        setCardBackgroundColor(if (boolean) ownedColor else standardBackgroundColor)
    }
}
