package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_mission_stage.*
import org.koin.android.ext.android.inject

class MissionStageFragment : GameStageFragment<MissionStageViewModel>(
    R.layout.fragment_mission_stage,
    MissionStageViewModel::class
) {

    private val target: Navigation.MissionStageTarget by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mission = target.retrieveArgument(fragment = this)
        viewModel.setActiveMission(mission)
        imageView_mission.setImageResource(mission.displayableParams.drawable)

        imageView_mission.setOnClickListener {
            target.toDetail.go(fragmentContainer = baseActivity, arg = mission)
        }

        textView_mission_name.text = getString(mission.displayableParams.name)

        button_finish_mission.setOnClickListener {
            target.toChooseOutcome.go(fragmentContainer = baseActivity)
        }
    }
}
