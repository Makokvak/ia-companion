package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.*

class CampaignStageViewModel(sessionManager: SessionManager) : GameStageViewModel(sessionManager) {

    val missions = sessionManager.session.missions
    val shop: NonNullLiveData<List<Resource.ResourceWithReferences.Item>> = sessionManager.session.currentShopItems

    val agendas: NonNullLiveData<List<Resource.Agenda>> = sessionManager.session.currentAgendaShopItems

    init {

        // Remove bought items from shop
        compositeLiveDataDisposable += sessionManager.session.ownedItems.observeForeverWithDisposable {
            it.forEach { item ->
                if(shop.value.contains(item))
                    sessionManager.session.currentShopItems.remove(item)
            }
        }

        // Remove bought agendas from shop
        compositeLiveDataDisposable += sessionManager.session.ongoingAgenda.observeForeverWithDisposable {
            it.forEach { agenda ->
                if(agendas.value.contains(agenda))
                    sessionManager.session.currentAgendaShopItems.remove(agenda)
            }
        }

        // Remove bought agendas from shop
        compositeLiveDataDisposable += sessionManager.session.missions.observeForeverWithDisposable { missions ->
            val agendasToRemove = mutableListOf<Resource.Agenda>()
            agendas.value.forEach { agenda ->
                agenda.sideMission ?: return@forEach
                if(missions.contains(agenda.sideMission)) {
                    agendasToRemove.add(agenda)
                }
            }
            agendasToRemove.forEach {
                sessionManager.session.currentAgendaShopItems.remove(it)
            }
        }
    }
}
