package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import com.makokvak.iacompanion.model.Resource
import kotlinx.android.synthetic.main.fragment_generic_selectable_list.*
import org.koin.android.ext.android.inject

class SelectCampaignFragment : SelectableResourceListFragment() {

    private val localTarget: Navigation.SelectCampaignTarget by inject()

    override var resourceType: ResourceType? = ResourceType.Campaign
    override fun navigateToNext() {
        viewModel.onCampaignSelected()
        localTarget.toNext.go(fragmentContainer = baseActivity)
    }
    override val maxSelectionCount: Int = 1
    override val minSelectionCount: Int = 1
}

class SelectAgendaSetFragment : SelectableResourceListFragment() {

    private val localTarget: Navigation.SelectAgendaSetTarget by inject()

    override var resourceType: ResourceType? = ResourceType.AgendaSets
    override fun navigateToNext() {
        localTarget.toNext.go(fragmentContainer = baseActivity)
    }
    override val maxSelectionCount: Int = 6
    override val minSelectionCount: Int = 6
}

class SelectImperialClassDeckFragment : SelectableResourceListFragment() {

    private val localTarget: Navigation.SelectImperialClassDeckTarget by inject()

    override var resourceType: ResourceType? = ResourceType.ImperialClassDecks
    override fun navigateToNext() {
        localTarget.toNext.go(fragmentContainer = baseActivity)
    }
    override val maxSelectionCount: Int = 1
    override val minSelectionCount: Int = 1
}

class SelectHeroesFragment : SelectableResourceListFragment() {

    private val localTarget: Navigation.SelectHeroesTarget by inject()

    override var resourceType: ResourceType? = ResourceType.Heroes

    override fun navigateToNext() {
        val mission = viewModel.getCurrentMission()

        val fm = baseActivity!!.containerFragmentManager
        for (i in 0 until fm.backStackEntryCount) {
            fm.popBackStack()
        }

        if(mission != null)
            localTarget.toMissionStage.go(fragmentContainer = baseActivity, arg = mission)
        else localTarget.toCampaignStage.go(fragmentContainer = baseActivity)
    }
    override val maxSelectionCount: Int = 4
    override val minSelectionCount: Int = 2
}

class SelectSideMissionsFragment : SelectableResourceListFragment() {

    private val localTarget: Navigation.SelectSideMissionsTarget by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.filterData {
            val mission = it as Resource.Mission.Side
            return@filterData !mission.isRandom && mission.forCharacter == null
        }
    }

    override var resourceType: ResourceType? = ResourceType.SideMissions

    override fun navigateToNext() {
        viewModel.buildSideMissionDeck()

        localTarget.toNext.go(fragmentContainer = baseActivity)
    }
    override val maxSelectionCount: Int = 4
    override val minSelectionCount: Int = 4
}

abstract class SelectableResourceListFragment : BaseResourceListFragment<ResourceListViewModel>(
    R.layout.fragment_generic_selectable_list,
    ResourceListViewModel::class
) {

    abstract fun navigateToNext()

    open val maxSelectionCount: Int = 1
    open val minSelectionCount: Int = 1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.setMaxSelection(maxSelectionCount)

        viewModel.selectedData.observe(this) {
            adapter.notifyDataSetChanged()
        }

        button_confirm.setOnClickListener {
            val resourceType = resourceType ?: return@setOnClickListener
            if(viewModel.selectedData.value.size < minSelectionCount) return@setOnClickListener

            viewModel.confirmSelection(resourceType)
            navigateToNext()
        }

        button_clear.setOnClickListener {
            viewModel.resetSelection()
        }

        button_selectAll.setOnClickListener {
            viewModel.selectAll()
        }
    }

    override fun onItemClicked(
        itemView: ResourceItemView,
        data: Resource,
        position: Int
    ): ShouldNavigateToResourcesList {

        viewModel.toggleItemSelection(position)

        return false
    }

    override fun onBindItemView(itemView: ResourceItemView, data: Resource, position: Int) {
        super.onBindItemView(itemView, data, position)

        val isSelected = viewModel.selectedData.value.contains(data)
        itemView.setHighlighted(isSelected)

        itemView.setOnLongClickListener {
            openDetail(data)
            return@setOnLongClickListener true
        }
    }

    open fun openDetail(data: Resource) {
        target.toDetail.go(fragmentContainer = baseActivity, arg = data)
    }
}
