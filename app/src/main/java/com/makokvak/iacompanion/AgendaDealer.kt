package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import kotlin.math.min

class AgendaDealer(private val sessionManager: SessionManager) : BaseViewModel() {

    private val availableAgendas get() =
        sessionManager.session.chosenAgendaDecks.value
            .map { listOf(it.agenda0, it.agenda1, it.agenda2) }
            .flatten()
            .minus(sessionManager.session.unavailableAgendas.value)

    fun dealAgendas(): List<Resource.Agenda> {

        val agendas = availableAgendas.toMutableList()
        val result = mutableListOf<Resource.Agenda>()

        for(i in 0 until min(availableAgendas.count(), 4 + sessionManager.session.extraAgendaCardsToDraw)) {
            val item = agendas.random()
            result.add(item)
            agendas.remove(item)
        }

        sessionManager.session.extraAgendaCardsToDraw = 0

        return result
    }
}
