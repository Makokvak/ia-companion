package com.makokvak.iacompanion

import android.content.res.Resources
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.extension.setHeightBoundByAspect
import com.makokvak.iacompanion.util.util.GridItemSpacingDecoration
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.item_component.view.*
import kotlinx.android.synthetic.main.item_resource.view.*
import org.koin.android.ext.android.inject

typealias ShouldNavigateToResourcesList = Boolean

open class ComponentListFragment(@LayoutRes layoutRes: Int = R.layout.fragment_generic_list) : ListBaseFragment<ComponentListViewModel, Component, ResourceItemView>(
    layoutRes,
    ComponentListViewModel::class
) {

    override val itemFactory: RecyclerViewItemFactory<ResourceItemView>
        get() = ResourceItemView.Factory()

    private val target: Navigation.ComponentListTarget by inject()

    protected open val hasAllComponentsItem = true

    override val data: NonNullLiveData<out List<Component>>
        get() = viewModel.data

    override val recyclerViewResourceId: Int
        get() = R.id.recyclerView

    override val layoutManager: RecyclerView.LayoutManager
        get() = GridLayoutManager(context, 2)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.addItemDecoration(GridItemSpacingDecoration(16))

        if(hasAllComponentsItem)
            viewModel.addAllComponentsItem()
    }

    override fun onCreateItemView(itemView: View) {
        super.onCreateItemView(itemView)

        val calculatedWidth = (Resources.getSystem().displayMetrics.widthPixels / 2f) - dpToPx(24)
        itemView.setHeightBoundByAspect(calculatedWidth.toInt(), ResourceType.Campaign.heightAspectRatioMultiplier)
    }

    override fun onBindItemView(itemView: ResourceItemView, data: Component, @Suppress("UNUSED_PARAMETER") position: Int) {
        itemView.imageView.setImageResource(data.displayableParams.drawable)
        itemView.textView_resource_name.text = getString(data.displayableParams.name)

        itemView.setOnClickListener {
            if(onItemClicked(itemView, data, position))
                target.toResourceList.go(fragmentContainer = baseActivity, arg = data)
        }
    }

    protected open fun onItemClicked(itemView: ResourceItemView, data: Component, position: Int): ShouldNavigateToResourcesList {
        return true
    }
}
