package com.makokvak.iacompanion

import android.app.Application
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module
import timber.log.Timber


//todo
// add all to search
// correct shop item count
// minor campaigns
// todo some agenda after played can be discarded or shuffled back into deck

@Suppress("unused")
class App : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()

        initializeKoin()

        Timber.plant(Timber.DebugTree())

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    private fun initializeKoin() {
        startKoin {
            androidContext(this@App)
            androidLogger(Level.DEBUG)
            module { }
        }

        AppKoinModule.start()
    }
}
