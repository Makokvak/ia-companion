package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Resource

class MissionStageViewModel(private val sessionManager: SessionManager) : GameStageViewModel(sessionManager) {

    fun setActiveMission(mission: Resource.Mission) {
        sessionManager.session.activeMission.value = mission
    }
}
