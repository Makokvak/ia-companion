package com.makokvak.iacompanion

import com.makokvak.iacompanion.util.MutableNonNullLiveData
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class MainMenuViewModel : BaseViewModel() {

    val selectedItemIndex = MutableNonNullLiveData(0)
}
