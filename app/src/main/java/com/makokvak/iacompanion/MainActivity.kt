package com.makokvak.iacompanion

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.makokvak.iacompanion.util.BaseActivity
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity<MainActivityViewModel>(
    R.layout.activity_main,
    MainActivityViewModel::class
) {

    private val target: Navigation.MainActivityTarget by inject()

    override val fragmentContainerId: Int
        get() = R.id.activityFragmentContainer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        target.toMainMenu.go(fragmentContainer = this)
    }


}
