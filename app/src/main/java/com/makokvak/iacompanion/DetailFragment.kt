package com.makokvak.iacompanion

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.text.Html
import android.view.Display
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.extension.setHeightBoundByAspect
import com.makokvak.iacompanion.util.extension.setVisible
import com.makokvak.iacompanion.util.extension.setWidthBoundByAspect
import com.makokvak.iacompanion.util.util.GridItemSpacingDecoration
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_detail.button
import kotlinx.android.synthetic.main.fragment_detail.recyclerView
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.item_detail_info.view.*
import kotlinx.android.synthetic.main.item_resource.view.*
import kotlinx.android.synthetic.main.layout_hero_attribute.view.*
import org.koin.android.ext.android.inject

class TriggerableMissionDetailFragment : DetailFragment() {

    private val missionTarget: Navigation.TriggerableMissionDetailTarget by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mission = target.retrieveArgument(fragment = this) as? Resource.Mission ?: throw AssertionError()
        button.visibility = if(viewModel.isBeginMissionVisible()) View.VISIBLE else View.GONE
        button.setOnClickListener {
            viewModel.removeMission()
            val fm = baseActivity!!.containerFragmentManager
            for (i in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
            missionTarget.toMission.go(fragmentContainer = baseActivity, arg = mission)
        }
        button.text = getString(R.string.begin_mission)

        val isAgenda = mission is Resource.Mission.Side && mission.isFromAgenda
        button2.text = getString(R.string.claim_imperial_reward)
        button2.setVisible(isAgenda)

        button2.setOnClickListener {
            viewModel.claimImperialReward(mission)
            baseActivity?.onBackPressed()
        }
    }
}

class ShopItemDetailFragment : DetailFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val item = target.retrieveArgument(fragment = this) as? Resource.ResourceWithReferences.Item ?: throw AssertionError()
        button.visibility = View.VISIBLE
        button.setOnClickListener {
            viewModel.buyItem(item)
            baseActivity?.onBackPressed()
        }
        button.text = getString(R.string.buy)
    }
}

class BuyAgendaDetailFragment : DetailFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val item = target.retrieveArgument(fragment = this) as? Resource.Agenda ?: throw AssertionError()
        button.visibility = View.VISIBLE
        button.setOnClickListener {
            if(viewModel.buyAgenda(item))
                when {
                    item.forcedMission != null -> {
                        val fm = baseActivity!!.containerFragmentManager
                        for (i in 0 until fm.backStackEntryCount) {
                            fm.popBackStack()
                        }

                        target.toMission.go(fragmentContainer = baseActivity, item.forcedMission)
                    }
                    else -> {
                        baseActivity?.onBackPressed()
                    }
                }
        }
        button.text = getString(R.string.buy)
    }
}

class BuyHeroSkillDetailFragment : DetailFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val item = target.retrieveArgument(fragment = this) as? Resource ?: throw AssertionError()
        button.visibility = View.VISIBLE
        button.setOnClickListener {
            viewModel.buySkill(item)
            baseActivity?.onBackPressed()
        }
        button.text = getString(R.string.buy)
    }
}

class OwnedItemDetailFragment : DetailFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val item = target.retrieveArgument(fragment = this) as? Resource.ResourceWithReferences.Item ?: throw AssertionError()
        button.visibility = View.VISIBLE
        button.setOnClickListener {
            viewModel.sellItem(item)
            baseActivity?.onBackPressed()
        }
        button.text = getString(R.string.sell)
    }
}

open class DetailFragment : ListBaseFragment<DetailViewModel, Resource, ResourceItemView>(
    R.layout.fragment_detail,
    DetailViewModel::class
) {

    override val itemFactory: RecyclerViewItemFactory<ResourceItemView>
        get() = object : RecyclerViewItemFactory<ResourceItemView> {
            override fun makeView(context: Context, parent: ViewGroup): ResourceItemView {
                return ResourceItemView(context)
            }
        }

    protected val target: Navigation.DetailTarget by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val resource = target.retrieveArgument(fragment = this)
        viewModel.setResource(resource)

        recyclerView.addItemDecoration(GridItemSpacingDecoration(16))

        viewModel.imageResourceId.observe(viewLifecycleOwner) {
            imageView.setImageResource(it)

            if(resource is Resource.ResourceWithReferences.Hero)
                imageView.layoutParams = imageView.layoutParams.apply { height = (Resources.getSystem().displayMetrics.widthPixels * 0.802f).toInt() }
            else
                imageView.layoutParams = imageView.layoutParams.apply { height = Resources.getSystem().displayMetrics.heightPixels - dpToPx(128) }
        }
        viewModel.title.observe(viewLifecycleOwner, textView_title::setText)
        viewModel.description.observe(viewLifecycleOwner) {
            textView_description.text = Html.fromHtml(it)
            textView_description.visibility = if(it.isBlank()) View.GONE else View.VISIBLE
        }
        viewModel.infoItems.observe(viewLifecycleOwner, ::addInfoLayouts)

        viewModel.additionalResources.observe(viewLifecycleOwner) {
            recyclerView.visibility = if(it.isEmpty()) View.GONE else View.VISIBLE
        }

        if(resource is Resource.ResourceWithReferences.Hero) {
            addHeroAttributeView(getString(R.string.damage_dealer), resource.damageDealer)
            addHeroAttributeView(getString(R.string.durable), resource.durable)
            addHeroAttributeView(getString(R.string.support), resource.support)
            addHeroAttributeView(getString(R.string.range), resource.range)
            addHeroAttributeView(getString(R.string.mobility), resource.mobility)
            addHeroAttributeView(getString(R.string.crowd_control), resource.crowdControl)
            addHeroAttributeView(getString(R.string.difficulty), resource.difficulty)
        }
    }

    private val drawableOwnedAttribute by lazy { ContextCompat.getDrawable(context!!, R.drawable.circle_full) }
    private val drawableAttribute by lazy { ContextCompat.getDrawable(context!!, R.drawable.circle) }

    private fun addHeroAttributeView(name: String, value: Int) {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_hero_attribute, container_details, false)
        container_details.addView(view)
        view.textView.text = name


        view.bar0.background = if(value > 0) drawableOwnedAttribute else drawableAttribute
        view.bar1.background = if(value > 1) drawableOwnedAttribute else drawableAttribute
        view.bar2.background = if(value > 2) drawableOwnedAttribute else drawableAttribute
    }

    private fun addInfoLayouts(infoMap: Map<String, String>) {

        descriptionDivider.visibility = if(infoMap.isEmpty()) View.GONE else View.VISIBLE

        layout_info.removeAllViews()

        val inflater = LayoutInflater.from(context)
        infoMap.forEach {
            val view = inflater.inflate(R.layout.item_detail_info, layout_info, false)
            layout_info.addView(view)
            view.textView_key.text = it.key
            view.textView_value.text = it.value
        }
    }

    override val layoutManager: RecyclerView.LayoutManager
        get() = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

    override val data: NonNullLiveData<out List<Resource>>
        get() = viewModel.additionalResources

    override val recyclerViewResourceId: Int
        get() = R.id.recyclerView

    override fun onBindItemView(itemView: ResourceItemView, data: Resource, @Suppress("UNUSED_PARAMETER") position: Int) {
        itemView.imageView.setImageResource(data.displayableParams.drawable)
        itemView.textView.text = getString(data.displayableParams.name)
        itemView.textView.visibility = View.VISIBLE
        itemView.imageView.setOnClickListener {
            target.toDetail.go(fragmentContainer = baseActivity, arg = data)
        }
    }

    override fun onCreateItemView(itemView: View) {
        super.onCreateItemView(itemView)

        val height = recyclerView.layoutParams.height - dpToPx(32)
        itemView.layoutParams.height = height
        itemView.setWidthBoundByAspect(height, viewModel.heightMultiplier)
    }
}
