package com.makokvak.iacompanion

import android.content.Context

import kotlinx.serialization.*
import kotlinx.serialization.json.*

class SessionSerializer(private val context: Context) {

    fun serialize(session: Session) {
        val data = SessionData.fromSession(session)

        val filename = "${data.sessionId}.iac"
        val fileContents = Json { allowStructuredMapKeys = true }.encodeToString(data)

        context.openFileOutput(filename, Context.MODE_PRIVATE).use {
            it.write(fileContents.toByteArray())
        }
    }

    fun deserialize(sessionId: Long): Session {

        val filename = "$sessionId.iac"

        val json = context.openFileInput(filename).bufferedReader().readText()
        val sessionData = Json { allowStructuredMapKeys = true }.decodeFromString<SessionData>(json)
        return sessionData.toSession()
    }

    fun getAllSessionsData(): List<SessionData> {
        val files = context.fileList().filter { it.endsWith(".iac") }

        return files.map {
            val json = context.openFileInput(it).bufferedReader().readText()
            Json { allowStructuredMapKeys = true }.decodeFromString(json)
        }
    }
}
