package com.makokvak.iacompanion

import android.content.Context
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.*
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class DetailViewModel(private val context: Context, private val sessionManager: SessionManager) : BaseViewModel() {

    @Suppress("PropertyName")
    private val _additionalResources = MutableNonNullLiveData(
        emptyList<Resource>()
    )
    val additionalResources: NonNullLiveData<List<Resource>> = _additionalResources

    @Suppress("PropertyName")
    private val _infoItems = MutableNonNullLiveData(
        emptyMap<String, String>()
    )
    val infoItems: NonNullLiveData<Map<String, String>> = _infoItems

    private val _title = MutableNonNullLiveData("")
    val title: NonNullLiveData<String> = _title

    private val _description = MutableNonNullLiveData("")
    val description: NonNullLiveData<String> = _description

    private val _imageResourceId = MutableNonNullLiveData(0)
    val imageResourceId: NonNullLiveData<Int> = _imageResourceId

    private lateinit var _resource: Resource

    fun setResource(resource: Resource) {

        this._resource = resource

        _title.value = context.getString(resource.displayableParams.name)
        _imageResourceId.value = resource.displayableParams.drawable

        setDescription(resource)
        setAdditionalResources(resource)
        setInfoItems(resource)
    }

    private fun setDescription(resource: Resource) {
        when(resource) {
            is Resource.ImperialClassDeck -> _description.value = context.getString(resource.description)
            else -> _description.value = ""
        }
    }

    var heightMultiplier = 1f

    private fun setAdditionalResources(resource: Resource) {
        when(resource) {
            is Resource.AgendaSet -> {
                _additionalResources.value = listOf(resource.agenda0, resource.agenda1, resource.agenda2)
                heightMultiplier = ResourceType.Agendas.heightAspectRatioMultiplier
            }
            is Resource.Mission.Side -> {
                _additionalResources.value = listOf(resource.possibleOutcomes.map {
                    it.reward.earnedAllies + it.reward.rewardsForRebels +
                    listOf(it.reward.earnedVillain, it.reward.rewardForImperialPlayer) }.flatten()).flatten().filterNotNull()

                heightMultiplier = ResourceType.Rewards.heightAspectRatioMultiplier
            }
            is Resource.ImperialClassDeck -> {
                _additionalResources.value = resource.cards
                heightMultiplier = ResourceType.Skills.heightAspectRatioMultiplier
            }
            is Resource.ResourceWithReferences.Hero -> {
                _additionalResources.value = resource.skillDeck
                heightMultiplier = ResourceType.Skills.heightAspectRatioMultiplier
            }
            else -> _additionalResources.value = emptyList()
        }

        if(resource is Resource.ResourceWithReferences)
            _additionalResources.addRange(resource.references)
    }

    private fun setInfoItems(resource: Resource) {
        when(resource) {
            is Resource.ResourceWithReferences.Item -> _infoItems.value = mapOf(
                context.getString(R.string.tier) to context.getString(resource.tier.title),
                context.getString(R.string.cost) to resource.cost.toString()
            )
            is Resource.ImperialClassDeckCard -> _infoItems.value = mapOf(
                context.getString(R.string.xp_cost) to resource.xpCost.toString()
            )
            is Resource.ResourceWithReferences.Supply -> _infoItems.value = mapOf(
                context.getString(R.string.count) to resource.countInDeck.toString()
            )
            else -> _infoItems.value = emptyMap()
        }
    }

    fun buyItem(item: Resource.ResourceWithReferences.Item) {
        sessionManager.buyItem(item)
    }

    fun buyAgenda(item: Resource.Agenda): Boolean {
        return sessionManager.buyAgenda(item)
    }

    fun buySkill(item: Resource) {
        sessionManager.buySkill(item)
    }

    fun sellItem(item: Resource.ResourceWithReferences.Item) {
        sessionManager.sellItem(item)
    }

    fun removeMission() {
        val mission = _resource as Resource.Mission
        sessionManager.session.missions.remove(mission)
    }

    fun addMission(mission: Resource.Mission.Side) {
        sessionManager.session.missions.add(mission)
    }

    fun claimImperialReward(mission: Resource.Mission) {
        val villain = mission.possibleOutcomes.firstOrNull { it is MissionResult.ImperialVictory }?.reward?.earnedVillain
        val reward = mission.possibleOutcomes.firstOrNull { it is MissionResult.ImperialVictory }?.reward?.rewardForImperialPlayer

        if(reward != null)
            sessionManager.session.earnedImperialRewards.add(reward)
        if(villain != null)
            sessionManager.session.ownedVillains.add(villain)

        removeMission()
    }

    fun isBeginMissionVisible(): Boolean {
        val mission = _resource as Resource.Mission
        val isStoryMission = mission is Resource.Mission.Story
        val nextMissionShouldBeStory = !sessionManager.session.campaign.value!!.details[sessionManager.session.missionsCompletedCount.value + 1].isSlotForSideMission

        return (isStoryMission && nextMissionShouldBeStory) || (!isStoryMission && !nextMissionShouldBeStory)
    }
}
