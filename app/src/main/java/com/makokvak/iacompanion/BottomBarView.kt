package com.makokvak.iacompanion

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.makokvak.iacompanion.util.BaseActivity
import kotlinx.android.synthetic.main.view_bottom_bar.view.*
import org.koin.android.ext.android.inject

class BottomBarView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {

    class ButtonViews(val textView: TextView, val imageView: ImageView)

    val buttons: List<ButtonViews>

    init {
        inflate(getContext(), R.layout.view_bottom_bar, this)
        buttons = listOf(
            ButtonViews(textView_browse, imageView_browse),
            ButtonViews(textView_session, imageView_session),
            ButtonViews(textView_search, imageView_search))
    }

    fun highlightButton(index: Int) {
        require(index < buttons.size)

        buttons.forEach {
            removeButtonHighlight(it)
        }

        val buttonViews = buttons[index]
        val color = ResourcesCompat.getColor(context.resources, R.color.secondaryDarkColor, null)
        buttonViews.textView.setTextColor(color)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            buttonViews.imageView.imageTintList = ColorStateList.valueOf(color)
        }
        else
            buttonViews.imageView.setBackgroundColor(color)
    }

    private fun removeButtonHighlight(buttonViews: ButtonViews) {
        val color = ResourcesCompat.getColor(context.resources, R.color.secondaryTextColor, null)
        buttonViews.textView.setTextColor(color)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            buttonViews.imageView.imageTintList = ColorStateList.valueOf(color)
        }
        else
            buttonViews.imageView.setBackgroundColor(color)
    }
}
