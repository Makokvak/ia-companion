package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DividerItemDecoration
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.util.LayoutResourceRecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.fragment_session_list.view.*
import kotlinx.android.synthetic.main.item_component.view.*
import kotlinx.android.synthetic.main.item_component.view.textView
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*

class SessionListFragment(@LayoutRes layoutRes: Int = R.layout.fragment_session_list) : ListBaseFragment<SessionListViewModel, SessionData, View>(
    layoutRes,
    SessionListViewModel::class
) {

    private val format = SimpleDateFormat.getDateTimeInstance()

    private val target: Navigation.SessionsTarget by inject()

    override val itemFactory: RecyclerViewItemFactory<View>
        get() = LayoutResourceRecyclerViewItemFactory(R.layout.item_component)

    override val data: NonNullLiveData<out List<SessionData>>
        get() = viewModel.data

    override val recyclerViewResourceId: Int
        get() = R.id.recyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        button.setOnClickListener {
            viewModel.newSession()
            target.toNewSession.go(fragmentContainer = baseActivity)
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.loadData()
    }

    override fun onBindItemView(itemView: View, data: SessionData, @Suppress("UNUSED_PARAMETER") position: Int) {
        itemView.imageView.setImageResource(data.campaign.displayableParams.drawable)
        itemView.textView.text = format.format(Date(data.sessionId))

        itemView.setOnClickListener {
            viewModel.setSessionData(data)
            if(viewModel.isMissionActiveInSession(data))
                target.toSessionMission.go(fragmentContainer = baseActivity, arg = viewModel.getActiveMission())
            else
                target.toSessionCampaign.go(fragmentContainer = baseActivity)
        }
    }
}
