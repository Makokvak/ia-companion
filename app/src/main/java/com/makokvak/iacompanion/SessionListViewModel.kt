package com.makokvak.iacompanion

import com.makokvak.iacompanion.util.MutableNonNullLiveData
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel
import java.lang.AssertionError

class SessionListViewModel(
    private val sessionSerializer: SessionSerializer,
    val sessionManager: SessionManager
) : BaseViewModel() {

    private val _data = MutableNonNullLiveData<List<SessionData>>(emptyList())
    val data: NonNullLiveData<List<SessionData>> = _data

    fun loadData() {
        _data.value = sessionSerializer.getAllSessionsData()
    }

    fun setSessionData(sessionData: SessionData) {
        sessionManager.setActiveSession(sessionData.toSession())
    }

    fun newSession() {
        val session = Session(System.currentTimeMillis())
        sessionManager.setActiveSession(session)
    }

    fun isMissionActiveInSession(sessionData: SessionData): Boolean {
        return sessionData.activeMission != null
    }

    fun getActiveMission()
        = sessionManager.session.activeMission.value ?: throw AssertionError()
}
