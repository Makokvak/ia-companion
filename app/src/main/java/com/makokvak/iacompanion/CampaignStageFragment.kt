package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.GridDividerDecoration
import com.makokvak.iacompanion.util.extension.setWidthBoundByAspect
import com.makokvak.iacompanion.util.util.GridItemSpacingDecoration
import com.makokvak.iacompanion.util.util.LayoutResourceRecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.LifecycleAwareRecyclerViewAdapter
import com.makokvak.iacompanion.util.util.dpToPx
import kotlinx.android.synthetic.main.fragment_campaign_stage.*
import kotlinx.android.synthetic.main.fragment_generic_list.*
import kotlinx.android.synthetic.main.item_resource.view.*
import org.koin.android.ext.android.inject

class CampaignStageFragment : GameStageFragment<CampaignStageViewModel>(
    R.layout.fragment_campaign_stage,
    CampaignStageViewModel::class
) {

    private val target: Navigation.CampaignStageTarget by inject()

    private val missionsAdapter by lazy {
        LifecycleAwareRecyclerViewAdapter(
            viewLifecycleOwner,
            viewModel.missions,
            ResourceItemView.Factory(),
            { itemView, data, _ ->
                itemView.imageView.setImageResource(data.displayableParams.drawable)
                itemView.textView_resource_name.visibility = View.VISIBLE
                itemView.textView_resource_name.text = getString(data.displayableParams.name)

                itemView.setOnClickListener {
                    target.toMissionDetail.go(fragmentContainer = baseActivity, arg = data)
                }
            },
            { view ->
                view.layoutParams = view.layoutParams.apply { height = (recyclerView_missions.height - dpToPx(32)) }
                view.setWidthBoundByAspect(view.layoutParams.height, ResourceType.SideMissions.heightAspectRatioMultiplier)
            }
        )
    }

    private val shopAdapter by lazy {
        LifecycleAwareRecyclerViewAdapter(
            viewLifecycleOwner,
            viewModel.shop,
            ResourceItemView.Factory(),
            { itemView, data, _ ->
                itemView.imageView.setImageResource(data.displayableParams.drawable)
                itemView.textView_resource_name.visibility = View.VISIBLE
                itemView.textView_resource_name.text = getString(data.displayableParams.name)

                itemView.setOnClickListener {
                    target.toShopItemDetail.go(fragmentContainer = baseActivity, arg = data)
                }
            },
            { view ->
                view.layoutParams = view.layoutParams.apply { height = ((recyclerView_shop.height - dpToPx(48)) * 0.5f ).toInt() }
                view.setWidthBoundByAspect(view.layoutParams.height, ResourceType.Items.heightAspectRatioMultiplier)
            }
        )
    }

    private val agendaAdapter by lazy {
        LifecycleAwareRecyclerViewAdapter(
            viewLifecycleOwner,
            viewModel.agendas,
            ResourceItemView.Factory(),
            { itemView, data, _ ->
                itemView.imageView.setImageResource(if(data.isSecret) R.mipmap.lock else data.displayableParams.drawable)
                itemView.textView_resource_name.visibility = if(data.isSecret) View.GONE else View.VISIBLE
                itemView.textView_resource_name.text = getString(data.displayableParams.name)

                itemView.setOnClickListener {
                    target.toBuyAgendaDetail.go(fragmentContainer = baseActivity, arg = data)
                }
            },
            { view ->
                view.layoutParams = view.layoutParams.apply { height = (recyclerView_agenda.height - dpToPx(32)) }
                view.setWidthBoundByAspect(view.layoutParams.height, ResourceType.Agendas.heightAspectRatioMultiplier)
            }
        )
    }

    private val shopLayoutManager: RecyclerView.LayoutManager
        get() = GridLayoutManager(context, 2, GridLayoutManager.HORIZONTAL, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context = context ?: return

        recyclerView_missions.adapter = missionsAdapter
        recyclerView_missions.layoutManager = horizontalLayoutManager
        recyclerView_missions.addItemDecoration(GridItemSpacingDecoration(dp = 16, isVertical = false))

        recyclerView_agenda.adapter = agendaAdapter
        recyclerView_agenda.layoutManager = horizontalLayoutManager
        recyclerView_agenda.addItemDecoration(GridItemSpacingDecoration(dp = 16, isVertical = false))

        recyclerView_shop.adapter = shopAdapter
        recyclerView_shop.layoutManager = shopLayoutManager
        recyclerView_shop.addItemDecoration(GridItemSpacingDecoration(dp = 16, isVertical = false))

    }
}
