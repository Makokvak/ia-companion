package com.makokvak.iacompanion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.util.BaseFragment
import com.makokvak.iacompanion.util.FragmentContainer
import kotlinx.android.synthetic.main.fragment_main_menu.*
import kotlinx.android.synthetic.main.view_bottom_bar.*
import org.koin.android.ext.android.inject

class MainMenuFragment : BaseFragment<MainMenuViewModel>(
    R.layout.fragment_main_menu,
    MainMenuViewModel::class
), FragmentContainer {

    private val target: Navigation.MainMenuTarget by inject()

    private fun goToAction(index: Int) {
        when(index) {
            0 -> target.toComponentList.go(fragmentContainer = this)
            1 -> target.toSessions.go(fragmentContainer = this)
            2 -> target.toSearch.go(fragmentContainer = this, arg = Navigation.ResourceListParams(
                ResourceType.Deployables,
                Component.AllComponents
            ))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        if(childFragmentManager.backStackEntryCount == 0) {
            goToAction(0)
            view?.findViewById<BottomBarView>(R.id.bottomBar)?.highlightButton(0)
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.selectedItemIndex.observe(viewLifecycleOwner) {
            bottomBar.highlightButton(it)
        }

        button_browse.setOnClickListener {
            goToAction(0)
            viewModel.selectedItemIndex.value = 0
        }
        button_session.setOnClickListener {
            goToAction(1)
            viewModel.selectedItemIndex.value = 1

        }
        button_search.setOnClickListener {
            goToAction(2)
            viewModel.selectedItemIndex.value = 2
        }
    }

    override val fragmentContainerId: Int
        get() = R.id.fragmentContainer

    override val containerFragmentManager: FragmentManager
        get() = childFragmentManager
}
