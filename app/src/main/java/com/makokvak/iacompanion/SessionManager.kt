package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.Component
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.*
import java.io.Serializable

typealias WasItemBought = Boolean

@kotlinx.serialization.Serializable
data class SessionData(
    val sessionId: Long,
    val components: List<Component>,
    val earnedRewards: List<Resource.ResourceWithReferences.Reward>,
    val chosenAgendaDecks: List<Resource.AgendaSet>,
    val ongoingAgenda: List<Resource.Agenda>,
    val chosenHeroes: List<Resource.ResourceWithReferences.Hero>,
    val chosenImperialClassDeck: Resource.ImperialClassDeck,
    val unavailableSideMissions: List<Resource.Mission.Side>,
    val unavailableAgendas: List<Resource.Agenda>,
    val ownedItems: List<Resource.ResourceWithReferences.Item>,
    val currentShopItems: List<Resource.ResourceWithReferences.Item>,
    val currentAgendaShopItems: List<Resource.Agenda>,
    val ownedVillains: List<Resource.ResourceWithReferences.Deployable>,
    val ownedAllies: List<Resource.Ally>,
    val missions: List<Resource.Mission>,
    val activeMission: Resource.Mission?,
    val credits: Int,
    val influence: Int,
    val totalExperienceEarnedPerHero: Int,
    val totalExperienceEarnedForImperialPlayer: Int,
    val heroSkills: Map<Int, List<Resource.HeroSkill>>,
    val imperialSkills: List<Resource.ImperialClassDeckCard>,
    val missionsCompletedCount: Int,
    val campaign: Resource.Campaign,
    val earnedImperialRewards: List<Resource.ResourceWithReferences.Reward>,
    val sideMissionDeck: List<Resource.Mission.Side>
) : Serializable {

    fun toSession() =
        Session(sessionId).also {
            it.components.value = components
            it.earnedRewards.value = earnedRewards
            it.chosenAgendaDecks.value = chosenAgendaDecks
            it.ongoingAgenda.value = ongoingAgenda
            it.chosenImperialClassDeck.value = chosenImperialClassDeck
            it.unavailableSideMissions.value = unavailableSideMissions
            it.unavailableAgendas.value = unavailableAgendas
            it.chosenHeroes.value = chosenHeroes
            it.ownedItems.value = ownedItems
            it.currentShopItems.value = currentShopItems
            it.currentAgendaShopItems.value = currentAgendaShopItems
            it.ownedVillains.value = ownedVillains
            it.ownedAllies.value = ownedAllies
            it.missions.value = missions
            it.activeMission.value = activeMission
            it.credits.value = credits
            it.influence.value = influence
            it.totalExperienceEarnedPerHero.value = totalExperienceEarnedPerHero
            it.totalExperienceEarnedForImperialPlayer.value = totalExperienceEarnedForImperialPlayer
            it.heroSkills.value = heroSkills
            it.imperialSkills.value = imperialSkills
            it.missionsCompletedCount.value = missionsCompletedCount
            it.campaign.value = campaign
            it.earnedImperialRewards.value = earnedImperialRewards
            it.sideMissionDeck.value = sideMissionDeck
        }

    companion object {
        fun fromSession(session: Session) = SessionData(
            session.sessionId,
            session.components.value,
            session.earnedRewards.value,
            session.chosenAgendaDecks.value,
            session.ongoingAgenda.value,
            session.chosenHeroes.value,
            session.chosenImperialClassDeck.value ?: throw AssertionError(),
            session.unavailableSideMissions.value,
            session.unavailableAgendas.value,
            session.ownedItems.value,
            session.currentShopItems.value,
            session.currentAgendaShopItems.value,
            session.ownedVillains.value,
            session.ownedAllies.value,
            session.missions.value,
            session.activeMission.value,
            session.credits.value,
            session.influence.value,
            session.totalExperienceEarnedPerHero.value,
            session.totalExperienceEarnedForImperialPlayer.value,
            session.heroSkills.value,
            session.imperialSkills.value,
            session.missionsCompletedCount.value,
            session.campaign.value ?: throw AssertionError(),
            session.earnedImperialRewards.value,
            session.sideMissionDeck.value
        )
    }
}

class Session(val sessionId: Long) {
    val components = MutableNonNullLiveData(listOf<Component>())
    val earnedRewards = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Reward>())
    val chosenAgendaDecks = MutableNonNullLiveData(listOf<Resource.AgendaSet>())
    val ongoingAgenda = MutableNonNullLiveData(listOf<Resource.Agenda>())
    val chosenHeroes = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Hero>())
    val chosenImperialClassDeck = MutableNullableLiveData<Resource.ImperialClassDeck>()
    val unavailableSideMissions = MutableNonNullLiveData(listOf<Resource.Mission.Side>())
    val unavailableAgendas = MutableNonNullLiveData(listOf<Resource.Agenda>())
    val ownedItems = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Item>())
    val currentShopItems = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Item>())
    val currentAgendaShopItems = MutableNonNullLiveData(listOf<Resource.Agenda>())
    val ownedVillains = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Deployable>())
    val ownedAllies = MutableNonNullLiveData(listOf<Resource.Ally>())
    val missions = MutableNonNullLiveData(listOf<Resource.Mission>())
    val activeMission = MutableNullableLiveData<Resource.Mission>()
    val credits = MutableNonNullLiveData(0)
    val influence = MutableNonNullLiveData(0)
    val totalExperienceEarnedPerHero = MutableNonNullLiveData(0)
    val totalExperienceEarnedForImperialPlayer = MutableNonNullLiveData(0)
    val heroSkills = MutableNonNullLiveData(mapOf<Int, List<Resource.HeroSkill>>())
    val imperialSkills = MutableNonNullLiveData(listOf<Resource.ImperialClassDeckCard>())
    val missionsCompletedCount = MutableNonNullLiveData(0)
    val campaign = MutableNullableLiveData<Resource.Campaign>()
    val earnedImperialRewards = MutableNonNullLiveData(listOf<Resource.ResourceWithReferences.Reward>())
    val threat = missionsCompletedCount.map { campaign.value?.details?.get(it)?.threatLevel ?: 0 }
    val sideMissionDeck = MutableNonNullLiveData(listOf<Resource.Mission.Side>())
    var extraAgendaCardsToDraw = 0
}

class SessionManager(private val sessionSerializer: SessionSerializer) {

    private val disposable = CompositeLiveDataDisposable()

    lateinit var session: Session

    fun setActiveSession(localSession: Session) {

        this.session = localSession

        observeAndSave(session.components)
        observeAndSave(session.earnedRewards)
        observeAndSave(session.chosenAgendaDecks)
        observeAndSave(session.ongoingAgenda)
        observeAndSave(session.chosenHeroes)
        observeAndSave(session.chosenImperialClassDeck)
        observeAndSave(session.unavailableSideMissions)
        observeAndSave(session.ownedItems)
        observeAndSave(session.ownedVillains)
        observeAndSave(session.ownedAllies)
        observeAndSave(session.missions)
        observeAndSave(session.activeMission)
        observeAndSave(session.credits)
        observeAndSave(session.influence)
        observeAndSave(session.totalExperienceEarnedPerHero)
        observeAndSave(session.totalExperienceEarnedForImperialPlayer)
        observeAndSave(session.heroSkills)
        observeAndSave(session.imperialSkills)
        observeAndSave(session.missionsCompletedCount)
        observeAndSave(session.campaign)
        observeAndSave(session.earnedImperialRewards)
        observeAndSave(session.sideMissionDeck)
        observeAndSave(session.unavailableAgendas)
        observeAndSave(session.currentAgendaShopItems)
        observeAndSave(session.currentShopItems)
    }

    private fun observeAndSave(data: NonNullLiveData<*>) {
        disposable += data.observeForeverWithDisposable {
            if (session.chosenHeroes.value.isNotEmpty())
                sessionSerializer.serialize(session)
        }
    }

    fun observeAndSave(data: NullableLiveData<*>) {
        disposable += data.observeForeverWithDisposable {
            if (session.chosenHeroes.value.isNotEmpty())
                sessionSerializer.serialize(session)
        }
    }

    fun getHeroExperience(hero: Resource): Int {
        return if (hero is Resource.ResourceWithReferences.Hero) {
            val totalSkillsCost = getTotalHeroSkillsExpCost(hero)
            val totalExp = session.totalExperienceEarnedPerHero.value - totalSkillsCost
            check(totalExp >= 0)
            totalExp
        } else {
            val totalSkillsCost = session.imperialSkills.value.sumBy { it.xpCost }
            val totalExp = session.totalExperienceEarnedForImperialPlayer.value - totalSkillsCost
            check(totalExp >= 0)
            totalExp
        }
    }

    private fun getTotalHeroSkillsExpCost(hero: Resource.ResourceWithReferences.Hero): Int {

        if (!session.heroSkills.value.containsKey(hero.displayableParams.name))
            session.heroSkills.add(hero.displayableParams.name, listOf())

        return (session.heroSkills.value[hero.displayableParams.name] ?: throw AssertionError()).sumBy { it.expCost }
    }

    fun getNumberOfHeroes() =
        session.chosenHeroes.value.size

    fun finishActiveMission(selectedOutcomes: List<MissionResult>, credits: Int, influence: Int) {
        val mission = session.activeMission.value ?: throw IllegalStateException()

        session.activeMission.value = null

        session.missionsCompletedCount.value += 1
        session.credits.value += credits
        session.influence.value += influence

        val rewards =
            listOf(listOf(mission.staticReward), selectedOutcomes.map { it.reward }).flatten()

        rewards.forEach {
            session.credits.value += it.creditsPerHero * getNumberOfHeroes()
            session.totalExperienceEarnedPerHero.value += it.expForEachPlayer + it.expForRebels
            session.totalExperienceEarnedForImperialPlayer.value += it.expForEachPlayer + it.expForImperialPlayer
            session.influence.value += it.influence

            if (it.earnedAllies.isNotEmpty())
                session.ownedAllies.addRange(it.earnedAllies)
            if (it.earnedVillain != null)
                session.ownedVillains.add(it.earnedVillain)
            if (it.rewardsForRebels.isNotEmpty())
                session.earnedRewards.addRange(it.rewardsForRebels)
            if (it.rewardForImperialPlayer != null)
                session.earnedImperialRewards.add(it.rewardForImperialPlayer)
        }
    }

    fun cancelMission() {
        require(session.activeMission.value != null)
        session.activeMission.value = null
    }

    fun buyItem(item: Resource.ResourceWithReferences.Item): WasItemBought {
        if (session.credits.value < item.cost) return false

        session.ownedItems.add(item)
        session.credits.value -= item.cost
        return true
    }

    fun buyAgenda(agenda: Resource.Agenda): WasItemBought {
        if (session.influence.value < agenda.cost) return false

        if(agenda.sideMission == null && agenda.forcedMission == null)
            session.ongoingAgenda.add(agenda)
        if(agenda.sideMission != null)
            session.missions.add(agenda.sideMission)

        session.unavailableAgendas.add(agenda)
        session.influence.value -= agenda.cost
        return true
    }

    private fun getHeroBySkill(skill: Resource.HeroSkill) =
        session.chosenHeroes.value.first { it.skillDeck.contains(skill) }

    fun buySkill(skill: Resource): WasItemBought {
        if (skill is Resource.HeroSkill) {
            val hero = getHeroBySkill(skill)
            if (getHeroExperience(hero) < skill.expCost) return false

            session.heroSkills.insertValueToValueList(hero.displayableParams.name, skill)
            return true
        } else {
            val imperial = skill as Resource.ImperialClassDeckCard
            if (getHeroExperience(session.chosenImperialClassDeck.value!!) < skill.xpCost) return false

            session.imperialSkills.add(imperial)
            return true
        }
    }

    fun sellItem(item: Resource.ResourceWithReferences.Item) {

        session.ownedItems.remove(item)
        session.credits.value += (item.cost / 2f).toInt()
    }
}
