package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DividerItemDecoration
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.util.ListBaseFragment
import com.makokvak.iacompanion.util.NonNullLiveData
import com.makokvak.iacompanion.util.util.LayoutResourceRecyclerViewItemFactory
import com.makokvak.iacompanion.util.util.RecyclerViewItemFactory
import kotlinx.android.synthetic.main.dialog_mission_outcome.*
import kotlinx.android.synthetic.main.item_mission_outcome.view.*
import org.koin.android.ext.android.inject

class ChooseOutcomeFragment(@LayoutRes layoutRes: Int = R.layout.dialog_mission_outcome) : ListBaseFragment<ChooseOutcomeViewModel, MissionResult, View>(
    layoutRes,
    ChooseOutcomeViewModel::class
) {

    private val target: Navigation.ChooseOutcomeTarget by inject()

    override val itemFactory: RecyclerViewItemFactory<View>
        get() = LayoutResourceRecyclerViewItemFactory(R.layout.item_mission_outcome)

    override val data: NonNullLiveData<out List<MissionResult>>
        get() = viewModel.data

    override val recyclerViewResourceId: Int
        get() = R.id.recycler_view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler_view.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        button_cancel.setOnClickListener {
            baseActivity?.onBackPressed()
        }

        button_confirm.setOnClickListener {
            if(viewModel.checkedItems.isEmpty()) return@setOnClickListener
            if(!viewModel.confirmSelection(
                editText_credits.text.toString().toIntOrNull() ?: 0,
                editText_influence.text.toString().toIntOrNull() ?: 0
            )) return@setOnClickListener
            val fm = baseActivity!!.containerFragmentManager
            for (i in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
            target.toCampaignStage.go(fragmentContainer = baseActivity)
        }

        editText_credits.visibility = if(viewModel.shouldPromptCredits()) View.VISIBLE else View.GONE
        editText_influence.visibility = if(viewModel.shouldPromptInfluence()) View.VISIBLE else View.GONE
    }

    override fun onBindItemView(itemView: View, data: MissionResult, @Suppress("UNUSED_PARAMETER") position: Int) {
        itemView.checkbox.text = if(data.description == null) getString(data.name)
        else getString(data.name) + ", " + getString(data.description!!)

        itemView.checkbox.setOnCheckedChangeListener { _, checked ->
            viewModel.setChecked(data, checked)
        }
    }
}
