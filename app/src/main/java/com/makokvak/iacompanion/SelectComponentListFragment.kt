package com.makokvak.iacompanion

import android.os.Bundle
import android.view.View
import com.makokvak.iacompanion.model.Component
import kotlinx.android.synthetic.main.fragment_generic_selectable_list.*
import org.koin.android.ext.android.inject

open class SelectComponentListFragment : ComponentListFragment(R.layout.fragment_generic_selectable_list) {

    private val target: Navigation.SelectComponentListTarget by inject()

    override val hasAllComponentsItem: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.selectedData.observe(this) {
            adapter.notifyDataSetChanged()
        }

        button_confirm.setOnClickListener {
            if(viewModel.selectedData.value.isEmpty()) return@setOnClickListener
            viewModel.confirmSelection()
            target.toNext.go(fragmentContainer = baseActivity)
        }

        button_clear.setOnClickListener {
            viewModel.resetSelection()
        }

        button_selectAll.setOnClickListener {
            viewModel.selectAll()
        }
    }

    override fun onItemClicked(
        itemView: ResourceItemView,
        data: Component,
        position: Int
    ): ShouldNavigateToResourcesList {

        viewModel.toggleItemSelection(position)

        return false
    }

    override fun onBindItemView(itemView: ResourceItemView, data: Component, position: Int) {
        super.onBindItemView(itemView, data, position)
        itemView.setHighlighted(viewModel.isSelected(position))
    }
}
