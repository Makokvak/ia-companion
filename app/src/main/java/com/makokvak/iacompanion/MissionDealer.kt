package com.makokvak.iacompanion

import com.makokvak.iacompanion.model.ComponentResourceMap
import com.makokvak.iacompanion.model.MissionResult
import com.makokvak.iacompanion.model.Resource
import com.makokvak.iacompanion.util.add
import com.makokvak.iacompanion.util.addRange
import com.makokvak.iacompanion.util.remove
import com.makokvak.iacompanion.util.viewmodel.BaseViewModel

class MissionDealer(private val sessionManager: SessionManager, private val map: ComponentResourceMap) : BaseViewModel() {

    private val availableSideMissions get() =
        sessionManager.session.sideMissionDeck.value
            .minus(sessionManager.session.unavailableSideMissions.value)

    fun dealSideMissions() {

        val numberToTake = 2 - sessionManager.session.missions.value.filterIsInstance<Resource.Mission.Side>().size

        val chosenMissions = availableSideMissions.shuffled().take(numberToTake)
        sessionManager.session.unavailableSideMissions.addRange(chosenMissions)
        sessionManager.session.missions.addRange(chosenMissions)
    }

    fun dealStoryMissions(mission: Resource.Mission.Story, outcome: MissionResult.StoryMissionResult? = null) {

        if(outcome != null)
            sessionManager.session.missions.add(outcome.nextMission)

        if(mission.nextMissions.isNotEmpty()) {
            // Replace story missions
            val currentStoryMissions = sessionManager.session.missions.value.filterIsInstance<Resource.Mission.Story>()
            currentStoryMissions.forEach {
                sessionManager.session.missions.remove(it)
            }
            sessionManager.session.missions.addRange(mission.nextMissions)
        }
    }

    fun makeSideMissionDeck(chosenMissions: List<Resource.Mission.Side>) {
        require(chosenMissions.size == 4)

        check(sessionManager.session.sideMissionDeck.value.isEmpty())

        val result = chosenMissions.toMutableList()

        val randomMissions = map.getByResourceTypeAndComponents<Resource.Mission.Side>(sessionManager.session.components.value)
            .filter { it.isRandom }.toMutableList()

        check(randomMissions.size >= 4)

        repeat(4) {
            val mission = randomMissions.random()
            randomMissions.remove(mission)
            result.add(mission)
        }

        val characters = sessionManager.session.chosenHeroes.value
        val heroMissions = map.getByResourceTypeAndComponents<Resource.Mission.Side>(sessionManager.session.components.value)
            .filter { it.forCharacter != null && characters.contains(it.forCharacter) }

        check(characters.size == heroMissions.size)

        result.addAll(heroMissions)

        sessionManager.session.sideMissionDeck.value = result
    }
}
