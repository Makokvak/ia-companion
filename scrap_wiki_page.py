from bs4 import BeautifulSoup
from requests import get
import click
import re
import os

pattern = "/wiki/*"

@click.command()
@click.option('--url',
              help='''url''', prompt=True, required=True)
def main(url):
    urls = scrape(url)
    download_imgs(url, urls)


def scrape(url):
    url_list = []
    page = get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    content_ul = soup.find_all('ul')
    for ul in content_ul:
        uls_a = ul.find_all('a', {"class": None, "id": None, "data-tracking": None}, href=True)
        for a in uls_a:
            if "/wiki/" in a['href']:
                url_list.append(a['href'])

    return url_list


def create_url(url, wiki_url):
    old_url = url.split(".com/")
    return "{}.com{}".format(old_url[0], wiki_url)


def snake(name):
    return '_'.join(
        re.sub('([A-Z][a-z]+)', r' \1',
               re.sub('([A-Z]+)', r' \1',
                      name.replace('-', ' '))).split()).lower()


def get_correct_images(images):
    eligible_images = []
    for image in images:
        try:
            for tag in image['class']:
                if re.match("thumb*", tag) or tag == "lazyload" or tag == "pi-image-thumbnail":
                    if is_valid_url(image['src']):
                        eligible_images.append(image)
                    else:
                        continue
        except:
            continue
    return eligible_images


def is_valid_url(url):
    if not "https" in url:
        return False
    else:
        return True


def download_imgs(prefix_url, wiki_urls):
    folder_name = prefix_url.split("/wiki/")[1]

    if not os.path.exists(folder_name):
        os.mkdir(folder_name)

    for wiki_page in wiki_urls:
        url = create_url(prefix_url, wiki_page)

        try:
            wiki_content = get(url).content

            soup = BeautifulSoup(wiki_content, 'html.parser')
            images = soup.find_all('img')

            eligible_images = get_correct_images(images)

            for image in eligible_images:
                if len(eligible_images) > 1:
                    item_name = snake(image['data-image-name']).split(".")[0]
                    if "layout" in item_name or "box" in item_name:
                        continue
                else:
                    item_name = \
                        snake(soup.find('h1', {"id": "firstHeading"}).text).replace("_(", "").replace(")", "").replace(
                            "'", "")

                image_file = get(image['src']).content

                file = open("{}/{}.png".format(folder_name, item_name), "wb+")
                file.write(image_file)
                file.close()

        except:
            print("skipped {}".format(wiki_page))
            continue


if __name__ == '__main__':
    main()
